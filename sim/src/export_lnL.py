#!/usr/bin/env python
# export log likelihood and time
import sys
import shelve

if __name__ == '__main__':
    db = shelve.open(sys.argv[1])
    print 'sim,dataset,hyp,type,lnL.0,lnL.1,time'
    for k in db:
        sim, f = k.split('/')
        d, hyp, tp, _ = f.split('.')
        obj = db[k]
        h0 = obj['H0']['maxLnL']
        h1 = obj['H1']['maxLnL']
        tm = obj['time']
        print ','.join(map(str, (
            k, sim, d, hyp[1], tp, h0, h1, tm
            )))
    db.close()
