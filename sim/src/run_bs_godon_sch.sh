#!/bin/bash
# run branch-site with three-rate Scheffler's parametrization on branch-site simuations
#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J bs-sim-sch[1-80]
#BSUB –R "select[tmp>1024],rusage[mem=4096]"
#BSUB -M 4194304
##BSUB -n 4
##BSUB -R "span[ptile=4]"

NAME=bsgp-sim-bs-est-sch
SUB=1-godon
SOURCE=bs.tar.xz

source $HOME/mysub/mysub.bash

export OMP_NUM_THREADS=1
PAR="--procs 1 --seed 1 --log-level info --m0-tree"
GODON=$CLUSTER/godon-2604d45

cmd () {
	if [[ $1 == *.fst ]]
	then
		dn=$(dirname $1)
		mkdir -p res/$dn
		fst=$1
		bn=$(basename $1)
		nwk=${1%.*}.nwk
		log=res/${1%.*}.sp.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BSG --ncat-site-rate 3 --proportional --json $json --trajectory $tr --out $log $fst $nwk
		log=res/${1%.*}.cp.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BSG --ncat-codon-rate 3 --proportional --json $json --trajectory $tr --out $log $fst $nwk
	fi
}

run
