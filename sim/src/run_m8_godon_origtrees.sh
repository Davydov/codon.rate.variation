#!/bin/bash
# run M8 on M8 simulations
#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J m8-sim[1-40]
#BSUB –R "select[tmp>1024],rusage[mem=4096]"
#BSUB -M 4194304
##BSUB -n 4
##BSUB -R "span[ptile=4]"

NAME=bsgp-sim-m8_3
SUB=2-godon-origtrees
SOURCE=m8_trees_all.tar.xz

source $HOME/mysub/mysub.bash

export OMP_NUM_THREADS=1
PAR="--procs 1 --seed 1 --log-level info --no-branch-length"
GODON=$CLUSTER/godon-30fb0f4

cmd () {
	if [[ $1 == *.fst ]]
	then
		dn=$(dirname $1)
		mkdir -p res/$dn
		fst=$1
		bn=$(basename $1)
		ds=${bn%.*.*}
		nwk=trees/$ds.nwk
		log=res/${1%.*}.nog.M8.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR M8 --json $json --trajectory $tr --out $log $fst $nwk 
		log=res/${1%.*}.sg.M8.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR M8 --ncat-site-rate 4 --json $json --trajectory $tr --out $log $fst $nwk 
		log=res/${1%.*}.cg.M8.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR M8 --ncat-codon-rate 4 --json $json --trajectory $tr --out $log $fst $nwk 
		log=res/${1%.*}.cp.M8.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR M8 --ncat-codon-rate 3 --proportional --json $json --trajectory $tr --out $log $fst $nwk
	fi
}

run
