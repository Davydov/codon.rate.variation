#!/bin/bash
# run old version of parris on branch-site simulations
#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J bs-sim-parris-old[1-40]
#BSUB –R "select[tmp>1024],rusage[mem=4096]"
#BSUB -M 4194304
##BSUB -n 4
##BSUB -R "span[ptile=4]"

NAME=bsgp-sim-bs
SUB=6-parris-old
SOURCE=bs.tgz

source $HOME/mysub/mysub.bash

export OMP_NUM_THREADS=1
export PATH=$PATH:$HOME/hyphy/bin

cmd () {
	if [[ $1 == *.nwk ]]
	then
		dn=$(dirname $1)
		mkdir -p res/$dn
		nwk=$1
		fst=${1%.*}.fst

		summary=res/$1.novar.res
		log=$summary.log
		bf=$($CLUSTER/hp_parris_old.sh $fst $nwk $summary 0 $CLUSTER/PS.bf)
		HYPHYMP $bf &> $log
		mv $fst.* res/$dn/
		mv messages.log $summary.messages
		test -f errors.log && mv errors.log $summary.errors

		summary=res/$1.var.res
		log=$summary.log
		bf=$($CLUSTER/hp_parris_old.sh $fst $nwk $summary 1 $CLUSTER/PS.bf)
		HYPHYMP $bf &> $log
		mv $fst.* res/$dn/
		mv messages.log $summary.messages
		test -f errors.log && mv errors.log $summary.errors
		
	fi
}

run
