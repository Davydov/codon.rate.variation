#!/bin/bash
# run olvd version of Parris
ali=$(readlink -f $1)
tree=$2
summary=$(readlink -f $3)
var=$4
bspath=$5

treefile=$summary.hptrees
bffile=$summary.cfg

cat > $treefile <<EOF
treeStringFullData = "$(cat $tree | sed 's/#1//')";
EOF

cat > $bffile << EOF
INPUTFILE = "$ali";
SUMMARY_FILE = "$summary";
numParts = 1;
breakpointlist = {};
variable_dS = ${var:-0};
SingleFullPartition=1;
SinglePartition=0;
Separate_Partitions = 0;
#include "$treefile";
#include "${bspath:-PS.bf}";
EOF

echo $bffile

