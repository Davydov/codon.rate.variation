#!/usr/bin/env python3
# export m1m2 results to csv
import sys
import json
import os.path
from glob import iglob


print('sim', 'dataset', 'hyp', 'model', 'L0', 'L1')
for fn in iglob('*/*.json'):
    dataset, hyp, model, _ = os.path.basename(fn).split('.')
    hyp = int('H1' == hyp)
    sim = os.path.dirname(fn)
    model = model
    with open(fn) as f:
        j = json.load(f)
    l0 = j['H0']['maxLnL']
    l1 = j['H1']['maxLnL']
    print(sim, dataset, hyp, model, l0, l1)
