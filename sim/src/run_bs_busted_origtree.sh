#!/bin/bash
# running busted on branch-site simulations
#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J bs-sim-busted[1-40]
#BSUB –R "select[tmp>1024],rusage[mem=4096]"
#BSUB -M 4194304
##BSUB -n 4
##BSUB -R "span[ptile=4]"

NAME=bsgp-sim-bs_3
SUB=4-busted-origtrees
SOURCE=bs_trees_all.tgz

source $HOME/mysub/mysub.bash

export OMP_NUM_THREADS=1
export PATH=$PATH:$HOME/hyphy/bin

cmd () {
	if [[ $1 == *sp/* ]]
	then
		return
	fi
	if [[ $1 == *.fst ]]
	then
		dn=$(dirname $1)
		mkdir -p res/$dn
		fst=$1
		bn=$(basename $fst)
		inwk=trees/${bn%.*.*}.nwk
		snwk=$inwk.scaled
		nwk=res/${1%.*}.onwk
		bf=res/${1%.*}.bf
		log=res/${1%.*}.log

		## rescale the tree
		$HOME/dndstools/scale_tree.py --multiply $inwk 10 $snwk
		## first convert tree
		branch=$($CLUSTER/rename_branch_for_hyphy.py $snwk $nwk)

		$CLUSTER/hp_busted.sh $fst $nwk $branch > $bf
		bf=$(readlink -f $bf)
		HYPHYMP $bf &> $log
		mv $fst.* res/$dn/
		mv messages.log res/$fst.messages
		test -f errors.log && mv errors.log res/$fst.errors
	fi
}

run
