## performance of branch-site on M8 simulations
library(plyr)
library(caret)
library(metap)
library(dplyr)
library(ROCR)
library(survcomp)
library(ggplot2)

# colorbrewer2 color blind friendly colors
c.b.green <- '#1b9e77'
c.b.red <- '#d95f02'
c.b.blue <- '#7570b3'
c.b.pink <- '#e7298a'
c.b.grgreen <- '#66a61e'

theme_set(theme_bw())

simple.style <- theme(panel.grid.major = element_blank(),
                      panel.grid.minor = element_blank())

## ROC plot function
my.roc <- function(pred, lab, ...) {
    p <- prediction(pred, lab)
    perf <- performance(p, measure='tpr', x.measure='fpr')
    plot(perf, ...)
}

## ROC plot using ggplot
my.gg.roc <- function(d) {
    d <- lapply(seq_along(d), function(i) e.xy(d[[i]], 'ss', '', names(d)[i]))
    df <- do.call(rbind, d)
    df <- plyr::rename(df, c('x'='Specificity', 'y'='Sensitivity'))
    ggplot(df, aes(Specificity, Sensitivity, color=Inference)) +
        geom_line() +
        theme(legend.position = c(.7, .2)) +
        scale_color_brewer(palette='Set2') +
        annotate('segment', x=0, xend=1, y=0, yend=1, linetype=2, col='gray')
}


## FPR plot
my.fpr <- function(pred, lab, add=FALSE, ...) {
    p <- prediction(1-pred, lab)
    perf <- performance(p, measure='fpr')
    if (add) f <- lines
    else f <- plot
    f(1-unlist(perf@x.values), unlist(perf@y.values), type='l', ...)
}

## read M8 data
read.m8 <- function(fn) {
    d <- read.table(fn, sep=',')
    d <- plyr::rename(d, c(hyp='sel', type='method'))

    d$lrt <- 2 * (d$lnL.1 - d$lnL.0)
    d$pvalue <- pchisq(d$lrt, df=1, lower.tail=F)/2

    thr <- qchisq(0.95, 1)
    d$det <- d$lrt > thr
    d

}

## read & process data
d.m8.bs <- read.table('m8_bs.txt', sep=',', header=TRUE)
d.m8.bs.cp <- read.table('m8_bs_cp.txt', sep=',', header=TRUE)
d.m8.bs <- dplyr::bind_rows(d.m8.bs, d.m8.bs.cp)
d.m8.bs <- plyr::rename(d.m8.bs, c('D'='lrt', 'hyp'='sel'))
d.m8.bs$pvalue <- pchisq(d.m8.bs$lrt, df=1, lower.tail=FALSE)/2
d.m8.m8 <- read.m8('../m8_roc/full_sim_m8.txt')
d.m8.m8.cp1 <- read.m8('../m8_roc/m8_sim_sch_newgodon.txt')
d.m8.m8.cp2 <- read.m8('../m8_roc/m8_simcp_estall.txt')
d.m8.m8 <- dplyr::bind_rows(d.m8.m8, d.m8.m8.cp1, d.m8.m8.cp2)
d.m8.m8 <- subset(d.m8.m8, select=c(-lnL.0, -lnL.1, -time, -det))

## confusion matrix
with(
    subset(d.m8.m8, sim=='nog' & method=='nog'),
    confusionMatrix(
    factor((pvalue < 0.05)*1),
    factor(sel)
    )
)

with(
    subset(d.m8.bs, sim=='nog' & method=='nog'),
    confusionMatrix(
    factor((pvalue < 0.05)*1),
    factor(sel)
    )
)


## different ways of pvalue aggregation
d.m8.bs.sumlog <- d.m8.bs %>%
    group_by(sim, dataset, sel, method) %>%
    summarize(pvalue=sumlog(pvalue)$p)

d.m8.bs.min <- d.m8.bs %>%
    group_by(sim, dataset, sel, method) %>%
    summarize(pvalue=min(pvalue))

d.m8.bs.mean <- d.m8.bs %>%
    group_by(sim, dataset, sel, method) %>%
    summarize(pvalue=mean(pvalue))

d.m8.bs.f <- d.m8.bs %>%
    group_by(sim, dataset, sel, method) %>%
    summarize(pvalue=combine.test(pvalue, method='fisher'))

d.m8.bs.z <- d.m8.bs %>%
    group_by(sim, dataset, sel, method) %>%
    summarize(pvalue=combine.test(pvalue, method='z.transform'))

d.m8.bs.l <- d.m8.bs %>%
    group_by(sim, dataset, sel, method) %>%
    summarize(pvalue=combine.test(pvalue, method='logit'))

## confusion matrix
with(
    subset(d.m8.bs.sumlog, sim=='nog' & method=='nog'),
    confusionMatrix(
    factor((pvalue < 0.05)*1),
    factor(sel)
    )
)

## plot ROC for different p-value aggregation methods
with(subset(d.m8.m8, sim=='nog' & method=='nog'),
     my.roc(-log10(pvalue), sel, col='green')
     )
with(subset(d.m8.bs.sumlog, sim=='nog' & method=='nog'),
     my.roc(-log10(pvalue), sel, add=TRUE, col='blue')
     )
with(subset(d.m8.bs.min, sim=='nog' & method=='nog'),
     my.roc(-log10(pvalue), sel, add=TRUE, col='red')
     )
with(subset(d.m8.bs.mean, sim=='nog' & method=='nog'),
     my.roc(-log10(pvalue), sel, add=TRUE, col='yellow')
     )
with(subset(d.m8.bs.f, sim=='nog' & method=='nog'),
     my.roc(-log10(pvalue), sel, add=TRUE, col='orange')
     )
with(subset(d.m8.bs.z, sim=='nog' & method=='nog'),
     my.roc(-log10(pvalue), sel, add=TRUE, col='black')
     )
with(subset(d.m8.bs.f, sim=='nog' & method=='nog'),
     my.roc(-log10(pvalue), sel, add=TRUE, col='cyan')
     )

## plot FPR for different p-value aggregation methods
with(subset(d.m8.m8, sim=='nog' & method=='nog'),
     my.fpr(pvalue, sel, col='green')
     )
abline(0, 1, col='gray', lty=19)
with(subset(d.m8.bs.sumlog, sim=='nog' & method=='nog'),
     my.fpr(pvalue, sel, add=TRUE, col='blue')
     )
with(subset(d.m8.bs.min, sim=='nog' & method=='nog'),
     my.fpr(pvalue, sel, add=TRUE, col='red')
     )
with(subset(d.m8.bs.mean, sim=='nog' & method=='nog'),
     my.fpr(pvalue, sel, add=TRUE, col='yellow')
     )
with(subset(d.m8.bs.f, sim=='nog' & method=='nog'),
     my.fpr(pvalue, sel, add=TRUE, col='orange')
     )
with(subset(d.m8.bs.z, sim=='nog' & method=='nog'),
     my.fpr(pvalue, sel, add=TRUE, col='black')
     )
with(subset(d.m8.bs.f, sim=='nog' & method=='nog'),
     my.fpr(pvalue, sel, add=TRUE, col='cyan')
     )


## compute performance
crr <- function (x) {
    pred <- prediction(1-x$pvalue, x$sel)
    list(ss=performance(pred, 'tpr', 'fpr'),
         auc=performance(pred, 'auc'),
         fpr=performance(pred, 'fpr')
         )
}


## convert list of prediction performace to a dataframe
cr.df <- function(x, stat, ds, names=c('n', 's', 'c', 'p'), method.names=c('No variation',
                                                                           'Site variation',
                                                                           'Codon gamma variation',
                                                                           'Codon 3-rate variation'))
    rbind(
        e.xy(x[[names[1]]], stat, ds=ds, method=method.names[1]),
        e.xy(x[[names[2]]], stat, ds=ds, method=method.names[2]),
        e.xy(x[[names[3]]], stat, ds=ds, method=method.names[3]),
        e.xy(x[[names[4]]], stat, ds=ds, method=method.names[4])
    )

## convert performace from ROCR to a data.frame
e.xy <- function(x, stat, ds, method)
    data.frame(x=unlist(x[[stat]]@x.values),
               y=unlist(x[[stat]]@y.values),
               ds=ds, Inference=method)

## compute performance for BS, BS+sitevar, BS+codonvar
mr.n <- function (x)
    list(
        n=crr(subset(x, method=='nog')),
        s=crr(subset(x, method=='sg')),
        c=crr(subset(x, method=='cg')),
        p=crr(subset(x, method=='cp'))
        )

## plot ROC using ggplot for three datasets
pl.n.gg <- function(n, s, c, p) {
    df <- rbind(cr.df(n, 'fpr', 'A) No variation'),
                cr.df(s, 'fpr', 'B) Site gamma variation'),
                cr.df(c, 'fpr', 'C) Codon gamma variation'),
                cr.df(p, 'fpr', 'D) Codon 3-rate variation')
                )
    df$x <- 1 - df$x
    df <- subset(df, x>=0)
    df <- plyr::rename(df, c('x'='Specificity', 'y'='Sensitivity'))
    ggplot(df, aes(Specificity, Sensitivity, color=Inference, linetype=Inference)) +
        geom_line() +
        facet_wrap(~ds, ncol=2) +
        ##theme(legend.position = c(.8, .2)) +
        scale_color_manual(values=c('black', c.b.red, c.b.green, c.b.grgreen)) +
        scale_linetype_manual(values=c('solid', 'solid', 'solid', 'dashed')) +
        annotate('segment', x=0, xend=1, y=0, yend=1, linetype=2, col='gray')
}

## compute performance for various p-value aggregation method for four
## datasets (no rate variation, site rate variation, codon gamma rate
## variation, and codon 3-rate variation)
r.n.noagg <- mr.n(subset(d.m8.bs, sim=='nog'))
r.s.noagg <- mr.n(subset(d.m8.bs, sim=='sg'))
r.c.noagg <- mr.n(subset(d.m8.bs, sim=='cg'))
r.p.noagg <- mr.n(subset(d.m8.bs, sim=='cp'))


r.n <- mr.n(subset(d.m8.bs.f, sim=='nog'))
r.s <- mr.n(subset(d.m8.bs.f, sim=='sg'))
r.c <- mr.n(subset(d.m8.bs.f, sim=='cg'))
r.p <- mr.n(subset(d.m8.bs.f, sim=='cp'))
r.ref <- mr.n(subset(d.m8.m8, sim=='nog'))

r.n.min <- mr.n(subset(d.m8.bs.min, sim=='nog'))
r.s.min <- mr.n(subset(d.m8.bs.min, sim=='sg'))
r.c.min <- mr.n(subset(d.m8.bs.min, sim=='cg'))
r.p.min <- mr.n(subset(d.m8.bs.min, sim=='cp'))

r.n.z <- mr.n(subset(d.m8.bs.z, sim=='nog'))
r.s.z <- mr.n(subset(d.m8.bs.z, sim=='sg'))
r.c.z <- mr.n(subset(d.m8.bs.z, sim=='cg'))
r.p.z <- mr.n(subset(d.m8.bs.z, sim=='cp'))

r.n.l <- mr.n(subset(d.m8.bs.l, sim=='nog'))
r.s.l <- mr.n(subset(d.m8.bs.l, sim=='sg'))
r.c.l <- mr.n(subset(d.m8.bs.l, sim=='cg'))
r.p.l <- mr.n(subset(d.m8.bs.l, sim=='cp'))

r.n.sumlog <- mr.n(subset(d.m8.bs.sumlog, sim=='nog'))
r.s.sumlog <- mr.n(subset(d.m8.bs.sumlog, sim=='sg'))
r.c.sumlog <- mr.n(subset(d.m8.bs.sumlog, sim=='cg'))
r.p.sumlog <- mr.n(subset(d.m8.bs.sumlog, sim=='cp'))

## plot FPR of running branch-site model on M8 simulations
pdf('sim-m8-bs-fpr.pdf', width=7, height=5)
pl.n.gg(r.n, r.s, r.c, r.p) + simple.style + coord_fixed()
dev.off()

pl.n.gg(r.n.noagg, r.s.noagg, r.c.noagg, r.p.noagg) + simple.style
pl.n.gg(r.n.z, r.s.z, r.c.z, r.p.z) + simple.style
pl.n.gg(r.n.l, r.s.l, r.c.l, r.p.l) + simple.style


## plot ROC of running branch-site model on M8 simulations
pdf('sim-m8-bs-roc.pdf', width=4, height=4)
my.gg.roc(list("Fisher's method"=r.n$n, "Minimum p-value"=r.n.min$n, "Reference (M8)"=r.ref$n)) +
    simple.style
dev.off()
