#!/usr/bin/env python
# export FUBAR results
import sys
import csv
import os.path
from glob import iglob


header = None
for fn in iglob('*/*.csv'):
    dataset = fn.rsplit('.', 4)[0]
    hyp = int('H1' in dataset)
    sim = os.path.dirname(fn)
    with open(fn) as f:
        first = True
        for record in csv.reader(f):
            if first:
                if header is None:
                    header = record
                    writer = csv.writer(sys.stdout)
                    header = ['sim', 'dataset', 'hyp'] + header
                    writer.writerow(header)
                first = None
                continue
            writer.writerow([sim, dataset, hyp] + record)
