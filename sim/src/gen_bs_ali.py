#!/usr/bin/env python
## generates bs alignments
import csv
from multiprocessing import Pool

from plumbum import local
cosim = local['cosim.py']

def simbs(base, bspar, record, tree):
    global commands
    if base.endswith('.H1'):
        omega2 = record['w2.bs']
    else:
        omega2 = '1'
    par = bspar + ['--omega2', omega2, '--log', base + '.log',
                     tree, record['alen'], base + '.fst']
    commands.append(par)

def run(par):
    print('cosim ' + ' '.join(par))
    cosim(par)

if __name__ == '__main__':
    mkdir = local['mkdir']
    mkdir('-p', 'data/bs/nog', 'data/bs/cg', 'data/bs/sg',
          'data/bs/cp', 'data/bs/sp')
    commands = []
    for record in csv.DictReader(open('data/gamma.txt')):
        bspar = ['--seed', '1',
                 '--model', 'BS',
                 '--kappa', record['k'],
                 '--omega', record['w0'],
                 '--p0', record['p0.bs'],
                 '--p1', record['p1.bs'],
                 '--scale-tree', '10',
                 '--debug']
        site_a = ['--site-alpha', record['alpha'], '--site-ncat', '4']
        codon_a = ['--codon-alpha', record['alpha'], '--codon-ncat', '4']
        site_r = ['--site-rates', ','.join((record['r1.eff'], record['r2.eff'], record['r3.eff'])),
                  '--site-props', ','.join((record['p1.eff'], record['p2.eff'], record['p3.eff']))]
        codon_r = ['--codon-rates', ','.join((record['r1.eff'], record['r2.eff'], record['r3.eff'])),
                  '--codon-props', ','.join((record['p1.eff'], record['p2.eff'], record['p3.eff']))]
        tree = 'data/trees/' + record['dataset'] + '.nwk'
        d = 'data/bs/nog/'
        base = d + record['dataset']
        simbs(base + '.H0', bspar, record, tree)
        simbs(base + '.H1', bspar, record, tree)
        d = 'data/bs/sg/'
        base = d + record['dataset']
        simbs(base + '.H0', bspar + site_a, record, tree)
        simbs(base + '.H1', bspar + site_a, record, tree)
        d = 'data/bs/cg/'
        base = d + record['dataset']
        simbs(base + '.H0', bspar + codon_a, record, tree)
        simbs(base + '.H1', bspar + codon_a, record, tree)
        d = 'data/bs/sp/'
        base = d + record['dataset']
        simbs(base + '.H0', bspar + site_r, record, tree)
        simbs(base + '.H1', bspar + site_r, record, tree)
        d = 'data/bs/cp/'
        base = d + record['dataset']
        simbs(base + '.H0', bspar + codon_r, record, tree)
        simbs(base + '.H1', bspar + codon_r, record, tree)

    nproc = 12
    p = Pool(nproc)
    p.map(run, commands)

