#!/bin/bash
# run simulations and bs with positive selection on bg branches
#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J bsbg[1-150]
#BSUB –R "select[tmp>1024],rusage[mem=4096]"
#BSUB -M 4194304
##BSUB -n 4
##BSUB -R "span[ptile=4]"

NAME=bsbg
SUB=1-sim-inf
SOURCE=trees.tgz

source $HOME/mysub/mysub.bash

export OMP_NUM_THREADS=1
cosim=$HOME/.local/bin/cosim.py
COSIMPAR="--model BS --p0 0.8 --p1 0.15 --omega2 10 --omega3 20 --p3 .3"
godon=$CLUSTER/godon-0bfc5af
GODONPAR="--no-final --no-branch-length --procs 1 --seed 1 --log-level info"

cmd () {
	if [[ $1 == *.nwk ]]
	then
	    bn=$(basename $1)
	    bn=res/${bn%.*}
	    nwk=$1

	    fst=$bn.BS.fst
	    $cosim --log ${fst%.*}.log $COSIMPAR $nwk 150 $fst
	    out=${fst%.*}.BS
	    $godon test BS $GODONPAR --out $out.out --json $out.json $fst $nwk > /dev/null
	    out=${fst%.*}.BSCG
	    $godon test BSG --ncat-codon-gamma 4 $GODONPAR --out $out.out --json $out.json $fst $nwk > /dev/null
	    
	    fst=$bn.BSCG.fst
	    $cosim --log ${fst%.*}.log $COSIMPAR --codon-alpha 0.5 --codon-ncat 4 $nwk 150 $fst
	    out=${fst%.*}.BS
	    $godon test BS $GODONPAR --out $out.out --json $out.json $fst $nwk > /dev/null
	    out=${fst%.*}.BSCG
	    $godon test BSG --ncat-codon-gamma 4 $GODONPAR --out $out.out --json $out.json $fst $nwk > /dev/null
	fi
}

run
