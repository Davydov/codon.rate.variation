#!/usr/bin/env python

# takes a tree with paml style fg branch and converts it to
# busted-friendly format. it outputs the foreground branch/tip name to
# stdout
import sys
import dendropy

if __name__ == '__main__':
    intreefn = sys.argv[1]
    ontreefn = sys.argv[2]
    tree = dendropy.Tree.get(path=sys.argv[1], schema='newick')
    fgi = 0
    for node in tree.postorder_node_iter():
        if node.is_leaf():
            if node.taxon.label.endswith('#1'):
                node.taxon.label = node.taxon.label.rsplit('#', 1)[0]
                print node.taxon.label
        else:
            if node.label == '#1':
                node.label = 'FG%d' % fgi
                fgi += 1
                print node.label
    tree.write(path=sys.argv[2], schema='newick', real_value_format_specifier='.8f')
