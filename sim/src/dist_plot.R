## Plot simluation parameters distributions
library(ggplot2)
library(reshape2)
library(plyr)
theme_set(theme_bw())

g <- read.table('../gamma.txt', sep=',', header=T)
g$tlen.bs <- g$tlen * 10
str(g)
g <- g[,c('dataset', 'ntips', 'alen', 'k', 'alpha',
       ## M8
       'tlen', 'w2.m8', 'a', 'b', 'p.m8',
       ## BS
       'tlen.bs', 'w0', 'p01sum', 'p0prop', 'w2.bs', 'p0.bs', 'p1.bs',
       ## 3-rate
       'r1', 'r2', 'p1', 'p2')
       ]
g <- melt(subset(g, select=-dataset))
conv <- c('ntips'='Number~of~sequences',
          'tlen'='Total~branch~length~(M8)',
          'tlen.bs'='Total~branch~length~("Branch-site")',
          'alen'='Alignment~length~(codons)',
          'w0'='omega[0]~("Branch-site")',
          'w2.m8'='omega~(M8)',
          'k'='kappa',
          'a'='p~(M8)',
          'b'='q~(M8)',
          'p.m8'='p[0]~(M8)',
          'p01sum'='p[0]+p[1]~("Branch-site")',
          'p0prop'='p[0]/(p[0]+p[1])~("Branch-site")',
          'alpha'='alpha',
          'w2.bs'='omega[2]~("Branch-site")',
          'p0.bs'='p[0]~("Branch-site")',
          'p1.bs'='p[1]~("Branch-site")',
          'r1'='R[1]~(3-rate)',
          'r2'='R[2]~(3-rate)',
          'p1'='p[1]~(3-rate)',
          'p2'='p[2]~(3-rate)'
          )
g$variable <- factor(g$variable, labels=conv[levels(g$variable)])

pdf('sim-par-dist.pdf', width=8.5, height=11)
ggplot(g, aes(value)) +
	geom_histogram() +
	facet_wrap(~variable, scale='free', labeller=label_parsed, ncol=3)
dev.off()
