#!/bin/bash
## reconstructs tree for all m8 alignments
## keeping the topology
## phyml version 20131022
find data/m8 -iname "*.phy" | xargs -n 1 -P 10 src/retree.sh
