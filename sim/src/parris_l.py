#!/usr/bin/env python3
# export PARRIS LRT
import glob
from os.path import join, basename

def parse_parris(fn):
    res = {}
    with open(fn) as f:
        first = True
        for line in f:
            if line.startswith('|'):
                line = line.rstrip().strip('|')
                if first:
                    header = [v.strip() for v in line.split('|')]
                    first = False
                else:
                    d = {k: v.strip() for k, v in zip(header, line.split('|'))}
                    res[d['Model']] = d
    return res

def lrt(d):
    l0 = float(d['discr(3), M1a']['Log Likelihood'])
    l1 = float(d['discr(3), M2a']['Log Likelihood'])
    return 2 * (l1 - l0)

if __name__ == '__main__':
    for ds in glob.glob('*'):
        for fn in glob.glob(join(ds, '*.parris')):
            ac, hyp, _ = basename(fn).split('.', 2)
            res = parse_parris(fn)
            l = lrt(res)
            print(ds, ac, hyp, l)
