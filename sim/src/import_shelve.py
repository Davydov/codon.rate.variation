#!/usr/bin/env python
# import json data into shelve for fast access
import shelve
from glob import iglob
import sys
import json
from itertools import chain

if __name__ == '__main__':
    dbname = sys.argv[1]
    db = shelve.open(dbname)
    cnt = 0
    for fn in chain(iglob('*.json'), iglob('*/*.json')):
        print cnt, fn
        with open(fn) as f:
            obj = json.load(f)
            db[fn.rsplit('.', 1)[0]] = obj
        cnt += 1

    db.close()

