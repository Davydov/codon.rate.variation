#!/bin/bash
## remove #1 from a tree
find -iname "*.nwk" | while read fn
do
	sed 's/#1//' $fn > $fn.nohash
done
