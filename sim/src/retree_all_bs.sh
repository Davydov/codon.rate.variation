#!/bin/bash
## reconstructs tree for all bs alignments
## keeping the topology
## phyml version 20131022
find data/bs -iname "*.phy" | xargs -n 1 -P 10 src/retree.sh
