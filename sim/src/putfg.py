#!/usr/bin/env python
## put fg branches on phyml trees
import glob
import os
from itertools import islice

from dendropy import Tree
from dendropy.calculate.treecompare import unweighted_robinson_foulds_distance

if __name__ == '__main__':
    for tn in chain(glob.iglob('data/m8/*/*_phyml_tree.txt'),
                    glob.iglob('data/bs/*/*_phyml_tree.txt')):
        t = Tree.get(path=tn, schema='newick')
        fgfn = 'data/trees/' + os.path.basename(tn).rsplit('.', 3)[0] + '.nwk'
        fgt = Tree.get(path=fgfn,
                       schema='newick',
                       taxon_namespace=t.taxon_namespace)
        if unweighted_robinson_foulds_distance(t, fgt) > 0:
            ## this means that one of the leafs is fg
            for node in fgt.leaf_nodes():
                if node.taxon.label.endswith('#1'):
                    nn = node.taxon.label.rsplit('#', 1)[0]
                    for onode in t.leaf_nodes():
                        if onode.taxon.label == nn:
                            onode.taxon = node.taxon
                            break
                    else:
                        print 'node %s not found in %s' % (nn, tn)
                    break
            else:
                print 'cannot found #1 leaf in %s' % fgfn
        else:
            # check that brackets structure is identical
            ts = t.as_string(schema='newick', suppress_edge_lengths=True)
            fgts = fgt.as_string(schema='newick', suppress_edge_lengths=True)
            assert ts == fgts.replace('#1', '')

            # get index of foreground node
            fgid = [i for i, n in enumerate(fgt.postorder_internal_node_iter())
                    if n.label=='#1']
            assert len(fgid) == 1
            fgid = fgid[0]

            # get foreground node
            fgnode = next(islice(t.postorder_internal_node_iter(), fgid, fgid
                                 + 1))
            fgnode.label = '#1'

            # check that now brackets structure is the same
            ts = t.as_string(schema='newick', suppress_edge_lengths=True)
            assert ts == fgts


        t.write(path=tn.rsplit('.', 2)[0] + '.nwk', schema='newick',
                real_value_format_specifier='.8f')
