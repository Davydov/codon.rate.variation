#!/usr/bin/env python3
## export m8vsm8 results of old version of PARRIS
from glob import iglob
from os.path import dirname, basename

def read_parris(fn):
    with open(fn) as f:
        for line in f:
            if line.startswith('| 15.'):
                l1 = float(line.split('|')[2])
            elif line.startswith('|  8.'):
                l2 = float(line.split('|')[2])
    return l1, l2


print('sim dataset hyp var l1 l2')
for fn in iglob('*/*.log'):
    sim = dirname(fn)
    ds = basename(fn).split('.')[0]
    hyp = int('.H1.' in fn)
    var = int('.var.' in fn)
    try:
        l1, l2 = read_parris(fn)
    except UnboundLocalError:
        continue
    print(sim, ds, hyp, var, l1, l2)
