#!/bin/bash
## 1) is no tree estimation worse?
## 2) is full tree estimation better?
## 3) is M0G better for pre estimation with BS+G


#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J bs-sim-misc[1-30]
#BSUB –R "select[tmp>1024],rusage[mem=4096]"
#BSUB -M 4194304
##BSUB -n 4
##BSUB -R "span[ptile=4]"

NAME=bsgp-sim-bs_2
SUB=2-misc
SOURCE=bs.tgz

source $HOME/mysub/mysub.bash

export OMP_NUM_THREADS=1
PAR="--procs 1 --seed 1 --log-level info"
GODON=$CLUSTER/godon-8c06ef0

cmd () {
	if [[ $1 == *.fst ]]
	then
		dn=$(dirname $1)
		mkdir -p res/$dn
		fst=$1
		bn=$(basename $1)
		nwk=${1%.*}.nwk
		log=res/${1%.*}.noblen.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BS --no-branch-length --json $json --trajectory $tr --out $log $fst $nwk 
		log=res/${1%.*}.fullblen.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BS --json $json --trajectory $tr --out $log $fst $nwk 
		log=res/${1%.*}.m0gammatree.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BSG --m0-tree --m0-tree-gamma --ncat-codon-gamma 4 --json $json --trajectory $tr --out $log $fst $nwk
	fi
}

run
