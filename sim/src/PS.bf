/* This file is included by a cfg file which defines filenames, breakpoints,
etc. */

ModelNames = {{"Neutral",
	  		  "Selection",
			  "Discrete",
			  "Freqs",
			  "Gamma",
			  "2 Gamma",
			  "Beta",
			  "Beta & w",
			  "Beta & Gamma",
			  "Beta & (Gamma+1)",
			  "Beta & (Normal>1)",
			  "0 & 2 (Normal>1)",
			  "3 Normal",
			  "Gamma mod Beta",
			  "Beta & 1"}};
			  
ParameterCount = {{2,
	  		  	   4,
			  	   5,
			  	   4,
			  	   2,
			  	   4,
			       2,
			  	   4,
			  	   5,
			       5,
			  	   5,
			       5,
			       6,
			       4,
			       3}};
			       
MAXIMUM_ITERATIONS_PER_VARIABLE = 2000;
OPTIMIZATION_PRECISION 			= 0.001;
			  
/* ________________________________________________________________________________________________*/

/* Functions copied from SingleBreakPointRecomb.bf: */

function InitializeDistances (dummy)
{
	HarvestFrequencies (_dNucFreq,filteredData,1,1,0);
	_d_fR = _dNucFreq[0]+_dNucFreq[2];
	_d_fY = _dNucFreq[1]+_dNucFreq[3];
	
	if (_dNucFreq[0] == 0 || _dNucFreq[1] == 0 || _dNucFreq[2] == 0 || _dNucFreq[3] == 0)
	{
		_useK2P = 1;
	}
	else
	{
		_d_TN_K1 = 2*_dNucFreq[0]*_dNucFreq[2]/_d_fR;
		_d_TN_K2 = 2*_dNucFreq[1]*_dNucFreq[3]/_d_fY;
		_d_TN_K3 = 2*(_d_fR*_d_fY-_dNucFreq[0]*_dNucFreq[2]*_d_fY/_d_fR-_dNucFreq[1]*_dNucFreq[3]*_d_fR/_d_fY);
		_useK2P = 0;
	}
	
	
	summingVector = {{1}{1}{1}{1}};

	return 0;
}

/* ________________________________________________________________________________________________*/

function TreeMatrix2TreeString (doLengths)
{
	treeString = "";
	p = 0;
	k = 0;
	m = treeNodes[0][1];
	n = treeNodes[0][0];
	d = treeString*(Rows(treeNodes)*25);

	while (m)
	{	
		if (m>p)
		{
			if (p)
			{
				d = treeString*",";
			}
			for (j=p;j<m;j=j+1)
			{
				d = treeString*"(";
			}
		}
		else
		{
			if (m<p)
			{
				for (j=m;j<p;j=j+1)
				{
					d = treeString*")";
				}
			}	
			else
			{
				d = treeString*",";
			}	
		}
		if (n<filteredData.species)
		{
			GetString (nodeName, filteredData, n);
			d = treeString*nodeName;
		}
		if (doLengths>.5)
		{
			nodeName = ":"+treeNodes[k][2];
			d = treeString*nodeName;
		}
		k=k+1;
		p=m;
		n=treeNodes[k][0];
		m=treeNodes[k][1];
	}

	for (j=m;j<p;j=j+1)
	{
		d = treeString*")";
	}
	
	d=treeString*0;
	return treeString;
}

/* ________________________________________________________________________________________________*/

function ComputeDistanceFormula (s1,s2)
{
	GetDataInfo (siteDifferenceCount, filteredData, s1, s2, DIST);
	
	totalSitesCompared = Transpose(summingVector)*(siteDifferenceCount*summingVector);
	totalSitesCompared = totalSitesCompared[0];
	
	if (_useK2P)
	{
		_dTransitionCounts 	 =    siteDifferenceCount[0][2]+siteDifferenceCount[2][0]  /* A-G and G-A */
								 +siteDifferenceCount[1][3]+siteDifferenceCount[3][1]; /* C-T and T-C */
							
		_dTransversionCounts = (siteDifferenceCount[0][0]+siteDifferenceCount[1][1]+siteDifferenceCount[2][2]+siteDifferenceCount[3][3])+_dTransitionCounts;
		
		_dTransitionCounts	 = _dTransitionCounts/totalSitesCompared;
		_dTransversionCounts = 1-_dTransversionCounts/totalSitesCompared;
		
		_d1C = 1-2*_dTransitionCounts-_dTransversionCounts;
		_d2C = 1-2*_dTransversionCounts;
		
		if (_d1C>0 && _d2C>0)
		{
			return -(0.5*Log(_d1C)+.25*Log(_d2C));	
		}
	}
	else
	{
		_dAGCounts 	 =    siteDifferenceCount[0][2]+siteDifferenceCount[2][0]  /* A-G and G-A */;
		_dCTCounts	 = 	  siteDifferenceCount[1][3]+siteDifferenceCount[3][1]; /* C-T and T-C */
							
		_dTransversionCounts = (siteDifferenceCount[0][0]+siteDifferenceCount[1][1]+siteDifferenceCount[2][2]+
								siteDifferenceCount[3][3])+_dAGCounts+_dCTCounts;
		
		_dAGCounts	 = _dAGCounts/totalSitesCompared;
		_dCTCounts	 = _dCTCounts/totalSitesCompared;
		
		_dTransversionCounts = 1-_dTransversionCounts/filteredData.sites;
		
		_d1C = 1-_dAGCounts/_d_TN_K1-0.5*_dTransversionCounts/_d_fR;
		_d2C = 1-_dCTCounts/_d_TN_K2-0.5*_dTransversionCounts/_d_fY;
		_d3C = 1-0.5*_dTransversionCounts/_d_fY/_d_fR;
		
		if ((_d1C>0)&&(_d2C>0)&&(_d3C>0))
		{
			return -_d_TN_K1*Log(_d1C)-_d_TN_K2*Log(_d2C)-_d_TN_K3*Log(_d3C);
		}
	}
	
	return 1000;
}

/*-------------------------------------------------------------------------------*/

function InferTreeTopology(verbFlag)
{
	distanceMatrix = {filteredData.species,filteredData.species};
	dummy = InitializeDistances (0);
		
	for (i = 0; i<filteredData.species; i=i+1)
	{
		for (j = i+1; j<filteredData.species; j = j+1)
		{
			distanceMatrix[i][j] = ComputeDistanceFormula (i,j);
		}
	}

	MESSAGE_LOGGING 		 	= 1;
	cladesMade 					= 1;
	

	if (filteredData.species == 2)
	{
		d1 = distanceMatrix[0][1]/2;
		treeNodes = {{0,1,d1__},
					 {1,1,d1__},
					 {2,0,0}};
					 
		cladesInfo = {{2,0}};
	}
	else
	{
		if (filteredData.species == 3)
		{
			d1 = (distanceMatrix[0][1]+distanceMatrix[0][2]-distanceMatrix[1][2])/2;
			d2 = (distanceMatrix[0][1]-distanceMatrix[0][2]+distanceMatrix[1][2])/2;
			d3 = (distanceMatrix[1][2]+distanceMatrix[0][2]-distanceMatrix[0][1])/2;
			treeNodes = {{0,1,d1__},
						 {1,1,d2__},
						 {2,1,d3__}
						 {3,0,0}};
						 
			cladesInfo = {{3,0}};		
		}
		else
		{	
			njm = (distanceMatrix > methodIndex)>=filteredData.species;
				
			treeNodes 		= {2*(filteredData.species+1),3};
			cladesInfo	    = {filteredData.species-1,2};
			
			for (i=Rows(treeNodes)-1; i>=0; i=i-1)
			{
				treeNodes[i][0] = njm[i][0];
				treeNodes[i][1] = njm[i][1];
				treeNodes[i][2] = njm[i][2];
			}

			for (i=Rows(cladesInfo)-1; i>=0; i=i-1)
			{
				cladesInfo[i][0] = njm[i][3];
				cladesInfo[i][1] = njm[i][4];
			}
			
			njm = 0;
		}
	}
	distanceMatrix = 0;
	
	return 1.0;
}
/*-------------------------------------------------------------------------------*/

function PromptForAParameter (promptString, leftBound, rightBound, midx)
{
	if (noMorePrompting)
	{
		return -1;
		
	}
	
	value = leftBound - 1;
	while ((value < leftBound)||(value > rightBound))
	{
		fprintf (stdout, "\n[", ModelNames[midx], "] ", promptString, " in [", leftBound, ",", rightBound, "] (-2 to quit):");
		fscanf	(stdin , "Number", value);
		if (value == (-2))
		{
			noMorePrompting = -1;
			return -1;
		}
	}
	
	return value;
}

/*-------------------------------------------------------------------------------*/

function PromptForWeights (promptString, wc,  midx)
{
	resMx = {wc,1};
	if (noMorePrompting)
	{
		return resMx;
	}
	
	wci	  = 0;
	currentMax = 1;
		
	for (widx = 0; (widx < wc-1)&&(currentMax>0); widx = widx+1)
	{
		resMx[wci] = PromptForAParameter (promptString[wci], 0, currentMax, midx);
		if (resMx[wci] == (-1))
		{
			return resMx;
		}
		currentMax = currentMax - resMx[wci];
		wci = wci+1;
	}
	
	
	leftOver = (1-resMx[0]);
	
	for (widx = 1; widx < wc-1; widx = widx+1)
	{
		if (leftOver == 0)
		{
			return resMx;
		}
		resMx [widx] = resMx [widx]/leftOver;
		leftOver = leftOver*(1-resMx[widx]);
	}
	
	return resMx;
}

/*-------------------------------------------------------------------------------*/

function WriteSnapshot (modelIdx)
{
	likOF = LIKELIHOOD_FUNCTION_OUTPUT;
	LIKELIHOOD_FUNCTION_OUTPUT = 6;
	SPOOL_FILE = SUMMARY_FILE+"_MODEL_"+modelIdx+".nex";
	fprintf (SPOOL_FILE, CLEAR_FILE, lf);
	LIKELIHOOD_FUNCTION_OUTPUT = likOF;
	return 0;
}

/*-------------------------------------------------------------------------------*/

function PromptForModelParameter (modelIdx)
{
	noMorePrompting = 0;
	if (modelIdx == 0)
	{
		global P = PromptForAParameter ("Weight for the '0' rate class", 0, 1, modelIdx);
	}
	else
	{
		if (modelIdx == 1)
		{
			promptMx  = {{"Weight for the '0' rate class", "Weight for the '1' rate class"}};
			wtMx	  = PromptForWeights (promptMx, 3, modelIdx);
			global P1 = wtMx[0];
			global P2 = wtMx[1];
			global W = PromptForAParameter ("Adjustable rate value", 0, 10000 , modelIdx);
		}		
		else
		{
			if (modelIdx == 2)
			{
				promptMx  = {{"Weight for the lowest rate class", "Weight for the middle rate class"}};
				wtMx	  = PromptForWeights (promptMx, 3, modelIdx);
				global P1 = wtMx[0];
				global P2 = wtMx[1];

				global R_1 = PromptForAParameter ("Lowest Rate Value",  0,  10000 , modelIdx);
				global W1  = PromptForAParameter ("Middle Rate Value", R_1, 10000 , modelIdx);
				global R_2 = PromptForAParameter ("Highest Rate Value", W1, 10000 , modelIdx);

				R_1	= R_1/W1;
				R_2 = R_2/W1;
			}
			else
			{
				if (modelIdx == 3)
				{
					promptMx  = {{"Weight for the '0' rate class", "Weight for the '1/3' rate class", "Weight for the '2/3' rate class",  
								  "Weight for the '1' rate class"}};

					wtMx	  = PromptForWeights (promptMx, 5, modelIdx);
					global P1 = wtMx[0];
					global P2 = wtMx[1];
					global P3 = wtMx[2];
					global P4 = wtMx[3];
				}
				else
				{
					if ((modelIdx == 4)||(modelIdx == 5))
					{
						global alpha = PromptForAParameter ("Alpha parameter for the gamma distribution", 0.01, 100 , modelIdx);
						global beta  = PromptForAParameter ("Beta  parameter for the gamma distribution", 0.01, 200 , modelIdx);
						if (modelIdx == 5)
						{
							global alpha2 = PromptForAParameter ("Alpha parameter for the 2nd gamma distribution", 0.01, 100 , modelIdx);
						}
					}
					else
					{
						if ((modelIdx == 6)||(modelIdx == 7)||(modelIdx == 8)||(modelIdx == 9)||(modelIdx == 10)||(modelIdx == 14))
						{
							global betaP = PromptForAParameter ("P parameter for the beta distribution", 0.05, 85 , modelIdx);
							global betaQ = PromptForAParameter ("Q parameter for the beta distribution", 0.05, 85 , modelIdx);
							global P 	 = PromptForAParameter ("Mixture weight for the beta distribution", 0, 1, modelIdx);
							if (modelIdx == 7)
							{
								global W = PromptForAParameter ("Value for omega", 1, 10000 , modelIdx);
							}
							if ((modelIdx == 8)||(modelIdx == 9))
							{
								global alpha = PromptForAParameter ("Alpha parameter for the gamma distribution", 0.01, 100 , modelIdx);
							}
							if (modelIdx == 9)
							{
								global beta = PromptForAParameter ("Beta parameter for the gamma distribution", 0.01, 500 , modelIdx);
							}
							if (modelIdx == 10)
							{
								global mu 	 = PromptForAParameter ("mu parameter for the normal distribution", 0, 10000 , modelIdx);
								global sigma = PromptForAParameter ("sigma parameter for the normal distribution", 0.0001, 10000 , modelIdx);
							}
							if (modelIdx == 14)
							{
								global W := 1;
							}
						}
						else
						{
							if (modelIdx == 11)
							{
								promptMx  = {{"Weight for the '0' rate class", "Weight for the first normal in the mixture"}};
								wtMx	  = PromptForWeights (promptMx, 3, modelIdx);
								P 		  = wtMx[0];
								P1		  = wtMx[1];
								
								global mu 	  = PromptForAParameter ("mu parameter for the first     normal distribution", 0, 10000 , modelIdx);
								global sigma  = PromptForAParameter ("sigma parameter for the first  normal distribution", 0.0001, 10000 , modelIdx);
								global sigma1 = PromptForAParameter ("sigma parameter for the second normal distribution", 0.0001, 10000 , modelIdx);
							}
							else
							{
								if (modelIdx == 12)
								{
									promptMx  = {{"Weight for the first normal in the mixture", "Weight for the second normal in the mixture"}};
									wtMx	  = PromptForWeights (promptMx, 3, modelIdx);
									P 		  = wtMx[0];
									P1		  = wtMx[1];
							
									global mu 	  = PromptForAParameter ("mu parameter for the first     normal distribution", 0, 10000 , modelIdx);
									global sigma  = PromptForAParameter ("sigma parameter for the first  normal distribution", 0.0001, 10000 , modelIdx);
									global sigma1 = PromptForAParameter ("sigma parameter for the second normal distribution", 0.0001, 10000 , modelIdx);
									global sigma2 = PromptForAParameter ("sigma parameter for the third normal distribution", 0.0001, 10000 , modelIdx);
								}
								if (modelIdx == 13)
								{
									global alpha = PromptForAParameter ("Alpha parameter for the gamma distribution", 0.01, 100 , modelIdx);
									global beta  = PromptForAParameter ("Beta  parameter for the gamma distribution", 0.01, 200 , modelIdx);
									global betaP = PromptForAParameter ("P parameter for the beta distribution", 0.05, 85 , modelIdx);
									global betaQ = PromptForAParameter ("Q parameter for the beta distribution", 0.05, 85 , modelIdx);
								}
							}
						}
					}				
				}	
			}	
		}	
	}		
	return noMorePrompting;	
}

/*-------------------------------------------------------------------------------*/

function SetWDistribution (resp)
{
	if (rateType == 0)
	{
		global P = .5;
		P:<1;
		global W = 0.1;
		W :< 1;
		categFreqMatrix = {{P,1-P}};
		categRateMatrix = {{W,1}};
		category c = (2, categFreqMatrix , MEAN, ,categRateMatrix, 0, 1e25);
	}
	else
	{
		if (rateType == 1)
		{
			global P1 = 1/3;
			global P2 = 1/2;
			
			P1:<1;
			P2:<1;
			
			global W_1 = 0.1;
			W_1 :< 1;
			global W_2 = 5;
			W_2 :> 1;
			categFreqMatrix = {{P1,(1-P1)*P2, (1-P1)*(1-P2)}} ;
			categRateMatrix = {{W_1,1,W_2}};
			category c = (3, categFreqMatrix , MEAN, ,categRateMatrix, 0, 1e25);
		}		
		else
		{
			if (rateType == 2)
			{
				global P1 = 1/3;
				global P2 = .5;
				P1:<1;
				P2:<1;
				global W1 = .25;
				global R_1 = .5;
				global R_2 = 3;
				R_1:<1;
				R_2:>1;
				categFreqMatrix = {{P1,(1-P1)*P2, (1-P1)*(1-P2)}} ;
				categRateMatrix = {{W1*R_1,W1,W1*R_2}};
				category c = (3, categFreqMatrix , MEAN, ,categRateMatrix, 0, 1e25);				
			}
			else
			{
				if (rateType == 3)
				{
					global P1 = 1/5;
					global P2 = 1/4;
					global P3 = 1/3;
					global P4 = 1/2;
					
					P1:<1;
					P2:<1;
					P3:<1;
					P4:<1;
					
					categFreqMatrix = {{P1,
										(1-P1)P2,
										(1-P1)(1-P2)*P3,
										(1-P1)(1-P2)(1-P3)P4,
										(1-P1)(1-P2)(1-P3)(1-P4)}} ;
					categRateMatrix = {{0,1/3,2/3,1,3}};
					category c = (5, categFreqMatrix , MEAN, ,categRateMatrix, 0, 1e25);				
				}
				else
				{
					if (rateType == 4)
					{
						global alpha = .5;
						global beta = 1;
						alpha:>0.01;alpha:<100;
						beta:>0.01;
						beta:<200;
						category c = (resp, EQUAL, MEAN, GammaDist(_x_,alpha,beta), CGammaDist(_x_,alpha,beta), 0 , 
				 			 		  1e25,CGammaDist(_x_,alpha+1,beta)*alpha/beta);
					}
					else
					{
						if (rateType == 5)
						{
							global alpha = .5;
							global beta  =  1;
							global alpha2=  .75;
							global P	 = .5; 
							alpha:>0.01;alpha:<100;
							beta:>0.01;
							beta:<200;
							P:<1;
							alpha2:>0.01;alpha2:<100;
							category c = (resp, EQUAL, MEAN, P*GammaDist(_x_,alpha,beta) + (1-P)*GammaDist(_x_,alpha2,alpha2)
														   , P*CGammaDist(_x_,alpha,beta) + (1-P)*CGammaDist(_x_,alpha2,alpha2), 
														   0 , 1e25,
														   P*CGammaDist(_x_,alpha+1,beta)*alpha/beta + (1-P)*CGammaDist(_x_,alpha2+1,alpha2));
						}
						else
						{
							if (rateType == 6)
							{
								global betaP = 1;
								global betaQ = 1;
								betaP:>0.05;betaP:<85;
								betaQ:>0.05;betaQ:<85;
								category c = (resp, EQUAL, MEAN, _x_^(betaP-1)*(1-_x_)^(betaQ-1)/Beta(betaP,betaQ), IBeta(_x_,betaP,betaQ), 0 , 
						 			 		  1,IBeta(_x_,betaP+1,betaQ)*betaP/(betaP+betaQ));
							}
							else
							{
								if (rateType == 7 || rateType == 14)
								{
									global W = 2;
									W:>1;
									global P	 = 1-1/(resp+1);
									global betaP = 1;
									global betaQ = 2;
									betaP:>0.1;
									betaQ:>0.1;
									betaP:<85;
									betaQ:<85;
									P:>0.0000001;
									P:<0.9999999;
									categFreqMatrix = {resp+1,1};
									for (k=0; k<resp; k=k+1)
									{
										categFreqMatrix[k]:=P/resp__;
									}
									categFreqMatrix[resp]:=(1-P);

									if (rateType == 14)
									{
										W:=1;
										category c = (resp+1, categFreqMatrix, MEAN, 
														P*_x_^(betaP-1)*(1-Min(_x_,1))^(betaQ-1)/Beta(betaP,betaQ)+W-W, 
														P*IBeta(Min(_x_,1),betaP,betaQ)+(1-P)*(_x_>=W), 
														0,1,
														P*IBeta(Min(_x_,1),betaP+1,betaQ)*betaP/(betaP+betaQ)+(1-P)*W*(_x_>=W));
									}
									else
									{
										category c = (resp+1, categFreqMatrix, MEAN, 
														P*_x_^(betaP-1)*(1-Min(_x_,1))^(betaQ-1)/Beta(betaP,betaQ)+W-W, 
														P*IBeta(Min(_x_,1),betaP,betaQ)+(1-P)*(_x_>=W), 
														0,1e25,
														P*IBeta(Min(_x_,1),betaP+1,betaQ)*betaP/(betaP+betaQ)+(1-P)*W*(_x_>=W));
									}
								}
								else
								{
									if (rateType == 8)
									{
										global P	 = .5;
										global betaP = 1;
										global betaQ = 2;
										betaP:>0.05;betaP:<85;
										betaQ:>0.05;betaQ:<85;
										global alpha = .5;
										global beta  = 1;
										alpha:>0.01;alpha:<100;
										beta:>0.01;										
										beta:<200;
										P:<1;
										category c = (resp, EQUAL, MEAN, 
															P*_x_^(betaP-1)*(1-Min(_x_,1))^(betaQ-1)/Beta(betaP,betaQ)+(1-P)*GammaDist(_x_,alpha,beta), 
															P*IBeta(Min(_x_,1),betaP,betaQ)+(1-P)*CGammaDist(_x_,alpha,beta), 
															0,1e25,
															P*betaP/(betaP+betaQ)*IBeta(Min(_x_,1),betaP+1,betaQ)+(1-P)*alpha/beta*CGammaDist(_x_,alpha+1,beta));
									}	
									else
									{
										if (rateType == 9)
										{
											global P	 = .5;
											P:<1;
											global betaP = 1;
											betaP:>0.05;betaP:<85;
											global betaQ = 2;
											betaQ:>0.05;betaQ:<85;
											global alpha = .5;
											alpha:>0.01;alpha:<100;
											global beta  = 1;
											beta:>0.01;beta:<500;
											category c = (resp, EQUAL, MEAN, 
																P*_x_^(betaP-1)*(1-Min(_x_,1))^(betaQ-1)/Beta(betaP,betaQ)+(1-P)*(_x_>1)*GammaDist(Max(1e-20,_x_-1),alpha,beta), 
																P*IBeta(Min(_x_,1),betaP,betaQ)+(1-P)*CGammaDist(Max(_x_-1,0),alpha,beta), 
																0,1e25,
																P*betaP/(betaP+betaQ)*IBeta(Min(_x_,1),betaP+1,betaQ)+
																		(1-P)*(alpha/beta*CGammaDist(Max(0,_x_-1),alpha+1,beta)+CGammaDist(Max(0,_x_-1),alpha,beta)));
										}				
										else
										{
											if (rateType == 10)
											{
												global P	 = .5;
												global betaP = 1;
												global betaQ = 2;
												betaP:>0.05;
												betaQ:>0.05;
												betaP:<85;
												betaQ:<85;
												global mu = 3;
												global sigma  = .01;
												sigma:>0.0001;
												sqrt2pi = Sqrt(8*Arctan(1));
												P:<1;

												category c = (resp, EQUAL, MEAN, 
																P*_x_^(betaP-1)*(1-Min(_x_,1))^(betaQ-1)/Beta(betaP,betaQ)+
																	(1-P)*(_x_>=1)*Exp(-(_x_-mu)(_x_-mu)/(2*sigma*sigma))/(sqrt2pi__*sigma)/ZCDF((mu-1)/sigma), 
																P*IBeta(Min(_x_,1),betaP,betaQ)+(1-P)*(_x_>=1)*(1-ZCDF((mu-_x_)/sigma)/ZCDF((mu-1)/sigma)), 
																0,1e25,
																P*betaP/(betaP+betaQ)*IBeta(Min(_x_,1),betaP+1,betaQ)+
																(1-P)*(_x_>=1)*(mu*(1-ZCDF((1-mu)/sigma)-ZCDF((mu-_x_)/sigma))+
																sigma*(Exp((mu-1)(1-mu)/(2*sigma*sigma))-Exp((_x_-mu)(mu-_x_)/(2*sigma*sigma)))/sqrt2pi__)/ZCDF((mu-1)/sigma));
											}				
											else
											{
												if (rateType == 11)
												{
													global P	 = 1/3;
													global P1    = .5;

													global mu = 3;
													global sigma  = .5;
													sigma:>0.0001;
													global sigma1  = 1;
													sigma1:>0.0001;

													sqrt2pi = Sqrt(8*Arctan(1));
													P:<1;
													P1:<1;
													
													categFreqMatrix = {resp+1,1};
													for (k=1; k<=resp; k=k+1)
													{
														categFreqMatrix[k]:=(1-P)/resp__;
													}
													categFreqMatrix[0]:=P;

													category c = (resp+1, categFreqMatrix, MEAN,
																	(1-P)((1-P1)*Exp(-(_x_-mu)(_x_-mu)/(2*sigma1*sigma1))/(sqrt2pi__*sigma1)/ZCDF(mu/sigma1)+
																			  P1*Exp(-(_x_-1)(_x_-1)/(2*sigma*sigma))/(sqrt2pi__*sigma)/ZCDF(1/sigma)), 
																	P+(1-P)(_x_>1e-20)((1-P1)(1-ZCDF((mu-_x_)/sigma1)/ZCDF(mu/sigma1))+
																						P1*(1-ZCDF((1-_x_)/sigma)/ZCDF(1/sigma))), 
																	0,1e25,
																	(1-P)((1-P1)(mu*(1-ZCDF(-mu/sigma1)-ZCDF((mu-_x_)/sigma1))+
																	sigma1*(Exp(-mu*mu/(2*sigma1*sigma1))-Exp((_x_-mu)(mu-_x_)/(2*sigma1*sigma1)))/sqrt2pi__)/ZCDF(mu/sigma1)+
																	P(1-ZCDF(-1/sigma)-ZCDF((1-_x_)/sigma)+
																	sigma*(Exp(-1/(2*sigma*sigma))-Exp((_x_-1)(1-_x_)/(2*sigma*sigma)))/sqrt2pi__)/ZCDF(1/sigma))
																 );
												}
												else		
												{
													if (rateType == 12)
													{
														global P	 = 1/3;
														global P1    = .5;

														global mu = 3;
														global sigma  = .25;
														global sigma1 = .5;
														global sigma2 = 1;
														sigma:>0.0001;
														sigma1:>0.0001;
														sigma2:>0.0001;

														sqrt2pi = Sqrt(8*Arctan(1));
														P:<1;
														P1:<1;

														category c = (resp, EQUAL , MEAN,
																		2*P*Exp(-_x_^2/(2*sigma*sigma))+
																		(1-P)((1-P1)*Exp((_x_-mu)(mu-_x_)/(2*sigma2*sigma2))/(sqrt2pi__*sigma2)/ZCDF(mu/sigma2)+
																			  P1*Exp((1-_x_)(_x_-1)/(2*sigma1*sigma1))/(sqrt2pi__*sigma1)/ZCDF(1/sigma1)), 
																		P*(1-2*ZCDF(-_x_/sigma))+
																		(1-P)((1-P1)(1-ZCDF((mu-_x_)/sigma2)/ZCDF(mu/sigma2))+
																			   P1*(1-ZCDF((1-_x_)/sigma1)/ZCDF(1/sigma1))), 
																		0,1e25,
																		2*P*sigma*(1-Exp(-_x_*_x_/(2*sigma*sigma)))/sqrt2pi__+
																		(1-P)((1-P1)(mu*(1-ZCDF(-mu/sigma2)-ZCDF((mu-_x_)/sigma2))+
																		sigma2*(Exp(-mu*mu/(2*sigma2*sigma2))-Exp((_x_-mu)(mu-_x_)/(2*sigma2*sigma2)))/sqrt2pi__)/ZCDF(mu/sigma2)+
																		P1(1-ZCDF(-1/sigma1)-ZCDF((1-_x_)/sigma1)+
																		sigma1*(Exp(-1/(2*sigma1*sigma1))-Exp((_x_-1)(1-_x_)/(2*sigma1*sigma1)))/sqrt2pi__)/ZCDF(mu/sigma1))
																		);
													}
													else
													{				
														if (rateType == 13)
														{
														
															global betaP = 1;
															global betaQ = 1;
															betaP:>0.05;betaP:<85;
															betaQ:>0.05;betaQ:<85;
															category pc = (resp-1, EQUAL, MEAN, 
																			_x_^(betaP-1)*(1-_x_)^(betaQ-1)/Beta(betaP,betaQ), /* density */
																			IBeta(_x_,betaP,betaQ), /*CDF*/
																			0, 				   /*left bound*/
																			1, 			   /*right bound*/
																			IBeta(_x_,betaP+1,betaQ)*betaP/(betaP+betaQ)
															);

															global alpha = .5;
															global beta = 1;
															alpha:>0.01;alpha:<100;
															beta:>0.01;
															beta:<200;
															category c = (resp, pc, MEAN, GammaDist(_x_,alpha,beta), CGammaDist(_x_,alpha,beta), 0 , 
													 			 		  1e25,CGammaDist(_x_,alpha+1,beta)*alpha/beta);
														}
													}
												}				
											}				
										}				
									}						
								}
							}
						}
					}				
				}	
			}	
		}	
	}		
	return 0;
}

/* ____________________________________________________________________________________________________________________*/

function FrameText (frameChar,vertChar,parOff,theText)
{
	h = Abs (theText)+4;
	fprintf (stdout,"\n");	
	for (k=0; k<parOff; k=k+1)
	{
		fprintf (stdout," ");
	}
	for (k=0; k<h;k=k+1)
	{
		fprintf (stdout,frameChar);
	}
	fprintf (stdout,"\n");	
	for (k=0; k<parOff; k=k+1)
	{
		fprintf (stdout," ");
	}
	fprintf (stdout,vertChar," ",theText," ",vertChar,"\n");
	for (k=0; k<parOff; k=k+1)
	{
		fprintf (stdout," ");
	}
	for (k=0; k<h;k=k+1)
	{
		fprintf (stdout,frameChar);
	}
	fprintf (stdout,"\n");	
	return 0;
}

/* ____________________________________________________________________________________________________________________*/

function BuildCodonFrequencies4 (obsF)
{
	PIStop = 1.0;
	result = {ModelMatrixDimension,1};
	hshift = 0;

	for (h=0; h<64; h=h+1)
	{
		first = h$16;
		second = h%16$4;
		third = h%4;
		if (_Genetic_Code[h]==10) 
		{
			hshift = hshift+1;
			PIStop = PIStop-obsF[first][0]*obsF[second][0]*obsF[third][0];
			continue; 
		}
		result[h-hshift][0]=obsF[first][0]*obsF[second][0]*obsF[third][0];
	}
	return result*(1.0/PIStop);
}

/* ____________________________________________________________________________________________________________________*/

function BuildCodonFrequencies12 (obsF)
{
	PIStop = 1.0;
	result = {ModelMatrixDimension,1};
	hshift = 0;

	for (h=0; h<64; h=h+1)
	{
		first = h$16;
		second = h%16$4;
		third = h%4;
		if (_Genetic_Code[h]==10) 
		{
			hshift = hshift+1;
			PIStop = PIStop-obsF[first][0]*obsF[second][1]*obsF[third][2];
			continue; 
		}
		result[h-hshift][0]=obsF[first][0]*obsF[second][1]*obsF[third][2];
	}
	return result*(1.0/PIStop);
}
/* ____________________________________________________________________________________________________________________*/


function GetDistributionParameters (sigLevel)
{
	GetInformation (distrInfo,c);
	D = Columns(distrInfo);
	E = 0.0;
	T = 0.0;
	sampleVar = 0.0;
	for (k=0; k<D; k=k+1)
	{
		T = distrInfo[0][k]*distrInfo[1][k];
		E = E+T;
		sampleVar = T*distrInfo[0][k]+sampleVar;
	}
	sampleVar = sampleVar-E*E;

	fprintf  (SUMMARY_FILE,"\n\n------------------------------------------------\n\ndN/dS = ",E, " (sample variance = ",sampleVar,")\n");
	for (k=0; k<D; k=k+1)
	{
		fprintf (SUMMARY_FILE,"\nRate[",Format(k+1,0,0),"]=",
				 Format(distrInfo[0][k],12,8), " (weight=", Format(distrInfo[1][k],9,7),")");
	}
	
	if (variable_dS)
	{
	    GetInformation (synDistrInfo,khs_synRate);
	    numcats_synRate = Columns(synDistrInfo);
	    synmean = 0.0;
	    T = 0.0;
	    sampleVar = 0.0;
	    for (k=0; k<numcats_synRate; k=k+1)
	    {
		T = synDistrInfo[0][k]*synDistrInfo[1][k];
		synmean = synmean+T;
		sampleVar = T*synDistrInfo[0][k]+sampleVar;
	    }
	    sampleVar = sampleVar-synmean*synmean;
	    fprintf  (SUMMARY_FILE,"\n\nMean synrate (all positions) = ",synmean, " (sample variance = ",sampleVar,")\n");
	    for (k=0; k<numcats_synRate; k=k+1)
	    {
		fprintf (SUMMARY_FILE,"\nRate[",Format(k+1,0,0),"]=",
			 Format(synDistrInfo[0][k],12,8), " (weight=", Format(synDistrInfo[1][k],9,7),")");
	    }
	}


	for (k=0; k<D; k=k+1)
	{
		if (distrInfo[0][k]>1) break;
	}
	ConstructCategoryMatrix(site_likelihoods,lf,COMPLETE);
	CC = Columns (site_likelihoods);
	DD = Rows (site_likelihoods);
	/* set up variable order information:
           (ordernum[n] is the number of the nth variable in site_likelihoods, 
           where omega and khs_synRate are variables 0 and 1 respectively): */
	GetInformation (order_ID, lf);
	if (variable_dS == 0)
	{
	    ordernum = {{0}};
	    order0 = 0;
	} else {
	    if (order_ID[0] == "c")
	    {
		ordernum = {{0,1}};
	    } else {
		ordernum = {{1,0}};
	    }
	    order0 = ordernum[0];
	    order1 = ordernum[1];
	}

	if (k<D)
	/* have rates > 1 */
	{
	    fprintf  (SUMMARY_FILE,"\n\n------------------------------------------------\n\n Sites with dN/dS > 1 (Posterior cutoff = ",sigLevel,")\n\n");
	}
	else
	{
	    fprintf  (SUMMARY_FILE,"\n\n------------------------------------------------\n\n No rate classes with dN/dS>1.");
	}

	/* allocate memory for data structures: */
	site_posteriors = {DD,CC};
	negprobs = {1,CC}; /* vector for storing site-specific negative selection probs */
	omega_marginals = {D,CC};
	synrate_marginals = {numcats_synRate,CC};

	for (v=0; v<CC; v=v+1)
	{
	    totProb = 0;
	    positiveProb = 0;
	    negativeProb = 0;

            /* initialise index and dimensions variables: */
	    indexes = {{0, 0}};
	    if (variable_dS == 0)
	    {
		dimensions = {{D}};
	    } else {
		dimensions = {{D, numcats_synRate}};
	    }   

            /* loop through all category combinations */
	    for (h=0; h<DD; h=h+1) 
	    {
		idx0 = indexes[0];
		idx1 = indexes[1];

		if (variable_dS == 0)
		{
		    site_posteriors[h][v] = distrInfo[1][h]*site_likelihoods[h][v];
		}
		if (variable_dS == 1)
		{
		    site_posteriors[h][v] = distrInfo[1][idx0]*synDistrInfo[1][idx1]*site_likelihoods[h][v];
		}
		totProb = totProb + site_posteriors[h][v]; /* accumulate conditional posteriors, summing to probability of data at this site */

		/* Accumulate all marginals of interest: */
		if (distrInfo[0][idx0]>1) /* positive AA selection category */
		{
		    positiveProb = positiveProb + site_posteriors[h][v];
		}
		if (distrInfo[0][idx0]<1) /* purifying AA selection category */
		{
		    negativeProb = negativeProb + site_posteriors[h][v];
		}
		if (variable_dS == 1)
		{
		    omega_marginals[idx0][v] = omega_marginals[idx0][v] + site_posteriors[h][v];
		    synrate_marginals[idx1][v] = synrate_marginals[idx1][v] + site_posteriors[h][v];
		}

		/* keep track of individual category variable indices: */
		if (variable_dS == 0)
		{
		    indexes[0] = indexes[0]+1;
		} else {
		    indexes[order1] = indexes[order1]+1;
		    if (indexes[order1] == dimensions[order1])
		    {
			indexes[order1] = 0;
			indexes[order0] = indexes[order0]+1;
		    }
		}
	    } /* for h */

	    positiveProb = positiveProb/totProb;
	    negprobs[0][v] = negativeProb/totProb;
	    if (variable_dS == 1)
	    {
		for (h = 0; h < D; h=h+1)
		{
		    omega_marginals[h][v] = omega_marginals[h][v]/totProb;
		}
		for (h = 0; h < numcats_synRate; h=h+1)
		{
		    synrate_marginals[h][v] = synrate_marginals[h][v]/totProb;
		}
	    }

	    for (h=0; h<DD; h=h+1)
	    {
		site_posteriors[h][v] = site_posteriors[h][v]/totProb;
	    }
	    if (positiveProb>=sigLevel)
	    {
		fprintf (SUMMARY_FILE,Format (v+1,0,0)," (",positiveProb,")\n");
	    }
	} /* for v */

	fprintf  (SUMMARY_FILE,"\n\n------------------------------------------------\n\n Sites with dN/dS < 1 (Posterior cutoff = ",sigLevel,")\n\n");
	for (v=0; v<CC; v=v+1)
	{
	    if (negprobs[0][v]>=sigLevel)
	    {
		fprintf (SUMMARY_FILE,Format (v+1,0,0)," (",negprobs[0][v],")\n");
	    }
	}

/* tabulate posteriors for omega and synRate rate classes */

	fprintf  (SUMMARY_FILE,"\n\n------------------------------------------------\n\nTabulated Posteriors for Each Site\nSites in Columns, Rate Classes in Rows\nImport the following part into a data processing program\nfor further analysis\n\n");
	if (variable_dS == 0)
	{
	    outString = "Omega/Site";
	    for (v=0; v<D; v=v+1)
	    {
		outString = outString + "\tOmega=" + distrInfo[0][v];
	    }	
	    
	    for (v=0; v<CC; v=v+1)
	    {
		outString = outString + "\n" + (v+1);
		for (h=0; h<D; h=h+1)
		{
		    outString = outString + "\t" + site_posteriors[h][v];
		}
	    }
	    
	    fprintf  (SUMMARY_FILE,"\n", outString,"\n");
	}	    

	if (variable_dS == 1)
	{
	    outString = "Rates/Site";
	    for (v=0; v<D; v=v+1)
	    {
		outString = outString + "\tOmega=" + distrInfo[0][v];
	    }	
	    for (v=0; v<numcats_synRate; v=v+1)
	    {
		outString = outString + "\tS=" + synDistrInfo[0][v];
	    }	
	    
	    for (v=0; v<CC; v=v+1)
	    {
		outString = outString + "\n" + (v+1);
		for (h=0; h<D; h=h+1)
		{
		    outString = outString + "\t" + omega_marginals[h][v];
		}
		for (h=0; h<numcats_synRate; h=h+1)
		{
		    outString = outString + "\t" + synrate_marginals[h][v];
		}
	    }
	    
	    fprintf  (SUMMARY_FILE,"\n", outString,"\n");
	}	    

	site_posteriors = 0;

	return E;
}


/* ____________________________________________________________________________________________________________________*/

function PopulateModelMatrix (ModelMatrixName&, EFV)
{
	ModelMatrixName = {ModelMatrixDimension,ModelMatrixDimension}; 

	hshift = 0;
	
	if (modelType==0)
	{
		for (h=0; h<64; h=h+1)
		{
			if (_Genetic_Code[h]==10) 
			{
				hshift = hshift+1;
				continue; 
			}
			vshift = hshift;
			for (v = h+1; v<64; v=v+1)
			{
				diff = v-h;
				if (_Genetic_Code[v]==10) 
				{
					vshift = vshift+1;
					continue; 
				}
			  	if ((h$4==v$4)||((diff%4==0)&&(h$16==v$16))||(diff%16==0))
			  	{
			  		if (h$4==v$4)
			  		{
			  			transition = v%4;
			  			transition2= h%4;
			  		}
			  		else
			  		{
			  			if(diff%16==0)
			  			{
			  				transition = v$16;
			  				transition2= h$16;
			  			}
			  			else
			  			{
			  				transition = v%16$4;
			  				transition2= h%16$4;
			  			}
			  		}
			  		if (_Genetic_Code[0][h]==_Genetic_Code[0][v]) 
			  		{
			  			ModelMatrixName[h-hshift][v-vshift] := t*EFV__[transition__];
			  			ModelMatrixName[v-vshift][h-hshift] := t*EFV__[transition2__];
				  	}
			  		else
			  		{
				  		ModelMatrixName[h-hshift][v-vshift] := c*t*EFV__[transition__];
			  			ModelMatrixName[v-vshift][h-hshift] := c*t*EFV__[transition2__];
		  			}
			  	}
			  }
		}
	}
	else
	{
		if (modelType==1)
		{
			for (h=0; h<64; h=h+1)
			{
				if (_Genetic_Code[h]==10) 
				{
					hshift = hshift+1;
					continue; 
				}
				vshift = hshift;
				for (v = h+1; v<64; v=v+1)
				{
					diff = v-h;
					if (_Genetic_Code[v]==10) 
					{
						vshift = vshift+1;
						continue; 
					}
					nucPosInCodon = 2;
				  	if ((h$4==v$4)||((diff%4==0)&&(h$16==v$16))||(diff%16==0))
				  	{
				  		if (h$4==v$4)
				  		{
				  			transition = v%4;
				  			transition2= h%4;
				  		}
				  		else
				  		{
				  			if(diff%16==0)
				  			{
				  				transition = v$16;
				  				transition2= h$16;
								nucPosInCodon = 0;
				  			}
				  			else
				  			{
				  				transition = v%16$4;
				  				transition2= h%16$4;
								nucPosInCodon = 1;
				  			}
				  		}
				  		if (_Genetic_Code[0][h]==_Genetic_Code[0][v]) 
				  		{
				  			ModelMatrixName[h-hshift][v-vshift] := t*EFV__[transition__][nucPosInCodon__];
				  			ModelMatrixName[v-vshift][h-hshift] := t*EFV__[transition2__][nucPosInCodon__];
					  	}
				  		else
				  		{
					  		ModelMatrixName[h-hshift][v-vshift] := c*t*EFV__[transition__][nucPosInCodon__];
				  			ModelMatrixName[v-vshift][h-hshift] := c*t*EFV__[transition2__][nucPosInCodon__];
			  			}
				  	}
				  }
			}
		}
		else
		{
			if (modelType<4)
			{
/* khs: added khs_synRate to equations for this case: either constrained to 1 (const
	synrate) or drawn from discrete, unit-mean distribution with 3 categories
	(variable synrate). I don't see where the existing synRate variable
	is defined, but it is used in modelType >= 4 case, so I am using a
	different variable name in case. */

				for (h=0; h<64; h=h+1)
				{
					if (_Genetic_Code[h]==10) 
					{
						hshift = hshift+1;
						continue; 
					}
					vshift = hshift;
					for (v = h+1; v<64; v=v+1)
					{
						diff = v-h;
						if (_Genetic_Code[v]==10) 
						{
							vshift = vshift+1;
							continue; 
						}
					  	if ((h$4==v$4)||((diff%4==0)&&(h$16==v$16))||(diff%16==0))
					  	{
					  		if (h$4==v$4)
					  		{
					  			transition = v%4;
					  			transition2= h%4;
					  		}
					  		else
					  		{
					  			if(diff%16==0)
					  			{
					  				transition = v$16;
					  				transition2= h$16;
					  			}
					  			else
					  			{
					  				transition = v%16$4;
					  				transition2= h%16$4;
					  			}
					  		}
					  		if (_Genetic_Code[0][h]==_Genetic_Code[0][v]) 
					  		{
					  			if (Abs(transition-transition2)%2)
					  			{
					  				ModelMatrixName[h-hshift][v-vshift] := kappa*khs_synRate*t;
					  				ModelMatrixName[v-vshift][h-hshift] := kappa*khs_synRate*t;
					  			}
					  			else
					  			{
					  				ModelMatrixName[h-hshift][v-vshift] := khs_synRate*t;
					  				ModelMatrixName[v-vshift][h-hshift] := khs_synRate*t;
					  			}
					  			
						  	}
					  		else
					  		{
					  			if (Abs(transition-transition2)%2)
					  			{
					  				ModelMatrixName[h-hshift][v-vshift] := kappa*c*khs_synRate*t;
					  				ModelMatrixName[v-vshift][h-hshift] := kappa*c*khs_synRate*t;
					  			}
					  			else
					  			{
					  				ModelMatrixName[h-hshift][v-vshift] := c*khs_synRate*t;
					  				ModelMatrixName[v-vshift][h-hshift] := c*khs_synRate*t;
					  			}
						  	}
					  	}	
					 }
				}	
			}
			else
			{
				for (h=0; h<64; h=h+1)
				{
					if (_Genetic_Code[h]==10) 
					{
						hshift = hshift+1;
						continue; 
					}
					vshift = hshift;
					for (v = h+1; v<64; v=v+1)
					{
						diff = v-h;
						if (_Genetic_Code[v]==10) 
						{
							vshift = vshift+1;
							continue; 
						}
						nucPosInCodon = 2;
					  	if ((h$4==v$4)||((diff%4==0)&&(h$16==v$16))||(diff%16==0))
					  	{
					  		if (h$4==v$4)
					  		{
					  			transition = v%4;
					  			transition2= h%4;
					  		}
					  		else
					  		{
					  			if(diff%16==0)
					  			{
					  				transition = v$16;
					  				transition2= h$16;
									nucPosInCodon = 0;
					  			}
					  			else
					  			{
					  				transition = v%16$4;
					  				transition2= h%16$4;
									nucPosInCodon = 1;
					  			}
					  		}
					  		
					  		if (modelType == 4)
					  		{
					  			nucPosInCodon = 0;
					  		}
					  		
					  		rateKind = mSpecMatrix[transition][transition2];
					  		
					  		if (rateKind == 1)
					  		{
						  		if (_Genetic_Code[0][h]==_Genetic_Code[0][v]) 
						  		{
						  			ModelMatrixName[h-hshift][v-vshift] := AC*synRate*EFV__[transition__][nucPosInCodon__];
						  			ModelMatrixName[v-vshift][h-hshift] := AC*synRate*EFV__[transition2__][nucPosInCodon__];
							  	}
						  		else
						  		{
							  		ModelMatrixName[h-hshift][v-vshift] := AC*c*synRate*EFV__[transition__][nucPosInCodon__];
						  			ModelMatrixName[v-vshift][h-hshift] := AC*c*synRate*EFV__[transition2__][nucPosInCodon__];
					  			}
					  		}
					  		else
					  		{
						  		if (rateKind == 2)
						  		{
							  		if (_Genetic_Code[0][h]==_Genetic_Code[0][v]) 
							  		{
							  			ModelMatrixName[h-hshift][v-vshift] := synRate*EFV__[transition__][nucPosInCodon__];
							  			ModelMatrixName[v-vshift][h-hshift] := synRate*EFV__[transition2__][nucPosInCodon__];
								  	}
							  		else
							  		{
								  		ModelMatrixName[h-hshift][v-vshift] := c*synRate*EFV__[transition__][nucPosInCodon__];
							  			ModelMatrixName[v-vshift][h-hshift] := c*synRate*EFV__[transition2__][nucPosInCodon__];
						  			}
						  		}
						  		else
						  		{
							  		if (rateKind == 3)
							  		{
								  		if (_Genetic_Code[0][h]==_Genetic_Code[0][v]) 
								  		{
								  			ModelMatrixName[h-hshift][v-vshift] := AT*synRate*EFV__[transition__][nucPosInCodon__];
								  			ModelMatrixName[v-vshift][h-hshift] := AT*synRate*EFV__[transition2__][nucPosInCodon__];
									  	}
								  		else
								  		{
									  		ModelMatrixName[h-hshift][v-vshift] := AT*c*synRate*EFV__[transition__][nucPosInCodon__];
								  			ModelMatrixName[v-vshift][h-hshift] := AT*c*synRate*EFV__[transition2__][nucPosInCodon__];
							  			}
							  		}
							  		else
							  		{
								  		if (rateKind == 4)
								  		{
									  		if (_Genetic_Code[0][h]==_Genetic_Code[0][v]) 
									  		{
									  			ModelMatrixName[h-hshift][v-vshift] := CG*synRate*EFV__[transition__][nucPosInCodon__];
									  			ModelMatrixName[v-vshift][h-hshift] := CG*synRate*EFV__[transition2__][nucPosInCodon__];
										  	}
									  		else
									  		{
										  		ModelMatrixName[h-hshift][v-vshift] := CG*c*synRate*EFV__[transition__][nucPosInCodon__];
									  			ModelMatrixName[v-vshift][h-hshift] := CG*c*synRate*EFV__[transition2__][nucPosInCodon__];
								  			}
								  		}
								  		else
								  		{
									  		if (rateKind == 5)
									  		{
										  		if (_Genetic_Code[0][h]==_Genetic_Code[0][v]) 
										  		{
										  			ModelMatrixName[h-hshift][v-vshift] := CT*synRate*EFV__[transition__][nucPosInCodon__];
										  			ModelMatrixName[v-vshift][h-hshift] := CT*synRate*EFV__[transition2__][nucPosInCodon__];
											  	}
										  		else
										  		{
											  		ModelMatrixName[h-hshift][v-vshift] := CT*c*synRate*EFV__[transition__][nucPosInCodon__];
										  			ModelMatrixName[v-vshift][h-hshift] := CT*c*synRate*EFV__[transition2__][nucPosInCodon__];
									  			}
									  		}
									  		else
									  		{
										  		if (_Genetic_Code[0][h]==_Genetic_Code[0][v]) 
										  		{
										  			ModelMatrixName[h-hshift][v-vshift] := GT*synRate*EFV__[transition__][nucPosInCodon__];
										  			ModelMatrixName[v-vshift][h-hshift] := GT*synRate*EFV__[transition2__][nucPosInCodon__];
											  	}
										  		else
										  		{
											  		ModelMatrixName[h-hshift][v-vshift] := GT*c*synRate*EFV__[transition__][nucPosInCodon__];
										  			ModelMatrixName[v-vshift][h-hshift] := GT*c*synRate*EFV__[transition2__][nucPosInCodon__];
									  			}
									  		}
									  	}
								  	}
							  	}
						  	}
					  	}
					}
				}	
			}
		}
	 }
	 return ((modelType>1)&&(modelType<4));
}

/* ____________________________________________________________________________________________________________________*/

NICETY_LEVEL = 3;

skipCodeSelectionStep = 1;
ExecuteAFile (HYPHY_LIB_DIRECTORY+"TemplateBatchFiles/TemplateModels/chooseGeneticCode.def");
dummy = ApplyGeneticCodeTable (0);

ModelMatrixDimension = 64;
for (h = 0 ;h<64; h=h+1)
{
	if (_Genetic_Code[h]==10)
	{
		ModelMatrixDimension = ModelMatrixDimension-1;
	}
}

DataSet ds = ReadDataFile (INPUTFILE);
DataSetFilter FullData = CreateFilter (ds,3,"","",GeneticCodeExclusions);

/* Data partitions read from breakpointlist: */
for (partNum = 1; partNum<=numParts; partNum=partNum+1)
{
	ExecuteCommands("DataSetFilter p"+partNum+" = CreateFilter (ds,3,breakpointlist[partNum-1],\"\",GeneticCodeExclusions);");
}

/* Single partition, but only the data contained in breakpointlist: */
partString = breakpointlist[0];
for (partNum = 1; partNum<numParts; partNum=partNum+1)
{
	partString = partString+","+breakpointlist[partNum];
}	
fprintf(stdout, "Partitions: ");
fprintf(stdout, partString);
fprintf(stdout, "\n\n");
DataSetFilter PartData = CreateFilter (ds,3,partString,"",GeneticCodeExclusions);

dummyVar = FrameText ("-","|",2,"READ THE FOLLOWING DATA");

fprintf (stdout,"\n",ds);

chosenModelList = {16,1};
chosenModelList[0][0] = 1; /* Single Rate */
chosenModelList[1][0] = 1; /* Neutral */
chosenModelList[2][0] = 1; /* Selection */
chosenModelList[3][0] = 0; /* Discrete */
chosenModelList[4][0] = 0; /* Freqs */
chosenModelList[5][0] = 0; /* Gamma */
chosenModelList[6][0] = 0; /* 2 Gamma */
chosenModelList[7][0] = 0; /* Beta */
chosenModelList[8][0] = 0; /* Beta & w */
chosenModelList[9][0] = 0; /* Beta & Gamma */
chosenModelList[10][0] = 0; /* Beta & (Gamma+1) */
chosenModelList[11][0] = 0; /* Beta & (Normal>1) */
chosenModelList[12][0] = 0; /* 0 & 2 (Normal>1) */
chosenModelList[13][0] = 0; /* 3 Normal */
chosenModelList[14][0] = 0; /* Two parameter gamma partitioned by a two parameter beta */
chosenModelList[15][0] = 0; /* Discrete beta and a point mass at one. The M8a model from PAML. */

modelType = 3; /* GY model */
HarvestFrequencies (observedFreq,FullData,3,1,1);
vectorOfFrequencies = BuildCodonFrequencies12 (observedFreq);
global kappa = 1.;

psigLevel = .95;
fprintf (stdout, "\nUsing ", psigLevel , " cutoff for a site to be considered under selective pressure.\n");

categCount = 10;

fprintf (stdout, "Using ", Format (categCount,0,0), " categories.\n");

fix_brlens = 1;
ClearConstraints (khs_synRate);
global khs_synRate;
numdscats = 3;
if (variable_dS) 
{
	if (numdscats == 3) 
	{
		global PS1 = 1/3;
		global PS2 = .5;
		PS1:<1;
		PS2:<1;
		global RS1 = .3;
		global RS3 = 1.5;
		RS1:<1; RS1:>0.000000001;
		RS3:>1; RS3:<100000;
		global synRate_scale := RS1*PS1 + (1-PS1)*PS2 + RS3*(1-PS1)*(1-PS2);
		categFreqMatrix = {{PS1,(1-PS1)*PS2, (1-PS1)*(1-PS2)}} ;
		categRateMatrix = {{RS1/synRate_scale,1/synRate_scale,RS3/synRate_scale}};
		category khs_synRate = (3, categFreqMatrix , MEAN, ,categRateMatrix, 0, 1e25);			
	} else if (numdscats == 4) {
		global PS1 = 1/4;
		global PS2 = 1/3;
		global PS3 = .5;
		PS1:<1;
		PS2:<1;
		PS3:<1;
		global RS1fac = .5;
		global RS2 = .6;
		global RS4 = 1.5;
		RS1fac:<1; RS1fac:>0.00001;
		RS2:<1; RS2:>0.00001;
		RS4:>1; RS4:<100000;
		global synRate_scale := RS1fac*RS2*PS1 + RS2*(1-PS1)*PS2 + (1-PS1)*(1-PS2)*PS3 + RS4*(1-PS1)*(1-PS2)*(1-PS3);
		categFreqMatrix = {{PS1,(1-PS1)*PS2, (1-PS1)*(1-PS2)*PS3, (1-PS1)*(1-PS2)*(1-PS3)}} ;
		categRateMatrix = {{RS1fac*RS2/synRate_scale,RS2/synRate_scale,1/synRate_scale,RS4/synRate_scale}};
		category khs_synRate = (4, categFreqMatrix , MEAN, ,categRateMatrix, 0, 1e25);			

	}
} 
else
{
	khs_synRate := 1;
}
				 
cachedBranchLengths = {{-1,-1}};
			
/* Single partition (full data): */
if (SingleFullPartition) {
fprintf(stdout, "Single partition (full data):\n");

dummyVar = FrameText ("-","|",2,"SUMMARY TABLE");
tableSeparator =  "+-------------------------+----------------+---------------+-----+\n";
fprintf (stdout, "\n\"p\" is the number of parameters in addition to the branch lengths.\nDetailed results including sites with dN/dS>1 will be written to\n",SUMMARY_FILE,"\n\n");
fprintf (stdout, tableSeparator,
				 "| MODEL (Number & Desc)   | Log likelihood | 	   dN/dS     |  p  |\n",
				 tableSeparator);

if (chosenModelList[0]>0)
{
	ClearConstraints (c);
	global c = 1.;
	rateType = -1;
	timer = Time(1);
	modelMatrix = 0;
	MULTIPLY_BY_FREQS = PopulateModelMatrix ("modelMatrix", observedFreq);
	Model theModel = (modelMatrix,vectorOfFrequencies,MULTIPLY_BY_FREQS);
	Tree TreeFullData = treeStringFullData;
	branchNames = BranchName (TreeFullData,-1);
	tvec = {Columns (branchNames)-1,1};
	LikelihoodFunction lf = (FullData, TreeFullData);
	timer = Time(1);
	Optimize (res,lf);
	dummy = ReceiveJobs (0);

	/* Store branch lengths:*/
	for (k = 0; k < Columns (branchNames)-1; k=k+1)
	{
		ExecuteCommands ("tvec[k]=TreeFullData."+branchNames[k]+".t;");
		fprintf(stdout, "  Branch ",k,": ",branchNames[k]," ",tvec[k]);
	}
	fprintf(stdout,"\n\n");
 
}

for (rateType = 0; rateType < 15; rateType = rateType + 1)
{
	if (chosenModelList[rateType+1]==0)
	{
		continue;
	}

	dummy = SetWDistribution (categCount);
	modelMatrix = 0;
	MULTIPLY_BY_FREQS = PopulateModelMatrix ("modelMatrix", observedFreq);
	Model theModel = (modelMatrix,vectorOfFrequencies,MULTIPLY_BY_FREQS);
	Tree TreeFullData = treeStringFullData;

	if (fix_brlens)
	{
	/* Fix branch lengths to previously estimated values: */
	   for (k = 0; k < Columns (branchNames)-1; k=k+1)
	   {
		ExecuteCommands ("TreeFullData."+branchNames[k]+".t:="+tvec[k]+";");
	   }
	}	   

	LikelihoodFunction lf = (FullData, TreeFullData);
	timer = Time(1);
	Optimize (res,lf);
	if (rateType == 0) /* neutral */
	{
	   lik_neut = res[1][0];
	}	
	if (rateType == 1) /* selection */
	{	
	   lik_sel = res[1][0];
	}

	dummy = ReceiveJobs (0);
	kappa = 2.;

}
lnlikDelta = 2*(lik_sel - lik_neut);
fprintf(stdout,"lnlikDelta = ",lnlikDelta,"\n");
pValue = 1-CChi2(lnlikDelta,2);
fprintf(stdout,"P-value for positive selection (M1 vs M2): ",pValue,"\n");
}

/* Single partition (partial data): */
if (SinglePartition) {
fprintf(stdout, "Single partition (partial data):\n");

dummyVar = FrameText ("-","|",2,"SUMMARY TABLE");
tableSeparator =  "+-------------------------+----------------+---------------+-----+\n";
fprintf (stdout, "\n\"p\" is the number of parameters in addition to the branch lengths.\nDetailed results including sites with dN/dS>1 will be written to\n",SUMMARY_FILE,"\n\n");
fprintf (stdout, tableSeparator,
				 "| MODEL (Number & Desc)   | Log likelihood | 	   dN/dS     |  p  |\n",
				 tableSeparator);

if (chosenModelList[0]>0)
{
	ClearConstraints (c);
	global c = 1.;
	rateType = -1;
	timer = Time(1);
	modelMatrix = 0;
	MULTIPLY_BY_FREQS = PopulateModelMatrix ("modelMatrix", observedFreq);
	Model theModel = (modelMatrix,vectorOfFrequencies,MULTIPLY_BY_FREQS);
	Tree TreePartData = treeStringPartData;
	branchNames = BranchName (TreePartData,-1);
	tvec = {Columns (branchNames)-1,1};
	LikelihoodFunction lf = (PartData, TreePartData);
	timer = Time(1);
	Optimize (res,lf);
	dummy = ReceiveJobs (0);

	/* Store branch lengths:*/
	for (k = 0; k < Columns (branchNames)-1; k=k+1)
	{
		ExecuteCommands ("tvec[k]=TreePartData."+branchNames[k]+".t;");
		fprintf(stdout, "  Branch ",k,": ",branchNames[k]," ",tvec[k]);
	}
	fprintf(stdout,"\n\n");
 
}

for (rateType = 0; rateType < 15; rateType = rateType + 1)
{
	if (chosenModelList[rateType+1]==0)
	{
		continue;
	}

	dummy = SetWDistribution (categCount);
	modelMatrix = 0;
	MULTIPLY_BY_FREQS = PopulateModelMatrix ("modelMatrix", observedFreq);
	Model theModel = (modelMatrix,vectorOfFrequencies,MULTIPLY_BY_FREQS);
	Tree TreePartData = treeStringPartData;

	if (fix_brlens)
	{
	/* Fix branch lengths to previously estimated values: */
	   for (k = 0; k < Columns (branchNames)-1; k=k+1)
	   {
		ExecuteCommands ("TreePartData."+branchNames[k]+".t:="+tvec[k]+";");
	   }
	}	   

	LikelihoodFunction lf = (PartData, TreePartData);
	timer = Time(1);
	Optimize (res,lf);
	if (rateType == 0) /* neutral */
	{
	   lik_neut = res[1][0];
	}	
	if (rateType == 1) /* selection */
	{	
	   lik_sel = res[1][0];
	}

	dummy = ReceiveJobs (0);
	kappa = 2.;

}
lnlikDelta = 2*(lik_sel - lik_neut);
fprintf(stdout,"lnlikDelta = ",lnlikDelta,"\n");
pValue = 1-CChi2(lnlikDelta,2);
fprintf(stdout,"P-value for positive selection (M1 vs M2): ",pValue,"\n");
}

/* Separate partitions: */
if (Separate_Partitions) {
fprintf(stdout, "Separate partitions:\n");

dummyVar = FrameText ("-","|",2,"SUMMARY TABLE");
tableSeparator =  "+-------------------------+----------------+---------------+-----+\n";
fprintf (stdout, "\n\"p\" is the number of parameters in addition to the branch lengths.\nDetailed results including sites with dN/dS>1 will be written to\n",SUMMARY_FILE,"\n\n");
fprintf (stdout, tableSeparator,
				 "| MODEL (Number & Desc)   | Log likelihood | 	   dN/dS     |  p  |\n",
				 tableSeparator);

if (chosenModelList[0]>0)
{
	ClearConstraints (c);
	global c = 1.;
	rateType = -1;
	timer = Time(1);
	modelMatrix = 0;
	MULTIPLY_BY_FREQS = PopulateModelMatrix ("modelMatrix", observedFreq);
	Model theModel = (modelMatrix,vectorOfFrequencies,MULTIPLY_BY_FREQS);
	for (partNum = 1; partNum<=numParts; partNum=partNum+1)
	{
		ExecuteCommands("Tree  Tree"+partNum+" = treeString"+partNum+";");
		ExecuteCommands("branchNames"+partNum+" = BranchName (Tree"+partNum+",-1);");
		ExecuteCommands("tvec"+partNum+" = {Columns (branchNames"+partNum+")-1,1};");
		ExecuteCommands ("numcols = Columns (branchNames"+partNum+")-1;");
		for (k = 0; k < numcols; k=k+1)
		{
			ExecuteCommands ("thisbranch = branchNames"+partNum+"[k];");
			ExecuteCommands ("tvec"+partNum+"[k]=Tree"+partNum+"."+thisbranch+".t;");
		}
	}		

	commandString = "LikelihoodFunction lf = (p1,Tree1";
	for (partNum = 2; partNum<=numParts; partNum=partNum+1)
	{
		commandString = commandString+",p"+partNum+",Tree"+partNum;
	}
	commandString = commandString+");";
	ExecuteCommands(commandString);

	timer = Time(1);
	Optimize (res,lf);
	dummy = ReceiveJobs (0);

	/* Store branch lengths:*/
	for (partNum = 1; partNum<=numParts; partNum=partNum+1)
	{
		ExecuteCommands ("numcols = Columns (branchNames"+partNum+")-1;");
		for (k = 0; k < numcols; k=k+1)
		{
			ExecuteCommands ("thisbranch = branchNames"+partNum+"[k];");
			ExecuteCommands ("tvec"+partNum+"[k]=Tree"+partNum+"."+thisbranch+".t;");
		}
	}

}

for (rateType = 0; rateType < 15; rateType = rateType + 1)
{
	if (chosenModelList[rateType+1]==0)
	{
		continue;
	}

	dummy = SetWDistribution (categCount);
	modelMatrix = 0;
	MULTIPLY_BY_FREQS = PopulateModelMatrix ("modelMatrix", observedFreq);
	Model theModel = (modelMatrix,vectorOfFrequencies,MULTIPLY_BY_FREQS);
	for (partNum = 1; partNum<=numParts; partNum=partNum+1)
	{
		ExecuteCommands("Tree  Tree"+partNum+" = treeString"+partNum+";");

		if (fix_brlens)
		{
/* Fix branch lengths to previously estimated values: */
		      ExecuteCommands ("numcols = Columns (branchNames"+partNum+")-1;");
		      for (k = 0; k < numcols; k=k+1)
		      {
				ExecuteCommands ("thisbranch = branchNames"+partNum+"[k];");
				ExecuteCommands ("thist = tvec"+partNum+"[k];");
				ExecuteCommands ("Tree"+partNum+"."+thisbranch+".t:="+thist+";");
		      }
		}

	}		

	commandString = "LikelihoodFunction lf = (p1,Tree1";
	for (partNum = 2; partNum<=numParts; partNum=partNum+1)
	{
		commandString = commandString+",p"+partNum+",Tree"+partNum;
	}
	commandString = commandString+");";
	ExecuteCommands(commandString);
	timer = Time(1);
	Optimize (res,lf);
	if (rateType == 0) /* neutral */
	{
	   lik_neut = res[1][0];
	}	
	if (rateType == 1) /* selection */
	{	
	   lik_sel = res[1][0];
	}

	dummy = ReceiveJobs (0);
	kappa = 2.;

}
lnlikDelta = 2*(lik_sel - lik_neut);
fprintf(stdout,"lnlikDelta = ",lnlikDelta,"\n");
pValue = 1-CChi2(lnlikDelta,2);
fprintf(stdout,"P-value for positive selection (M1 vs M2): ",pValue,"\n");
}
/* ____________________________________________________________________________________________________________________*/

function ReceiveJobs (sendOrNot)
{
	
	dummy = WriteSnapshot (rateType);
	
	if (rateType>=0)
	{
		fprintf (SUMMARY_FILE,"\n*** RUNNING MODEL ", Format(rateType+1,0,0), " (",ModelNames[rateType],") ***\n######################################\n");
		fprintf (SUMMARY_FILE,"\n>Done in ",Time(1)-timer, " seconds \n\n", lf);
		fprintf (stdout, "| ");
		if (rateType<9)
		{
			fprintf (stdout," ");
		}
		fprintf (stdout, Format (rateType+1,0,0), ". ", ModelNames[rateType]);
		for (dummy = Abs(ModelNames[rateType])+5; dummy<25; dummy = dummy+1)
		{
			fprintf (stdout," ");
		}
		dummy = GetDistributionParameters(psigLevel);
		fprintf (stdout,"| ",Format (res[1][0],14,6)," | ",Format (dummy,13,8)," |  ",
							 Format(ParameterCount[rateType],0,0),"  |\n",tableSeparator);
	}
	else
	{
		fprintf (SUMMARY_FILE,"\n*** RUNNING SINGLE RATE MODEL ***\n#################################\n");
		fprintf (SUMMARY_FILE,"\n>Done in ", Time(1)-timer, " seconds \n\n");
		fprintf (SUMMARY_FILE,lf,"\n\n-----------------------------------\n\ndN/dS = ",c,"\n\n");

		fprintf (stdout, "|  0. Single Rate Model   | ",Format (res[1][0],14,6)," | ",Format (c,13,8)," |  0  |\n",
					 		 tableSeparator);
	}
						 
	return fromNode-1;
}
