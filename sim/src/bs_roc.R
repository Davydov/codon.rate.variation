## 1) is no tree estimation worse?
## 2) is full tree estimation better?
## 3) is M0G better for pre estimation with BS+G
## 4) performance of three methods for predicting data
## 5) p-value distribution for h0 and h1
## 6) false positive rate vs p-value
## 7) p-value distribution
library(ROCR)
library(plyr)
library(ggplot2)
library(gridExtra)
library(magrittr)
theme_set(theme_bw())

simple.style <- theme(panel.grid.major = element_blank(),
                      panel.grid.minor = element_blank())

# colorbrewer2 color blind friendly colors
c.b.green <- '#1b9e77'
c.b.red <- '#d95f02'
c.b.blue <- '#7570b3'
c.b.pink <- '#e7298a'
c.b.grgreen <- '#66a61e'

## read results of branch-site model, compute lrt, pvalue
read.bs <- function(fn) {
    d <- read.table(fn, sep=',')
    d <- rename(d, c(hyp='sel', type='method'))

    d$lrt <- 2 * (d$lnL.1 - d$lnL.0)
    d$pvalue <- pchisq(d$lrt, df=1, lower.tail=F)/2
    thr <- 0.05
    d$det <- d$pvalue < thr
    d
}

d <- read.bs('full_sim_bs.txt')
d.sch <- read.bs('bs_sim_sch.txt')
d.cp <- read.bs('bs_simcp_estall.txt')
d.cp <- subset(d.cp,sim=='cp' & method %in% c('nog', 'cp', 'cg','sg'))
d <- rbind(d, d.sch, d.cp)
rm(d.cp)

d.busted <- read.table('../bs_busted/bs-busted.csv', sep=',', header=T) %>%
    dplyr::bind_rows(read.table('../bs_busted/bs-busted-cp.csv', sep=',', header=T)) %>%
    dplyr::mutate(det=pvalue<0.05,
                  dataset=as.numeric(stringr::str_replace(dataset, '.*/([0-9]+).*', '\\1')),
                  method='busted') %>%
    dplyr::rename(sel=hyp)

d <- dplyr::bind_rows(d, d.busted)

d.tt <- read.bs('sim_bs_tt.txt')
with(d.tt, table(sim, method))

d.busted.tt <- read.table('../bs_busted/bs-busted-tt.csv', sep=',', header=T) %>%
    dplyr::mutate(det=pvalue<0.05,
                  dataset=as.numeric(stringr::str_replace(dataset, '.*/([0-9]+).*', '\\1')),
                  method='busted') %>%
    dplyr::rename(sel=hyp)
d.tt <- dplyr::bind_rows(d.tt, d.busted.tt)


## convert performace from ROCR to a data.frame
e.xy <- function(x, stat, ds, method)
    data.frame(x=unlist(x[[stat]]@x.values),
               y=unlist(x[[stat]]@y.values),
               ds=ds, Inference=method)

## convert list of prediction performace to a dataframe
cr.df <- function(x, stat, ds, names=c('n', 's', 'c', 'p', 'b'), method.names=c('No variation',
                                                                                'Site variation',
                                                                                'Codon gamma variation',
                                                                                'Codon 3-rate variation',
                                                                                'BUSTED'))
    do.call(
        rbind,
        lapply(1:length(names),
               function(i) e.xy(x[[names[i]]], stat, ds=ds, method=method.names[i])
               )
    )

cr.df.3rate <- function(x, stat, ds)
    cr.df(x, stat, ds,
          names=c('n', 's', 'c', 'p', 'b'),
          method.names=c('No variation',
                         'Site gamma variation',
                         'Codon gamma variation',
                         'Codon 3-rate variation',
                         'BUSTED'))

## compute prediction performance
crr <- function (x) {
    if (nrow(x)==0) NULL
    else
        list(ss=performance(prediction(1-x$pvalue, x$sel), 'sens', 'spec'),
             auc=performance(prediction(1-x$pvalue, x$sel), 'auc'),
             fpr=performance(prediction(1-(x$pvalue), x$sel), 'fpr'),
             pr=performance(prediction(1-(x$pvalue), x$sel), 'prec', 'rec')
             )
}

## compute prediction for three methods of branch-length estimation
mr <- function (x)
    list(
        fbl=crr(subset(x, method=='fullblen')),
        m0bl=crr(subset(x, method=='nog')),
        nbl=crr(subset(x, method=='noblen'))
        )

## plot ROC for three methods of branch-length estimation
pl3 <- function(x, ...) {
    plot(x$nbl$ss, xlim=c(1,0), ...)
    plot(x$m0bl$ss, col='red', add=T)
    plot(x$fbl$ss, col='green', add=T)
    legend('bottomright', legend=c('noblen', 'm0blen', 'fullblen'),
           col=c('black', 'red', 'green'), lty=c(1,1))
    abline(v=0.95, lty=2, col="#178200")
}

## plot FPR
plot.fpr <- function(o, add=F, ...) {
    pltfn <- if (add) lines else plot
    pltfn(1-o$fpr@x.values[[1]], o$fpr@y.values[[1]], type='l',
          xlab='Significance level', ylab='False positive rate', ...)
    abline(0, 1, lty=2)
}

## plot three FPR for methods of branch-length estimation
pl3.fpr <- function(x, ...) {
    plot.fpr(x$nbl, ...)
    plot.fpr(x$m0bl, col='red', add=T, ...)
    plot.fpr(x$fbl, col='green', add=T, ...)
    legend('topleft', legend=c('noblen', 'm0blen', 'fullblen'),
           col=c('black', 'red', 'green'), lty=c(1,1))
    abline(v=0.05, lty=2, col="#178200")
}

## compute performance for three datasets (no variation, site
## variation, codon variation)
r.n <- mr(subset(d, sim=='nog'))
r.s <- mr(subset(d, sim=='sg'))
r.c <- mr(subset(d, sim=='cg'))

## plot performance (ROC & FPR) of three methods for branch-length estimation
pl.3.gg <- function(x, style=NULL) {
    df <- cr.df(x, 'ss', '', names=c('nbl', 'm0bl', 'fbl'),
                method.names=c('PhyML', 'M0', 'Estimate'))
    df <- rename(df, c('x'='Specificity', 'y'='Sensitivity'))
    p1 <- ggplot(df, aes(Specificity, Sensitivity, color=Inference)) +
        geom_line() + xlim(1, 0) +
        theme(legend.position = c(.85, .4),
              legend.box.background = element_rect()) +
        scale_color_brewer(type='qual', palette=2) +
        annotate('segment', x=0, xend=1, y=1, yend=0, linetype=2, col='gray') +
        annotate('segment', x=0, xend=1, y=1, yend=1, linetype=2, col='gray') +
        annotate('segment', x=1, xend=1, y=1, yend=0, linetype=2, col='gray') +
        geom_vline(xintercept=0.95, linetype=2, col='yellow') +
        labs(title='A) ROC curve') + style

    df <- cr.df(x, 'fpr', '', names=c('nbl', 'm0bl', 'fbl'),
                method.names=c('PhyML', 'M0', 'Estimate'))
    df$x <- 1 - df$x
    df <- subset(df, x>=0)
    df <- rename(df, c('x'='Significance level', 'y'='False positive rate'))

    p2 <- ggplot(df, aes(`Significance level`, `False positive rate`, color=Inference)) +
        geom_line() +
        theme(legend.position='none') +
        scale_color_brewer(type='qual', palette=2) +
        annotate('segment', x=0, xend=0.5, y=0, yend=0.5, linetype=2, col='gray') +
        geom_vline(xintercept=0.05, col='yellow', linetype=2) +
        labs(title='B) False positive rate') + style

    grid.arrange(p1, p2, ncol=1)
}

## plot performance of difference branch-length estimations
pdf('sim-bs-m0.pdf', width=5, height=5)
pl.3.gg(r.n, simple.style)
dev.off()

## plot ROC for three datasets (branch-length estimation)
png('m0tree_%03d.png')
pl3(r.n, main='norm')
pl3(r.s, main='sitegamma')
pl3(r.c, main='codongamma')
dev.off()


## plot false positive rate for three datasets (branch-length estimation)
pl3.fpr(r.n, main='norm', xlim=c(1e-2, .5), ylim=c(0, 0.2))
pl3.fpr(r.s, main='sitegamma', xlim=c(1e-2, .5), ylim=c(0, 0.2))
pl3.fpr(r.c, main='codongamma', xlim=c(1e-2, .5), ylim=c(0, 0.2))

## confusion matrix for branch-length estimation methods
with(subset(d, method %in% c('fullblen', 'nog', 'noblen')),
     table(sel, det, factor(method),
           dnn=c('selection', 'detected',  'method'))
     )

with(subset(d, method %in% c('fullblen', 'nog', 'noblen')),
     table(sel, det, factor(method), sim,
           dnn=c('selection', 'detected',  'method', 'simulation'))
     )


## 1) yer
## 2) no



## compute performance of a) M0 followed by branch-site with codon rate variation
## b) M0 with codon rate variation followed by branch-site with codon rate variation
mr.cg <- function (x)
    list(
        m0=crr(subset(x, method=='cg')),
        m0g=crr(subset(x, method=='m0gammatree'))
        )

## plot ROC for M0 vs M0+codonratevar
pl.cg <- function(x, ...) {
    plot(x$m0$ss, xlim=c(1,0), ...)
    plot(x$m0g$ss, col='green', add=T)
    legend('bottomright', legend=c('m0tree', 'm0gammatree'),
           col=c('black', 'green'), lty=c(1,1))
    abline(v=0.95, lty=2, col="#178200")
}

## plot FPR for M0 vs M0+codonratevar
pl.cg.fpr <- function(x, ...) {
    plot.fpr(x$m0, ...)
    plot.fpr(x$m0g, col='green', add=T, ...)
    legend('topleft', legend=c('m0tree', 'm0gammatree'),
           col=c('black', 'green'), lty=c(1,1))
    abline(v=0.05, lty=2, col="#178200")
}


## compute performance for M0 vs M0+codonratevar
r.cg.n <- mr.cg(subset(d, sim=='nog'))
r.cg.s <- mr.cg(subset(d, sim=='sg'))
r.cg.c <- mr.cg(subset(d, sim=='cg'))

## plot performance (ROC) for M0 vs M0+codonratevar
png('m0treegamma_%03d.png')
pl.cg(r.cg.n, main='norm')
pl.cg(r.cg.s, main='sitegamma')
pl.cg(r.cg.c, main='codongamma')
dev.off()

## plot performance (FPR) for M0 vs M0+codonratevar
pl.cg.fpr(r.cg.n, main='norm')
pl.cg.fpr(r.cg.s, main='norm')
pl.cg.fpr(r.cg.c, main='norm')

## confusion matrix for M0 vs M0+codonratevar
with(subset(d, method %in% c('cg', 'm0gammatree')),
     table(sel, det, factor(method),
           dnn=c('selection', 'detected',  'method'))
     )

with(subset(d, method %in% c('cg', 'm0gammatree')),
     table(sel, det, factor(method), sim,
           dnn=c('selection', 'detected',  'method', 'simulation'))
     )

## 3) no

## compute performance for BS, BS+sitevar, BS+codonvar
mr.n <- function (x)
    list(
        n=crr(subset(x, method=='nog')),
        s=crr(subset(x, method=='sg')),
        c=crr(subset(x, method=='cg')),
        p=crr(subset(x, method=='cp')),
        b=crr(subset(x, method=='busted'))
        )

## return AUC string
p.auc <- function(a, b)
    sprintf("AUC=%.2f (%.0f%%)", a, a/b*100)

## plot ROC
pl.n <- function(x, bw=FALSE, show.auc=TRUE, show.legend=TRUE, xlim=c(1,0), ...) {
    plot(x$n$ss, xlim=xlim, ...)
    segments(0,1,1,0, col="#808080", lty=2)
    segments(1,0,1,1, col="#808080", lty=2)
    segments(1,1,0,1, col="#808080", lty=2)
    plot(x$s$ss, col=c.b.red, add=T)
    plot(x$c$ss, col=c.b.green, add=T)
    if (bw) {
        arrows(0.85, 0.85, .99, .99, col='gray')
        arrows(0.69, 0.69, .51, .51, col='gray')
        text(0.83, 0.83, 'better', col='gray')
        text(0.71, 0.71, 'worse', col='gray')
    }
    auc.val <- list(
        n=x$n$auc@y.values[[1]],
        s=x$s$auc@y.values[[1]],
        c=x$c$auc@y.values[[1]]
    )
    best.auc <- do.call(max, auc.val)
    aucs = list(
        bquote('No' ~ Gamma * ',' ~ .(p.auc(auc.val$n, best.auc))),
        bquote('Site' ~ Gamma * ','  ~ .(p.auc(auc.val$s, best.auc))),
        bquote('Codon' ~ Gamma * ',' ~ .(p.auc(auc.val$c, best.auc)))
    )

    if (show.auc) {
    ##best.auc = r.c[[best]]$auc@y.values[[1]]
        text(0.3, 0.5, aucs[[1]])
        text(0.3, 0.4, aucs[[2]])
        text(0.3, 0.3, aucs[[3]])
    }

    abline(v=0.95, lty=4, col="#F0F000")

    if (show.legend)
        legend('bottomright', legend=c("No variation",
                                       'Site variation',
                                       'Codon variation'),
               col=c('black', c.b.red, c.b.green), lty=c(1,1), bg='white')
}

## compute performance for three datasets
r.n.n <- mr.n(subset(d, sim=='nog'))
r.n.s <- mr.n(subset(d, sim=='sg'))
r.n.c <- mr.n(subset(d, sim=='cg'))
r.n.p <- mr.n(subset(d, sim=='cp'))

## compute performance for true trees
r.n.n.tt <- mr.n(subset(d.tt, sim=='nog'))
r.n.s.tt <- mr.n(subset(d.tt, sim=='sg'))
r.n.c.tt <- mr.n(subset(d.tt, sim=='cg'))
r.n.p.tt <- mr.n(subset(d.tt, sim=='cp'))

## plot ROC for three datasets
png('rocnsc_%03d.png', width=640, height=480)
pl.n(r.n.n, bw=T, main='norm')
pl.n(r.n.s, main='sitegamma')
pl.n(r.n.c, main='codongamma')
pl.n(r.n.p, main='codonprop')
dev.off()


## plot ROC for three datasets
png('roc3.png', width=600, height=600)
par(mfrow=c(2,2))
pl.n(r.n.n, bw=T, main='A) Branch-site, no variation')
pl.n(r.n.s, main='B) Branch-site, site gamma variation')
pl.n(r.n.c, main='C) Branch-site, codon gamma variation')
dev.off()



## plot ROC using ggplot for three datasets
pl.n.gg <- function(n, s, c, p) {
    df <- rbind(cr.df.3rate(n, 'ss', 'A) No variation'),
                cr.df.3rate(s, 'ss', 'B) Site gamma variation'),
                cr.df.3rate(c, 'ss', 'C) Codon gamma variation'),
                cr.df.3rate(p, 'ss', 'D) Codon 3-rate variation')
                )
    ggplot(df, aes(x, y, color=Inference, linetype=Inference)) +
        labs(x='Specificity', y='Sensitivity') +
        geom_line() + xlim(1, 0) +
        facet_wrap(~ds, ncol=2) +
        theme(legend.position = 'bottom') +
        scale_color_manual(values=c('black', c.b.red, c.b.green, c.b.grgreen, c.b.pink)) +
        scale_linetype_manual(values=c('solid', 'solid', 'solid', 'dashed', 'solid')) +
        annotate('segment', x=0, xend=1, y=1, yend=0, linetype=2, col='gray') +
        annotate('segment', x=0, xend=1, y=1, yend=1, linetype=2, col='gray') +
        annotate('segment', x=1, xend=1, y=1, yend=0, linetype=2, col='gray') +
        geom_vline(xintercept=0.95, linetype=2, col='#e7298a')
}


## plot PR using ggplot for three datasets
pl.n.gg.pr <- function(n, s, c, p, metrics='pr') {
    df <- rbind(cr.df.3rate(n, metrics, 'A) No variation'),
                cr.df.3rate(s, metrics, 'B) Site gamma variation'),
                cr.df.3rate(c, metrics, 'C) Codon gamma variation'),
                cr.df.3rate(p, metrics, 'D) Codon 3-rate variation'))
    df <- rename(df, c('x'='Recall', 'y'='Precision'))
    ggplot(df, aes(Recall, Precision, color=Inference, linetype=Inference)) +
        geom_line() +
        facet_wrap(~ds, ncol=2) +
        theme(legend.position = 'bottom') +
        scale_color_manual(values=c('black', c.b.red, c.b.green, c.b.grgreen, c.b.pink)) +
        scale_linetype_manual(values=c('solid', 'solid', 'solid', 'dashed', 'solid')) +
        ylim(0.5, 1)
}

## paper sim-bs-roc; plot ROC for four datasets
pdf('sim-bs-roc.pdf', width=4, height=5)
pl.n.gg(r.n.n, r.n.s, r.n.c, r.n.p) + simple.style + guides(col=guide_legend(nrow=5))
dev.off()

## paper sim-bs-pr; plot PR for four datasets
pdf('sim-bs-pr.pdf', width=8, height=8)
pl.n.gg.pr(r.n.n, r.n.s, r.n.c, r.n.p) + simple.style + guides(col=guide_legend(nrow=5))
dev.off()

## paper sim-bs-roc-tt; plot ROC for four dataset with true trees
pdf('sim-bs-roc-tt.pdf', width=8, height=8)
pl.n.gg(r.n.n.tt, r.n.s.tt, r.n.c.tt, r.n.p.tt) + simple.style + guides(col=guide_legend(nrow=5))
dev.off()


## confusing matrices
with(subset(d, sim=='nog'),
     table(sel, det, method))

with(subset(d, sim=='sg'),
     table(sel, det, method))

with(subset(d, sim=='cg'),
     table(sel, det, method))

with(subset(d, sim=='cp'),
     table(sel, det, method))

## generate AUC & ACC tables for the paper
m.methods <- c('nog', 'sg', 'cg', 'cp')

## compute accuracy
accuracy <- function(d) 
    with(d, sum(sel==det)/length(sel))

## compute AUC
auc  <- function(x)
    unlist(performance(prediction(x$lrt, x$sel), 'auc')@y.values)

## Apply for two dimentions
apply.2d <- function(v1, v2, FUN) {
    m <- outer(v1, v2, Vectorize(FUN))
    dimnames(m) <- list(v1, v2)
    m
}

## express in percentage
by.max <- function(x) 100*t(t(x)/apply(x, 2, max))

## print AUC matrix
prn.mat <- function(x) {
    cat(paste(gsub('"', ' ', capture.output(x), fixed=T), collapse = "\n"))
    cat("\n")
}

## convert percent to a string
by.max.str <- function(x)
    prn.mat(
        matrix(sprintf("%0.3f/%5.1f%%",
                       x,
                       by.max(x)),
               ncol=ncol(x),
               dimnames=dimnames(x)
               )
    )


## compute accuracy for a matrix
acc.d <- apply.2d(m.methods, m.methods, function(m, s) accuracy(subset(d, sim == s & method == m)))

## print matrix
by.max.str(acc.d)

## compute AUC matrix
auc.d <- apply.2d(m.methods, m.methods, function(m, s) {
    d <- subset(d, sim == s & method == m)
    if (nrow(d) > 0)
        auc(d)
    else {
        NA
    }
})

## print matrix
by.max.str(auc.d)




## 4) done, the same as previously

## plot p-value distribution
ggplot(subset(d, sim=='nog' & method=='nog' & sel == 0), aes(pvalue))+ geom_histogram()
ggplot(subset(d, sim=='nog' & method=='nog' & sel == 1), aes(pvalue))+ geom_histogram()
ggplot(subset(d, sim=='nog' & method=='nog' & pvalue < 1), aes(pvalue))+ geom_histogram()

ggplot(subset(d, sim=='nog' & method=='nog' & sel == 0 & pvalue < 1), aes(pvalue))+ geom_histogram()
ggplot(subset(d, sim=='nog' & method=='nog' & sel == 1 & pvalue < 1), aes(pvalue))+ geom_histogram()
ggplot(subset(d, sim=='nog' & method=='nog' & pvalue < 1), aes(pvalue))+ geom_histogram()

ggplot(subset(d, sim=='nog' & method=='nog' & pvalue < 1), aes(pvalue))+ geom_histogram(bins=200)

ggplot(subset(d, sim=='nog' & method=='nog' & pvalue < 1), aes(pvalue))+ geom_histogram(bins=200)

## 5) similar artifacts

## compute speedup
levels(d$method)

tm <- sapply(c('nog', 'sg', 'cg', 'cp'),
       function(m) sum(subset(d, method==m)$time))

tm / min(tm)
tm/60/60

## 6) false positive rate

## plot false positive rate for three simulations
pl3.fpr.2 <- function(x, show.legend=TRUE, ...) {
    plot.fpr(x$n, ...)
    plot.fpr(x$s, col=c.b.red, add=T, ...)
    plot.fpr(x$c, col=c.b.green, add=T, ...)
    abline(v=0.05, lty=2, col="#178200")
    abline(0, 1, lty=3, col="#178200")
    if (show.legend)
        legend('topleft', legend=c('No variation', 'Site variation', 'Codon variation'),
               col=c('black', c.b.red, c.b.green), lty=c(1,1), bg='white')
}

pl3.fpr.2(r.n.n, main='norm')
abline(0, 1)
pl3.fpr.2(r.n.s, main='sitegamma')
abline(0, 1)
pl3.fpr.2(r.n.c, main='codongamma')
abline(0, 1)

png('fpr3.png', width=600, height=600)
par(mfrow=c(2,2))
pl3.fpr.2(r.n.n, main='A) Branch-site, no variation')
pl3.fpr.2(r.n.s, main='B) Branch-site, site gamma variation')
pl3.fpr.2(r.n.c, main='C) Branch-site, codon gamma variation')
dev.off()

## plot false positive rate for three simulations using ggplot
pl3.fpr.gg <- function(n, s, c, p, horiz=FALSE) {
    df <- rbind(cr.df.3rate(n, 'fpr', 'A) No variation'),
                cr.df.3rate(s, 'fpr', 'B) Site gamma variation'),
                cr.df.3rate(c, 'fpr', 'C) Codon gamma variation'),
                cr.df.3rate(p, 'fpr', 'D) Codon 3-rate variation'))
    df$x <- 1 - df$x
    df <- subset(df, x>=0)
    ##df <- plyr::rename(df, c)
    ncol <- if (horiz) 3 else 2
    ggplot(df, aes(x, y, color=Inference, linetype=Inference)) +
        geom_line() +
        labs(x='Significance level', y='False positive rate') +
        facet_wrap(~ds, ncol=ncol) +
        scale_color_manual(values=c('black', c.b.red, c.b.green, c.b.grgreen, c.b.pink)) +
        scale_linetype_manual(values=c('solid', 'solid', 'solid', 'dashed', 'solid')) +
        annotate('segment', x=0, xend=0.5, y=0, yend=0.5, linetype=2, col='gray') +
        geom_vline(xintercept=0.05, col='#e7298a', linetype=2) +
        xlim(c(0, 0.5))
}


## paper
pdf('sim-bs-fpr.pdf', width=8, height=5)
pl3.fpr.gg(r.n.n, r.n.s, r.n.c, r.n.p) + simple.style
dev.off()

## paper
pdf('sim-bs-fpr-tt.pdf', width=8, height=6)
pl3.fpr.gg(r.n.n.tt, r.n.s.tt, r.n.c.tt, r.n.p.tt) + simple.style + guides(col=guide_legend(nrow=5))
dev.off()


## 7) p-value distribution
ggplot(subset(d, method=='nog' & sim=='nog'), aes(pvalue, fill=sel==1)) + geom_histogram()

ggplot(subset(d, method=='nog' & sim=='nog'), aes(pvalue)) + geom_histogram() + facet_wrap(~ factor(sel), ncol=1) + ylim(0, 200)


## plot false positive rate as a function of alpha
sim.par <- read.table('../gamma.txt', header=TRUE, sep=',')

d.cg.nog.sim <- merge(subset(d, sim=='cg' & method=='nog'),
                      sim.par, by='dataset')

d.sg.nog.sim <- merge(subset(d, sim=='sg' & method=='nog'),
                      sim.par, by='dataset')

## compute false positive rate for a threshold
fpr.a <- function(x, a.lower=0, a.upper=Inf,  thr=0.05) with(x[x$alpha <= a.upper & x$alpha >= a.lower & x$sel==0,], sum(pvalue<thr)/length(pvalue))

## plot false positive rate versus p-value
pl.fpr.a <- function(d, ...) {
    x <- sort(unique(d$alpha))
    y <- sapply(x, function(a) fpr.a(d, a.upper=a))
    plot(x, y, type='l', xlab=expression(alpha), ylab='False positive rate')
}

## plot false positive rate versus q-value
pl.fpr.q <- function(d, q=seq(0, 1, 0.2), ...) {
    a.range <- quantile(d$alpha, q)
    fpr <- mapply(function(l, u) fpr.a(d, l, u), head(a.range, -1), tail(a.range, -1))
    mids <- (head(a.range, -1) + tail(a.range, -1))/2
    plot(mids, fpr, log='x', type='l', xlab=expression(alpha), ylab='False postive rate', ...)
    points(mids, fpr)
}

pl.fpr.a(d.cg.nog.sim)
pl.fpr.a(d.sg.nog.sim)

## paper; plot local false positive rate value
pdf('sim-bs-local-fpr.pdf', width=4, height=4)
par(mai=c(0.8, 0.8, 0.25, 0.15))
pl.fpr.q(d.cg.nog.sim)
dev.off()

pl.fpr.q(d.sg.nog.sim)
