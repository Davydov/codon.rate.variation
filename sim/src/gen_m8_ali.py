#!/usr/bin/env python
## generates m8 alignments
import csv
from multiprocessing import Pool

from plumbum import local
cosim = local['cosim.py']

def simm8(base, m8par, record, tree):
    global commands
    if base.endswith('.H1'):
        omega2 = record['w2.m8']
    else:
        omega2 = '1'
    par = m8par + ['--omega2', omega2, '--log', base + '.log',
                     tree, record['alen'], base + '.fst']
    commands.append(par)

def run(par):
    print('cosim ' + ' '.join(par))
    cosim(par)

if __name__ == '__main__':
    mkdir = local['mkdir']
    mkdir('-p', 'data/m8/nog', 'data/m8/cg', 'data/m8/sg',
          'data/m8/cp', 'data/m8/sp')
    commands = []
    for record in csv.DictReader(open('data/gamma.txt')):
        m8par = ['--seed', '1',
                 '--model', 'M8',
                 '--kappa', record['k'],
                 '--omega0-a', record['a'],
                 '--omega0-b', record['b'],
                 '--omega0-ncat', '5',
                 '--p0', record['p.m8'],
                 '--debug']
        site_a = ['--site-alpha', record['alpha'], '--site-ncat', '4']
        codon_a = ['--codon-alpha', record['alpha'], '--codon-ncat', '4']
        site_r = ['--site-rates', ','.join((record['r1.eff'], record['r2.eff'], record['r3.eff'])),
                  '--site-props', ','.join((record['p1.eff'], record['p2.eff'], record['p3.eff']))]
        codon_r = ['--codon-rates', ','.join((record['r1.eff'], record['r2.eff'], record['r3.eff'])),
                  '--codon-props', ','.join((record['p1.eff'], record['p2.eff'], record['p3.eff']))]
        tree = 'data/trees/' + record['dataset'] + '.nwk'
        d = 'data/m8/nog/'
        base = d + record['dataset']
        simm8(base + '.H0', m8par, record, tree)
        simm8(base + '.H1', m8par, record, tree)
        d = 'data/m8/sg/'
        base = d + record['dataset']
        simm8(base + '.H0', m8par + site_a, record, tree)
        simm8(base + '.H1', m8par + site_a, record, tree)
        d = 'data/m8/cg/'
        base = d + record['dataset']
        simm8(base + '.H0', m8par + codon_a, record, tree)
        simm8(base + '.H1', m8par + codon_a, record, tree)
        d = 'data/m8/sp/'
        base = d + record['dataset']
        simm8(base + '.H0', m8par + site_r, record, tree)
        simm8(base + '.H1', m8par + site_r, record, tree)
        d = 'data/m8/cp/'
        base = d + record['dataset']
        simm8(base + '.H0', m8par + codon_r, record, tree)
        simm8(base + '.H1', m8par + codon_r, record, tree)

    nproc = 12
    p = Pool(nproc)
    p.map(run, commands)

