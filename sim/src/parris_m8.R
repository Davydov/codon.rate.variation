## Performance PARRIS on M8 simulations
## not used in the paper, here for completenes
library(ROCR)

read.parris <- function(fn) {
    d <- read.table(fn, col.names=c('sim', 'ac', 'hyp', 'lrt'))
    d$pvalue <- pchisq(d$lrt, df=2, lower.tail=F)
    thr <- 0.05
    d$sel <- d$hyp == 'H1'
    d$det <- d$pvalue < thr
    d
}

d.5 <- read.parris('m8_parris_5nsclass.txt')
d.6 <- read.parris('m8_parris_6nsclass.txt')

crr <- function (x)
    list(ss=performance(prediction(x$lrt, x$sel), 'sens', 'spec'),
         auc=performance(prediction(x$lrt, x$sel), 'auc'),
         fpr=performance(prediction(1-(x$pvalue), x$sel), 'fpr')
         )

plot.fpr <- function(o, add=F, ...) {
    pltfn <- if (add) lines else plot
    pltfn(1-o$fpr@x.values[[1]], o$fpr@y.values[[1]], type='l',
         xlab='p-value', ylab='False positive rate', ...)
}


pl3.fpr <- function(nog, sg, cg, ...) {
    plot.fpr(nog, ...)
    plot.fpr(sg, col='red', add=T, ...)
    plot.fpr(cg, col='green', add=T, ...)
    legend('topleft', legend=c('nog', 'sg', 'cg'),
           col=c('black', 'red', 'green'), lty=c(1,1))
    abline(v=0.05, lty=2, col="#178200")
}

plot.roc <- function(o, add=F, ...) {
    pltfn <- if (add) lines else plot
    pltfn(o$ss)
}

pl3.roc <- function(nog, sg, cg, ...) {
    plot(nog$ss, xlim=c(1,0), ...)
    plot(sg$ss, col='red', add=T, ...)
    plot(cg$ss, col='green', add=T, ...)
    legend('topleft', legend=c('nog', 'sg', 'cg'),
           col=c('black', 'red', 'green'), lty=c(1,1))
    abline(1, -1, lty=2, col="#178200")
}

r.n.5 <- crr(subset(d.5, sim=='nog'))
r.s.5 <- crr(subset(d.5, sim=='sg'))
r.c.5 <- crr(subset(d.5, sim=='cg'))

pl3.fpr(nog=r.n.5, sg=r.s.5, cg=r.c.5)
pl3.roc(nog=r.n.5, sg=r.s.5, cg=r.c.5)

r.n.6 <- crr(subset(d.6, sim=='nog'))
r.s.6 <- crr(subset(d.6, sim=='sg'))
r.c.6 <- crr(subset(d.6, sim=='cg'))

pl3.fpr(nog=r.n.6, sg=r.s.6, cg=r.c.6)
pl3.roc(nog=r.n.6, sg=r.s.6, cg=r.c.6)
