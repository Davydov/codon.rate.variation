#!/usr/bin/env python3
## create trees with #2 on background branches
## variating number of #2 from 0 to max number of background
import dendropy
import sys
import numpy as np
from os.path import basename

t = dendropy.Tree.get(path=sys.argv[1], schema='newick')
## this only works for internal nodes
bgnodes = t.nodes(lambda x: x._parent_node is not None and x.label != '#1')
rep=1000

for rm in range(len(bgnodes)):
    for i in range(rep):
        bg_w3_nodes = np.random.choice(bgnodes, rm)
        for node in bg_w3_nodes:
            node.label = '#2'
        fn = 'tree.%02d.%03d.nwk' % (rm, i)
        t.write(path=fn,
                schema='newick',
                suppress_leaf_node_labels=False,
                node_label_element_separator='')
        for node in bg_w3_nodes:
            node.label = ''
