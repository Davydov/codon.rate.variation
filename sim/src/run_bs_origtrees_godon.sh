#!/bin/bash
# run branch-site with true trees on branch-site simuations
#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J bs-sim[1-80]
#BSUB –R "select[tmp>1024],rusage[mem=4096]"
#BSUB -M 4194304
##BSUB -n 4
##BSUB -R "span[ptile=4]"

NAME=bsgp-sim-bs_3
SUB=2-godon-trees
SOURCE=bs_trees_all.tgz

source $HOME/mysub/mysub.bash

export OMP_NUM_THREADS=1
PAR="--procs 1 --seed 1 --log-level info --no-branch-length"
GODON=$CLUSTER/godon-30fb0f4

cmd () {
	if [[ $1 == *sp/* ]]
	then
		return
	fi
	if [[ $1 == *.fst ]]
	then
		dn=$(dirname $1)
		mkdir -p res/$dn
		fst=$1
		bn=$(basename $1)
		ds=${bn%.*.*}
		inwk=trees/$ds.nwk
		nwk=$inwk.scaled
		$HOME/dndstools/scale_tree.py --multiply $inwk 10 $nwk

		log=res/${1%.*}.nog.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BS --json $json --trajectory $tr --out $log $fst $nwk 
		log=res/${1%.*}.sg.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BSG --ncat-site-rate 4 --json $json --trajectory $tr --out $log $fst $nwk
		log=res/${1%.*}.cg.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BSG --ncat-codon-rate 4 --json $json --trajectory $tr --out $log $fst $nwk
		log=res/${1%.*}.cp.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BSG --ncat-codon-rate 3 --proportional --json $json --trajectory $tr --out $log $fst $nwk
	fi
}

run
