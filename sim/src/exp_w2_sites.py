#!/usr/bin/env python
# export sites with omega2
import os
from glob import iglob

def get_sites(fn):
    for line in open(fn):
        if line.startswith('Sites with omega2:'):
            arr = line.split('[')[1].strip().strip(']')
            if not arr:
                return []
            return [int(i.strip()) + 1 for i in arr.split(',')]

print 'sim,dataset,hyp,Codon'
for fn in iglob('*/*.H1.log'):
    dn = os.path.dirname(fn)
    for site in get_sites(fn):
        print ','.join((dn, fn.rsplit('.', 1)[0], '1', str(site)))
