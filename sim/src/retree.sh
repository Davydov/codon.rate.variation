#!/bin/bash
## reconstructs tree for a single alignment
## keeping the topology
## phyml version 20131022
bn=$(basename $1)
start_tree=data/trees/${bn%.*.*}.nwk
phyml --input $1 --datatype nt --bootstrap 0 --model HKY85 -f e --inputtree $start_tree -o lr --r_seed 1 
