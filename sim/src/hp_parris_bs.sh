#!/bin/bash
# run PARRIS on branch-site
ali=$(readlink -f $1)
tree=$(readlink -f $2)
cat << EOF
inputRedirect = {};
inputRedirect["01"]="Universal"; // genetic code
inputRedirect["02"]="1"; // number of datasets
inputRedirect["03"]="$ali"; // codon data
inputRedirect["04"]="$tree"; // tree
inputRedirect["05"]="Nucleotide Model"; // Branch Lengths: Nucleotide Model (other hangs sometimes)
inputRedirect["06"]="GY"; // equilibrium frequencies
inputRedirect["07"]="HKY85"; // Nucleotide Rate Matrix: kappa
inputRedirect["08"]="Single"; // classes of non-synonymous substitutions
inputRedirect["09"]="Proportional"; // rate variation
inputRedirect["10"]="Run Custom"; // model choice
inputRedirect["11"]="4"; // number of synonymous rate classes
inputRedirect["12"]="3"; // number of non-synonymous rate classes
inputRedirect["13"]="M1a"; // model choice
inputRedirect["14"]="M2a"; // model choice
inputRedirect["15"]=""; // model choice: end
inputRedirect["16"]="Default"; // initial value
inputRedirect["17"]="${ali}.parris"; // save summary




ExecuteAFile (HYPHY_LIB_DIRECTORY+"TemplateBatchFiles/PARRIS.bf", inputRedirect);
EOF
