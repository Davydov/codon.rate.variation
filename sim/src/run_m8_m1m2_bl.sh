#!/bin/bash
# run M2a vs M1a on M8 simulations (with branch-length estimation)
#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J m8-m1m2[1-40]
#BSUB –R "select[tmp>1024],rusage[mem=4096]"
#BSUB -M 4194304
##BSUB -n 4
##BSUB -R "span[ptile=4]"

NAME=bsgp-sim-m8
SUB=9-godon-m1m2
SOURCE=m8.tgz

source $HOME/mysub/mysub.bash

export OMP_NUM_THREADS=1
PAR="--procs 1 --seed 1 --log-level info"
GODON=$CLUSTER/godon-ca95ea8

cmd () {
	if [[ $1 == *.fst ]]
	then
		dn=$(dirname $1)
		mkdir -p res/$dn
		fst=$1
		bn=$(basename $1)
		nwk=${1%.*}.nwk
		
		log=res/${1%.*}.M1a.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON $PAR M1a --json $json  --out $log $fst $nwk

		log=res/${1%.*}.M2a.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON $PAR M2a --json $json  --out $log $fst $nwk

		log=res/${1%.*}.M1aG.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON $PAR --ncat-codon-gamma 4 M1a --json $json  --out $log $fst $nwk

		log=res/${1%.*}.M2aG.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON $PAR --ncat-codon-gamma 4 M2a --json $json  --out $log $fst $nwk

		
	fi
}

run
