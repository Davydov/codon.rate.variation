#!/usr/bin/env python
## export pvalues from busted
import json
import glob
import os.path

print 'dataset,sim,hyp,pvalue'
for fn in glob.iglob('*/*.json'):
    dataset = fn.rsplit('.', 3)[0]
    sim = os.path.dirname(fn)
    hyp = int('H1' in dataset)
    record = json.load(open(fn))
    print ','.join((dataset, sim, str(hyp), str(record['test results']['p'])))
