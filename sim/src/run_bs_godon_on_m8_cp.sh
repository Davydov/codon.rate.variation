#!/bin/bash
# run branch-site on cp M8 simulations
#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J m8-sim[1-30]
#BSUB –R "select[tmp>1024],rusage[mem=4096]"
#BSUB -M 4194304
##BSUB -n 4
##BSUB -R "span[ptile=4]"

NAME=bsgp-sim-m8_3
SUB=7-godon-bs-cp
SOURCE=m8.tgz

source $HOME/mysub/mysub.bash

export OMP_NUM_THREADS=1
PAR="--procs 1 --seed 1 --log-level info --m0-tree --all-branches"
GODON=$CLUSTER/godon-387b972

cmd () {
	if [[ $1 == *.fst ]]
	then
		dn=$(dirname $1)
		mkdir -p res/$dn
		fst=$1
		bn=$(basename $1)
		nwk=${1%.*}.nwk
		log=res/${1%.*}.nog.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BS --json $json --trajectory $tr --out $log $fst $nwk 
		log=res/${1%.*}.sg.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BSG --ncat-site-rate 4 --json $json --trajectory $tr --out $log $fst $nwk
		log=res/${1%.*}.cg.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BSG --ncat-codon-rate 4 --json $json --trajectory $tr --out $log $fst $nwk
		log=res/${1%.*}.cp.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BSG --ncat-codon-rate 3 --proportional --json $json --trajectory $tr --out $log $fst $nwk
	fi
}

run
