#!/usr/bin/env python
## reads foreground branch from the source tree,
## and transfer it to the output tree
import dendropy
import glob
import os
from itertools import islice

def nth(iterable, n, default=None):
    "Returns the nth item or a default value"
    return next(islice(iterable, n, None), default)

def noblen(tree, rmhash=True):
    s = tree.as_string(schema='newick', suppress_edge_lengths=True)
    return s.replace('#1', '') if rmhash else s
    

for sfn in glob.iglob('data/bs/*/*.phy_phyml_tree.txt'):
    stree = dendropy.Tree.get(path=sfn, schema='newick')
    base = os.path.basename(sfn).split('.', 1)[0]
    fgfn = 'data/trees/%s.nwk' % base
    fgtree = dendropy.Tree.get(path=fgfn, schema='newick')
    assert noblen(stree) == noblen(fgtree)
    assert noblen(stree, False) != noblen(fgtree, False)
    fgnodes_ind = {i for i, node in enumerate(fgtree.postorder_node_iter())
               if node.label == '#1' or (node.is_leaf() and node.taxon.label.endswith('#1'))}
    assert len(fgnodes_ind) == 1
    fgnodes = [node for i, node in enumerate(stree.postorder_node_iter()) if i in fgnodes_ind]
    for node in fgnodes:
        if node.is_leaf():
            node.taxon.label += '#1'
        else:
            node.label = '#1'
    assert noblen(stree, False) == noblen(fgtree, False)
    outfn = sfn.rsplit('.', 2)[0] + '.nwk'
    stree.write(path=outfn, schema='newick', real_value_format_specifier='.8f')
