#!/bin/bash
# run absreal on a file
cat << EOF
inputRedirect = {};
inputRedirect["01"]="Universal"; // genetic code
inputRedirect["02"]="Yes"; // [Strongly recommended] Automatically decide on appropriate model complexity among branches
inputRedirect["03"]="Yes"; // [Yes] Both alpha and beta vary along branch-site combinations (alt: [No] [Default] Alpha varies from branch to branch, while omega varies among branch-site combinations)
inputRedirect["04"]="$(readlink -f $1)"; // codon data
inputRedirect["05"]="$(readlink -f $2)"; // alignment
inputRedirect["06"]="$(readlink -f $3)"; // Save analysis results to

ExecuteAFile (HYPHY_LIB_DIRECTORY+"TemplateBatchFiles/BranchSiteREL.bf", inputRedirect);
EOF
