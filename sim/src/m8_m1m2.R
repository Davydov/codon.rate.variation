## running M2a vs M1a on M8 simulations

## this was not used for the paper, here for completenes
library(ROCR)
library(plyr)


## compute pvalue & lrt
comp.stat <- function(d) {
    d$lrt <- 2*(d$L.M2 - d$L.M1)

    d$pvalue <- pchisq(d$lrt, df=2, lower.tail = F)

    d$det <- d$pvalue < 0.05

    d
}

## read m2a vs m1a data
read.m1m2 <- function(fn) {
    d <- read.table(fn, header=T)

    d <- subset(d, model != 'M0')

    d <- merge(subset(d, model=='M1a', select=-model),
               subset(d, model=='M2a', select=-model),
               by=c('sim', 'dataset', 'hyp', 'var'),
               suffixes=c('.M1', '.M2'))


    comp.stat(d)
}

## read data
d.m <- read.m1m2('m8_m1m2.txt')
d.b <- read.m1m2('m8_m1m2_bl.txt') # with branch length estimation (?)
d.h <- comp.stat(rename(read.table('m8_m1m2_hyptest.txt', header=T), c('L0'='L.M1', 'L1'='L.M2')))



## m8 -> m1 vs m2 in godon



## confusion matrices
with(subset(d.m, sim=='nog'),
     table(det, hyp, var)
     )

with(subset(d.b, sim=='nog'),
     table(det, hyp, var)
     )

with(subset(d.h, sim=='nog'),
     table(det, hyp, model)
     )

with(subset(d.m, sim=='cg'),
     table(det, hyp, var)
     )

with(subset(d.b, sim=='cg'),
     table(det, hyp, var)
     )

with(subset(d.h, sim=='cg'),
     table(det, hyp, model)
     )

with(subset(d.m, sim=='sg'),
     table(det, hyp, var)
     )

with(subset(d.b, sim=='sg'),
     table(det, hyp, var)
     )

with(subset(d.h, sim=='sg'),
     table(det, hyp, var)
     )

## compute prediction performance
crr <- function (x)
    list(ss=performance(prediction(x$lrt, x$hyp==1), 'sens', 'spec'),
         auc=performance(prediction(x$lrt, x$hyp==1), 'auc'),
         fpr=performance(prediction(1-(x$pvalue), x$hyp==1), 'fpr'),
         tpr=performance(prediction(1-(x$pvalue), x$hyp==1), 'tpr')
         )

plot.fpr <- function(o, add=F, ...) {
    pltfn <- if (add) lines else plot
    pltfn(1-o$fpr@x.values[[1]], o$fpr@y.values[[1]], type='l',
         xlab='p-value', ylab='False positive rate', ...)
}

plot.tpr <- function(o, add=F, ...) {
    pltfn <- if (add) lines else plot
    pltfn(1-o$fpr@x.values[[1]], o$tpr@y.values[[1]], type='l',
         xlab='p-value', ylab='True positive rate', ...)
}

# plot FPR for multiple datasets
pl3.fpr <- function(nog, sg, cg, ...) {
    plot.fpr(nog, ...)
    plot.fpr(sg, col='red', add=T, ...)
    plot.fpr(cg, col='green', add=T, ...)
    legend('topleft', legend=c('nog', 'sg', 'cg'),
           col=c('black', 'red', 'green'), lty=c(1,1))
    abline(v=0.05, lty=2, col="#178200")
}

plot.roc <- function(o, add=F, ...) {
    pltfn <- if (add) lines else plot
    pltfn(o$ss)
}

# plot ROC for multiple datasets
pl3.roc <- function(nog, sg, cg, ...) {
    plot(nog$ss, xlim=c(1,0), ...)
    plot(sg$ss, col='red', add=T, ...)
    plot(cg$ss, col='green', add=T, ...)
    legend('topleft', legend=c('nog', 'sg', 'cg'),
           col=c('black', 'red', 'green'), lty=c(1,1))
    abline(1, -1, lty=2, col="#178200")
}

## return AUC for three datasets
auc.3 <- function(nog, sg, cg) {
    e <- function(x) x$auc@y.values[[1]]
    c(nog=e(nog), sg=e(sg), cg=e(cg))
}

## no rate variation in the data
r.n <- crr(subset(d.m, sim=='nog' & !var))
r.s <- crr(subset(d.m, sim=='sg' & !var))
r.c <- crr(subset(d.m, sim=='cg' & !var))

pl3.fpr(nog=r.n, sg=r.s, cg=r.c)
pl3.roc(nog=r.n, sg=r.s, cg=r.c)
auc.3(nog=r.n, sg=r.s, cg=r.c)

## codon rate variation in the data
r.n.v <- crr(subset(d.m, sim=='nog' & var))
r.s.v <- crr(subset(d.m, sim=='sg' & var))
r.c.v <- crr(subset(d.m, sim=='cg' & var))

pl3.fpr(nog=r.n.v, sg=r.s.v, cg=r.c.v)
pl3.roc(nog=r.n.v, sg=r.s.v, cg=r.c.v)
auc.3(nog=r.n.v, sg=r.s.v, cg=r.c.v)

r.n.b <- crr(subset(d.b, sim=='nog' & !var))
r.s.b <- crr(subset(d.b, sim=='sg' & !var))
r.c.b <- crr(subset(d.b, sim=='cg' & !var))

pl3.fpr(nog=r.n.b, sg=r.s.b, cg=r.c.b)
pl3.roc(nog=r.n.b, sg=r.s.b, cg=r.c.b)
auc.3(nog=r.n.b, sg=r.s.b, cg=r.c.b)

r.n.b.v <- crr(subset(d.b, sim=='nog' & var))
r.s.b.v <- crr(subset(d.b, sim=='sg' & var))
r.c.b.v <- crr(subset(d.b, sim=='cg' & var))

pl3.fpr(nog=r.n.b.v, sg=r.s.b.v, cg=r.c.b.v)
pl3.roc(nog=r.n.b.v, sg=r.s.b.v, cg=r.c.b.v)
auc.3(nog=r.n.b.v, sg=r.s.b.v, cg=r.c.b.v)


r.n.h <- crr(subset(d.h, sim=='nog' & model=='M12a'))
r.s.h <- crr(subset(d.h, sim=='sg' & model=='M12a'))
r.c.h <- crr(subset(d.h, sim=='cg' & model=='M12a'))

pl3.fpr(nog=r.n.h, sg=r.s.h, cg=r.c.h)
pl3.roc(nog=r.n.h, sg=r.s.h, cg=r.c.h)
auc.3(nog=r.n.h, sg=r.s.h, cg=r.c.h)

r.n.h.v <- crr(subset(d.h, sim=='nog' & model=='M12aG'))
r.s.h.v <- crr(subset(d.h, sim=='sg' & model=='M12aG'))
r.c.h.v <- crr(subset(d.h, sim=='cg' & model=='M12aG'))

pl3.fpr(nog=r.n.h.v, sg=r.s.h.v, cg=r.c.h.v)
pl3.roc(nog=r.n.h.v, sg=r.s.h.v, cg=r.c.h.v)
auc.3(nog=r.n.h.v, sg=r.s.h.v, cg=r.c.h.v)


r.n.h.v.t <- crr(subset(d.h, sim=='nog' & model=='M12aGt'))
r.s.h.v.t <- crr(subset(d.h, sim=='sg' & model=='M12aGt'))
r.c.h.v.t <- crr(subset(d.h, sim=='cg' & model=='M12aGt'))

pl3.fpr(nog=r.n.h.v.t, sg=r.s.h.v.t, cg=r.c.h.v.t)
pl3.roc(nog=r.n.h.v.t, sg=r.s.h.v.t, cg=r.c.h.v.t)
auc.3(nog=r.n.h.v.t, sg=r.s.h.v.t, cg=r.c.h.v.t)


## m8 -> m1 vs m2 in parris (PS.bf)

d.parris <- read.table('parris-m12.txt', header=T)

d.parris$lrt <- 2*(d.parris$l2 - d.parris$l1)

d.parris$pvalue <- pchisq(d.parris$lrt, df=2, lower.tail = F)

d.parris$det <- d.parris$pvalue < 0.05

with(subset(d.parris, sim=='nog'),
     table(det, hyp, var)
     )

with(subset(d.parris, sim=='cg'),
     table(det, hyp, var)
     )

with(subset(d.parris, sim=='sg'),
     table(det, hyp, var)
     )

r.n.p <- crr(subset(d.parris, sim=='nog' & !var))
r.s.p <- crr(subset(d.parris, sim=='sg' & !var))
r.c.p <- crr(subset(d.parris, sim=='cg' & !var))

pl3.fpr(nog=r.n.p, sg=r.s.p, cg=r.c.p)
pl3.roc(nog=r.n.p, sg=r.s.p, cg=r.c.p)
auc.3(nog=r.n.p, sg=r.s.p, cg=r.c.p)

r.n.p.v <- crr(subset(d.parris, sim=='nog' & var))
r.s.p.v <- crr(subset(d.parris, sim=='sg' & var))
r.c.p.v <- crr(subset(d.parris, sim=='cg' & var))

pl3.fpr(nog=r.n.p.v, sg=r.s.p.v, cg=r.c.p.v)
pl3.roc(nog=r.n.p.v, sg=r.s.p.v, cg=r.c.p.v)
auc.3(nog=r.n.p.v, sg=r.s.p.v, cg=r.c.p.v)

## compare parris and godon on the same datasets

## read m8-godon res:
m8.cgdist <- read.table('m8_godon_cgdisc.txt')

m8.cgdist$lrt <- 2 * (m8.cgdist$L1 - m8.cgdist$L0)
m8.cgdist$pvalue <- pchisq(m8.cgdist$lrt, df=1, lower.tail=F)

m8.cgdist$det <- m8.cgdist$pvalue < 0.05

m8.cgdist.n <- crr(subset(m8.cgdist, method=='nog' & sim=='cg'))
m8.cgdist.s <- crr(subset(m8.cgdist, method=='sg' & sim=='cg'))
m8.cgdist.c <- crr(subset(m8.cgdist, method=='cg' & sim=='cg'))

## read m8 godon cgdist
m8.m8 <- read.table('../m8_roc/sim_m8.txt', sep=',')

m8.m8$lrt <- 2 * (m8.m8$lnL.1 - m8.m8$lnL.0)
m8.m8$pvalue <- pchisq(m8.m8$lrt, df=1, lower.tail=F)

m8.m8$det <- m8.m8$pvalue < 0.05

m8.n.n <- crr(subset(m8.m8, type=='nog' & sim=='nog'))
m8.n.s <- crr(subset(m8.m8, type=='sg' & sim=='nog'))
m8.n.c <- crr(subset(m8.m8, type=='cg' & sim=='nog'))

m8.c.n <- crr(subset(m8.m8, type=='nog' & sim=='cg'))
m8.c.s <- crr(subset(m8.m8, type=='sg' & sim=='cg'))
m8.c.c <- crr(subset(m8.m8, type=='cg' & sim=='cg'))

m8.s.n <- crr(subset(m8.m8, type=='nog' & sim=='sg'))
m8.s.s <- crr(subset(m8.m8, type=='sg' & sim=='sg'))
m8.s.c <- crr(subset(m8.m8, type=='cg' & sim=='sg'))



## read m8 parris:
m8.m8p <- read.table('m8_m8am8_parrisold.txt', header=T)

m8.m8p$lrt <- 2 * (m8.m8p$l2 - m8.m8p$l1)
m8.m8p$pvalue <- pchisq(m8.m8p$lrt, df=1, lower.tail=F)

m8.m8p$det <- m8.m8p$pvalue < 0.05


m8p.n.n <- crr(subset(m8.m8p, sim=='nog' & !var))
m8p.n.c <- crr(subset(m8.m8p, sim=='nog' & var))

m8p.c.n <- crr(subset(m8.m8p, sim=='cg' & !var))
m8p.c.c <- crr(subset(m8.m8p, sim=='cg' & var))

m8p.s.n <- crr(subset(m8.m8p, sim=='sg' & !var))
m8p.s.c <- crr(subset(m8.m8p, sim=='sg' & var))

plot.rocs <- function(..., xlim=c(1,0)) {
    args <- list(...)
    pal <- rainbow(length(args))

    for (i in seq_along(args)) {
        nm <- names(args)[i]
        add <- i > 1
        plot(args[[i]]$ss, col=pal[i], xlim=xlim, add=add)
    }

    legend('bottomright', legend=names(args), col=pal, lty=1)
}

plot.fprs <- function(..., FUN=plot) {
    args <- list(...)
    pal <- rainbow(length(args))

    for (i in seq_along(args)) {
        nm <- names(args)[i]
        add <- i > 1
        plot.fpr(args[[i]], col=pal[i], add=add)
    }

    abline(0, 1, lty=22)
    legend('bottomright', legend=names(args), col=pal, lty=1)
}

plot.tprs <- function(..., FUN=plot) {
    args <- list(...)
    pal <- rainbow(length(args))

    for (i in seq_along(args)) {
        nm <- names(args)[i]
        add <- i > 1
        plot.tpr(args[[i]], col=pal[i], add=add)
    }

    legend('bottomright', legend=names(args), col=pal, lty=1)
}


aucs <- function(...) {
    sapply(list(...), function(x) x$auc@y.values[[1]])
}


## read m8 godon hyptest bl
m8h.bl <- read.table('m8_godon_bl_part.txt')
m8h.bl$lrt <- 2 * (m8h.bl$L1 - m8h.bl$L0)
m8h.bl$pvalue <- pchisq(m8h.bl$lrt, df=1, lower.tail=F)

m8h.bl$det <- m8h.bl$pvalue < 0.05

m8h.bl.c.n <- crr(subset(m8h.bl, sim=='cg' & method=='nog'))
m8h.bl.c.c <- crr(subset(m8h.bl, sim=='cg' & method=='cg'))


## read m8 10cat
m8.10cat.cg <- rbind(read.table('m8_godon10cat_cg.txt'), read.table('m8_godon10cat_nog.txt'))
m8.10cat.cg$lrt <- 2 * (m8.10cat.cg$L1 - m8.10cat.cg$L0)
m8.10cat.cg$pvalue <- pchisq(m8.10cat.cg$lrt, df=1, lower.tail=F)

m8.10cat.cg$det <- m8.10cat.cg$pvalue < 0.05

m8.10.n.c <- crr(subset(m8.10cat.cg, sim=='nog'))
m8.10.c.c <- crr(subset(m8.10cat.cg, sim=='cg'))

## read m8 10cat + b5
m8.10cat.5b <- read.table('m8_godon10cat_5b.txt')
m8.10cat.5b$lrt <- 2 * (m8.10cat.5b$L1 - m8.10cat.5b$L0)
m8.10cat.5b$pvalue <- pchisq(m8.10cat.5b$lrt, df=1, lower.tail=F)

m8.10cat.5b$det <- m8.10cat.5b$pvalue < 0.05

m8.10.5.n.c <- crr(subset(m8.10cat.5b, sim=='nog'))
m8.10.5.c.c <- crr(subset(m8.10cat.5b, sim=='cg'))


## a) nog, no variation
plot.rocs(godon.m2=r.n, godon.m2.bl=r.n.b, godon.m2.hyp=r.n.h, godon.m8=m8.n.n, parris.m2=r.n.p, parris.m8=m8p.n.n)
plot.fprs(godon.m2=r.n, godon.m2.bl=r.n.b, godon.m2.hyp=r.n.h, godon.m8=m8.n.n, parris.m2=r.n.p, parris.m8=m8p.n.n)
aucs(godon.m2=r.n, godon.m2.bl=r.n.b, godon.m2.hyp=r.n.h, godon.m8=m8.n.n, parris.m2=r.n.p, parris.m8=m8p.n.n)


## b) nog, rate variation
plot.rocs(godon.m2.v=r.n.v, godon.m2.v.bl=r.n.b.v, godon.m2.v.hyp=r.n.h.v, godon.m2.v.hyp.g0t=r.n.h.v.t, godon.m8=m8.n.c, parris.m2=r.n.p.v, parris.m8=m8p.n.c)
plot.fprs(godon.m2.v=r.n.v, godon.m2.v.bl=r.n.b.v, godon.m2.v.hyp=r.n.h.v, godon.m2.v.hyp.g0t=r.n.h.v.t, godon.m8=m8.n.c, parris.m2=r.n.p.v, parris.m8=m8p.n.c)
aucs(godon.m2.v=r.n.v, godon.m2.v.bl=r.n.b.v, godon.m2.v.hyp=r.n.h.v, godon.m2.v.hyp.g0t=r.n.h.v.t, godon.m8=m8.n.c, parris.m2=r.n.p.v, parris.m8=m8p.n.c)

## c) cg, no variation
plot.rocs(godon.m2=r.c, godon.m2.bl=r.c.b, godon.m2.hyp=r.c.h, godon.m8=m8.c.n, parris.m2=r.c.p, parris.m8=m8p.c.n)
plot.fprs(godon.m2=r.c, godon.m2.bl=r.c.b, godon.m2.hyp=r.c.h, godon.m8=m8.c.n, parris.m2=r.c.p, parris.m8=m8p.c.n)
aucs(godon.m2=r.c, godon.m2.bl=r.c.b, godon.m2.hyp=r.c.h, godon.m8=m8.c.n, parris.m2=r.c.p, parris.m8=m8p.c.n)


## d) cg, rate variation
plot.rocs(godon.m2.v=r.c.v, godon.m2.v.bl=r.c.b.v, godon.m2.v.hyp=r.c.h.v, godon.m2.v.hyp.m0g=r.c.h.v.t, godon.m8=m8.c.c, parris.m2=r.c.p.v, parris.m8=m8p.c.c)
plot.fprs(godon.m2.v=r.c.v, godon.m2.v.bl=r.c.b.v, godon.m2.v.hyp=r.c.h.v, godon.m2.v.hyp.m0g=r.c.h.v.t, godon.m8=m8.c.c, parris.m2=r.c.p.v, parris.m8=m8p.c.c)
aucs(godon.m2.v=r.c.v, godon.m2.v.bl=r.c.b.v, godon.m2.v.hyp=r.c.h.v, godon.m2.v.hyp.m0g=r.c.h.v.t, godon.m8=m8.c.c, parris.m2=r.c.p.v, parris.m8=m8p.c.c)

## e) sg, no variation
plot.rocs(godon.m2=r.s, godon.m2.bl=r.s.b, godon.m2.hyp=r.s.h, godon.m8=m8.s.n, parris.m2=r.s.p, parris.m8=m8p.s.n)
plot.fprs(godon.m2=r.s, godon.m2.bl=r.s.b, godon.m2.hyp=r.s.h, godon.m8=m8.s.n, parris.m2=r.s.p, parris.m8=m8p.s.n)
aucs(godon.m2=r.s, godon.m2.bl=r.s.b, godon.m2.hyp=r.s.h, godon.m8=m8.s.n, parris.m2=r.s.p, parris.m8=m8p.s.n)


## f) sg, rate variation
plot.rocs(godon.m2.v=r.s.v, godon.m2.v.bl=r.s.b.v, godon.m2.v.hyp=r.s.h.v, godon.m2.v.hyp.m0g=r.s.h.v.t, godon.m8=m8.s.c, parris.m2=r.s.p.v, parris.m8=m8p.s.c)
plot.fprs(godon.m2.v=r.s.v, godon.m2.v.bl=r.s.b.v, godon.m2.v.hyp=r.s.h.v, godon.m2.v.hyp.m0g=r.s.h.v.t, godon.m8=m8.s.c, parris.m2=r.s.p.v, parris.m8=m8p.s.c)
aucs(godon.m2.v=r.s.v, godon.m2.v.bl=r.s.b.v, godon.m2.v.hyp=r.s.h.v, godon.m2.v.hyp.m0g=r.s.h.v.t, godon.m8=m8.s.c, parris.m2=r.s.p.v, parris.m8=m8p.s.c)



## short version

## a) nog, no variation
plot.rocs(godon.m2=r.n.h, godon.m8=m8.n.n, parris.m2=r.n.p, parris.m8=m8p.n.n)
plot.fprs(godon.m2=r.n.h, godon.m8=m8.n.n, parris.m2=r.n.p, parris.m8=m8p.n.n)
plot.tprs(godon.m2=r.n.h, godon.m8=m8.n.n, parris.m2=r.n.p, parris.m8=m8p.n.n)
aucs(godon.m2=r.n.h, godon.m8=m8.n.n, parris.m2=r.n.p, parris.m8=m8p.n.n)


## b) nog, rate variation
plot.rocs(godon.m2.v=r.n.h.v, godon.m8.v=m8.n.c, godon.m8.v.10cat=m8.10.n.c, godon.m8.v.10cat.5b=m8.10.5.n.c, parris.m2.v=r.n.p.v, parris.m8.v=m8p.n.c)
plot.fprs(godon.m2.v=r.n.h.v, godon.m8.v=m8.n.c, godon.m8.v.10cat=m8.10.n.c, godon.m8.v.10cat.5b=m8.10.5.n.c, parris.m2.v=r.n.p.v, parris.m8.v=m8p.n.c)
plot.tprs(godon.m2.v=r.n.h.v, godon.m8.v=m8.n.c, godon.m8.v.10cat=m8.10.n.c, godon.m8.v.10cat.5b=m8.10.5.n.c, parris.m2.v=r.n.p.v, parris.m8.v=m8p.n.c)
aucs(godon.m2.v=r.n.h.v, godon.m8.v=m8.n.c, godon.m8.v.10cat=m8.10.n.c, godon.m8.v.10cat.5b=m8.10.5.n.c, parris.m2.v=r.n.p.v, parris.m8.v=m8p.n.c)

## c) cg, no variation
plot.rocs(godon.m2=r.c.h, godon.m8=m8.c.n, godon.m8.bl=m8h.bl.c.n, parris.m2=r.c.p, parris.m8=m8p.c.n)
plot.fprs(godon.m2=r.c.h, godon.m8=m8.c.n, godon.m8.bl=m8h.bl.c.n, parris.m2=r.c.p, parris.m8=m8p.c.n)
plot.tprs(godon.m2=r.c.h, godon.m8=m8.c.n, godon.m8.bl=m8h.bl.c.n, parris.m2=r.c.p, parris.m8=m8p.c.n)
aucs(godon.m2=r.c.h, godon.m8=m8.c.n, godon.m8.bl=m8h.bl.c.n, parris.m2=r.c.p, parris.m8=m8p.c.n)


## d) cg, rate variation
plot.rocs(godon.m2.v=r.c.h.v, godon.m8.v=m8.c.c, godon.m8.v.bl=m8h.bl.c.c, godon.m8.v.10=m8.10.c.c, godon.m8.v.5.10=m8.10.5.c.c, parris.m2.v=r.c.p.v, parris.m8.v=m8p.c.c)
plot.fprs(godon.m2.v=r.c.h.v, godon.m8.v=m8.c.c, godon.m8.v.bl=m8h.bl.c.c, godon.m8.v.10=m8.10.c.c, godon.m8.v.5.10=m8.10.5.c.c, parris.m2.v=r.c.p.v, parris.m8.v=m8p.c.c)
plot.tprs(godon.m2.v=r.c.h.v, godon.m8.v=m8.c.c, godon.m8.v.bl=m8h.bl.c.c, godon.m8.v.10=m8.10.c.c, godon.m8.v.5.10=m8.10.5.c.c, parris.m2.v=r.c.p.v, parris.m8.v=m8p.c.c)
aucs(godon.m2.v=r.c.h.v, godon.m8.v=m8.c.c, godon.m8.v.bl=m8h.bl.c.c, godon.m8.v.10=m8.10.c.c, godon.m8.v.5.10=m8.10.5.c.c, parris.m2.v=r.c.p.v, parris.m8.v=m8p.c.c)

## e) sg, no variation
plot.rocs(godon.m2=r.s.h, godon.m8=m8.s.n, parris.m2=r.s.p, parris.m8=m8p.s.n)
plot.fprs(godon.m2=r.s.h, godon.m8=m8.s.n, parris.m2=r.s.p, parris.m8=m8p.s.n)
aucs(godon.m2=r.s.h, godon.m8=m8.s.n, parris.m2=r.s.p, parris.m8=m8p.s.n)


## f) sg, rate variation
plot.rocs(godon.m2.v=r.s.h.v, godon.m8.v=m8.s.c, parris.m2.v=r.s.p.v, parris.m8.v=m8p.s.c)
plot.fprs(godon.m2.v=r.s.h.v, godon.m8.v=m8.s.c, parris.m2.v=r.s.p.v, parris.m8.v=m8p.s.c)
aucs(godon.m2.v=r.s.h.v, godon.m8.v=m8.s.c, parris.m2.v=r.s.p.v, parris.m8.v=m8p.s.c)

## for lab meeting
## a) nog, no variation
png('nog_nog_%d.png')
plot.rocs(godon.m2=r.n.h, godon.m8=m8.n.n, parris.m2=r.n.p, parris.m8=m8p.n.n, xlim=c(1,0.95))
plot.fprs(godon.m2=r.n.h, godon.m8=m8.n.n, parris.m2=r.n.p, parris.m8=m8p.n.n)
dev.off()


## c) cg, no variation
png('cg_nog_%d.png')
plot.rocs(godon.m2=r.c.h, godon.m8=m8.c.n,  parris.m2=r.c.p, parris.m8=m8p.c.n)
plot.rocs(godon.m2=r.c.h, godon.m8=m8.c.n,  godon.cgdist.n=m8.cgdist.n, parris.m2=r.c.p, parris.m8=m8p.c.n)
dev.off()


## d) cg, rate variation
png('cg_cg_%d.png', width=800)
par(mfrow=c(1,2))
plot.rocs(godon.m2.v=r.c.h.v, godon.m8.v=m8.c.c, parris.m2.v=r.c.p.v, parris.m8.v=m8p.c.c, xlim=c(1,0.9))
plot.rocs(godon.m2.v=r.c.h.v, godon.m8.v=m8.c.c, parris.m2.v=r.c.p.v, parris.m8.v=m8p.c.c)
par(mfrow=c(1,1))


plot.fprs(godon.m2.v=r.c.h.v, godon.m8.v=m8.c.c, parris.m2.v=r.c.p.v, parris.m8.v=m8p.c.c)
plot.tprs(godon.m2.v=r.c.h.v, godon.m8.v=m8.c.c, parris.m2.v=r.c.p.v, parris.m8.v=m8p.c.c)


plot.rocs(godon.m2.v=r.c.h.v, godon.m8.v=m8.c.c, godon.cgdist.c=m8.cgdist.c, parris.m2.v=r.c.p.v, parris.m8.v=m8p.c.c)
plot.fprs(godon.m2.v=r.c.h.v, godon.m8.v=m8.c.c, godon.cgdist.c=m8.cgdist.c, parris.m2.v=r.c.p.v, parris.m8.v=m8p.c.c)
plot.tprs(godon.m2.v=r.c.h.v, godon.m8.v=m8.c.c, godon.cgdist.c=m8.cgdist.c, parris.m2.v=r.c.p.v, parris.m8.v=m8p.c.c)

par(mfrow=c(1,2))
plot.rocs(godon.m8.v.5.10=m8.10.5.c.c, parris.m2.v=r.c.p.v, parris.m8.v=m8p.c.c, xlim=c(1,0.9))
plot.rocs(godon.m8.v.5.10=m8.10.5.c.c, parris.m2.v=r.c.p.v, parris.m8.v=m8p.c.c)
par(mfrow=c(1,1))

plot.fprs(godon.m8.v.5.10=m8.10.5.c.c, parris.m2.v=r.c.p.v, parris.m8.v=m8p.c.c)
plot.tprs(godon.m8.v.5.10=m8.10.5.c.c, parris.m2.v=r.c.p.v, parris.m8.v=m8p.c.c)
dev.off()


## find subset where godon.m8 underperforms
merge(
    subset(m8.m8, sim=='cg' & type=='cg' & hyp==0 & det, select=-c(sim, hyp, type, time, pvalue, det)),
    subset(m8.m8p, sim=='cg' & var & hyp==0 & !det, select=-c(sim, det, hyp, var, pvalue)),
    by=c('dataset'),
    suffixes=c('.g', '.p')
)$dataset


## with blen
merge(
    subset(m8h.bl, sim=='cg' & method=='cg' & hyp==0 & det, select=-c(sim, hyp, method, pvalue, det)),
    subset(d.parris, sim=='cg' & var & hyp==0 & !det, select=-c(sim, det, hyp, var, pvalue)),
    by=c('dataset'),
    suffixes=c('.g', '.p')
)


## find h1 which are detected by small ncal
merge(
    subset(m8.m8, sim=='cg' & type=='cg' & hyp==1 & det, select=-c(sim, hyp, type, time, pvalue, det)),
    subset(m8.m8p, sim=='cg' & var & hyp==0 & !det, select=-c(sim, det, hyp, var, pvalue)),
    by=c('dataset'),
    suffixes=c('.g', '.p')
)$dataset


sprintf("%0.3f", sort(head(subset(m8.m8, sim=='cg' & type=='cg' & hyp==0)$lrt, 125)))
sprintf("%0.3f", sort(head(subset(m8.m8p, sim=='cg' & var & hyp==0)$lrt, 125)))

sprintf("%0.3f", sort(head(subset(m8.m8, sim=='cg' & type=='cg' & hyp==1)$lrt, 125)))
sprintf("%0.3f", sort(head(subset(m8.m8p, sim=='cg' & var & hyp==1)$lrt, 125)))


sprintf("%0.3f", sort(head(subset(m8.m8, sim=='cg' & type=='nog' & hyp==1)$lrt, 125)))
sprintf("%0.3f", sort(head(subset(m8.m8p, sim=='cg' & !var & hyp==1)$lrt, 125)))

sprintf("%0.3f", sort(head(subset(m8.m8, sim=='cg' & type=='nog' & hyp==0)$lrt, 125)))
sprintf("%0.3f", sort(head(subset(m8.m8p, sim=='cg' & !var & hyp==0)$lrt, 125)))
