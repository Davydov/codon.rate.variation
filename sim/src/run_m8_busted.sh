#!/bin/bash
# run BUSTED on M8 simulations
#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J m8-sim-busted[1-20]
#BSUB –R "select[tmp>1024],rusage[mem=4096]"
#BSUB -M 4194304
##BSUB -n 4
##BSUB -R "span[ptile=4]"

NAME=bsgp-sim-m8_3
SUB=3-busted
SOURCE=m8.tgz

source $HOME/mysub/mysub.bash

export OMP_NUM_THREADS=1
export PATH=$PATH:$HOME/hyphy/bin

cmd () {
	if [[ $1 == *.fst ]]
	then
		dn=$(dirname $1)
		mkdir -p res/$dn
		fst=$1
		nwkin=${1%.*}.nwk
		nwk=$nwkin.nohash
		sed 's/#1//' $nwkin > $nwk
		bf=res/${1%.*}.bf
		log=res/${1%.*}.log

		$CLUSTER/hp_busted.sh $fst $nwk > $bf
		bf=$(readlink -f $bf)
		HYPHYMP $bf &> $log
		mv $fst.* res/$dn/
		mv messages.log res/$fst.messages
		test -f errors.log && mv errors.log res/$fst.errors
	fi
}

run
