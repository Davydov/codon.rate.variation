#!/bin/bash
# run M2a vs M1a on branch-site simulations
#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J bs-m1m2[1-40]
#BSUB –R "select[tmp>1024],rusage[mem=4096]"
#BSUB -M 4194304
##BSUB -n 4
##BSUB -R "span[ptile=4]"

NAME=bsgp-sim-bs
SUB=7-godon-m1m2
SOURCE=bs.tgz

source $HOME/mysub/mysub.bash

export OMP_NUM_THREADS=1
PAR="--procs 1 --seed 1 --log-level info --no-final"
GODON=$CLUSTER/godon-bfeb28c

cmd () {
	if [[ $1 == *.fst ]]
	then
		dn=$(dirname $1)
		mkdir -p res/$dn
		fst=$1
		bn=$(basename $1)
		nwk=${1%.*}.nwk
		
		log=res/${1%.*}.M12a.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON $PAR test M2a --m0-tree --json $json --out $log $fst $nwk

		log=res/${1%.*}.M12aG.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON $PAR test M2a --m0-tree --ncat-codon-gamma 4 --json $json --out $log $fst $nwk

		log=res/${1%.*}.M12aGt.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON $PAR test M2a --m0-tree-gamma --ncat-codon-gamma 4 --json $json --out $log $fst $nwk
	fi
}

run
