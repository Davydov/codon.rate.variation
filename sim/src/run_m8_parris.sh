#!/bin/bash
# Run PARRIS on M8 simulations
#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J m8-sim-parris[1-80]
#BSUB –R "select[tmp>1024],rusage[mem=4096]"
#BSUB -M 4194304
##BSUB -n 4
##BSUB -R "span[ptile=4]"

NAME=bsgp-sim-m8
SUB=5-parris
SOURCE=m8.tgz

source $HOME/mysub/mysub.bash

export OMP_NUM_THREADS=1
export PATH=$PATH:$HOME/hyphy/bin

cmd () {
	if [[ $1 == *.nwk.nohash ]]
	then
		dn=$(dirname $1)
		mkdir -p res/$dn
		nwk=$1
		fst=${1%.*.*}.fst
		bf=res/$1.bf
		log=res/$1.log

		$CLUSTER/hp_parris_m8.sh $fst $nwk > $bf
		bf=$(readlink -f $bf)
		HYPHYMP $bf &> $log
		mv $fst.* res/$dn/
		mv messages.log res/$fst.messages
		test -f errors.log && mv errors.log res/$fst.errors
	fi
}

run
