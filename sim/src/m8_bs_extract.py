#!/usr/bin/env python
## exports m8_bs.txt table for later processing in R
import shelve
import glob
import sys
import json
import re
import csv


def clean_tree(s):
    return re.sub(':[0-9.][0-9.e+-]*', '',
        re.sub('T[0-9]+', '', s)
    )

def get_br(t):
    return len(tree.replace('(', '').split('#')[0])


if __name__ == '__main__':
    db = shelve.open(sys.argv[1])
    o = csv.DictWriter(sys.stdout, fieldnames=('sim', 'dataset', 'branch',
                                               'hyp', 'method', 'D'))
    o.writeheader()
    for k in db.keys():
        sim, rest = k.split('/')
        ds, ps, me, _ = rest.split('.', 3)
        v = db[k]
        for tst in v['tests']:
            tree = clean_tree(tst['tree'])
            br = get_br(tree)
            L0 = tst['H0']['maxLnL']
            L1 = tst['H1']['maxLnL']
            D = 2 * (L1 - L0)
            d = {
                'sim': sim,
                'dataset': ds,
                'branch': br,
                'hyp': ps[1],
                'method': me,
                'D': D,
            }
            o.writerow(d)
    db.close()

