#!/bin/bash
# run FUBAR
cat << EOF
inputRedirect = {};
inputRedirect["01"]="Universal"; // genetic code
inputRedirect["02"]="1"; // How many datafiles are to be analyzed
inputRedirect["03"]="$(readlink -f $1)"; // alignment
inputRedirect["04"]="$(readlink -f $2)"; // tree
inputRedirect["05"]="20"; // Number of grid points per dimension
inputRedirect["06"]="5"; // Number of MCMC chains to run
inputRedirect["07"]="2000000"; // The length of each chain
inputRedirect["08"]="1000000"; // Discard this many samples as burn-in
inputRedirect["09"]="100"; // How many samples should be drawn from each chain
inputRedirect["10"]="0.5"; // The concentration parameter of the Dirichlet prior


ExecuteAFile (HYPHY_LIB_DIRECTORY+"TemplateBatchFiles/FUBAR.bf", inputRedirect);
EOF
