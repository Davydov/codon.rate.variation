#!/usr/bin/env python
## generates trees
import csv
import subprocess


if __name__ == '__main__':
    for record in csv.DictReader(open('data/gamma.txt')):
        print record['dataset']
        base = 'data/trees/' + record['dataset']
        otfn = base + '.onwk'
        otlog = otfn + '.log'
        tfn = base + '.nwk'
        with open(otfn, 'w') as otf:
            with open(otlog, 'w') as lf:
                subprocess.call(
                    ['gen_tree.py',
                     '--ntax', record['ntips'],
                     '--birth-rate', '0.3',
                     '--death-rate', '0.1',
                     '--verbose', '--foreground-leafs',
                    '--add-foreground'],
                    stdout=otf, stderr=lf)
        subprocess.call(
            ['scale_tree.py', otfn, record['tlen'], tfn]
        )
