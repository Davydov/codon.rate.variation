We created two simulated dataset: one with M8 the model and one with
the branch-site model. First we generated simulation parameters using
(`src/get_gamma.R`), then trees (`src/gen_trees.py`). Then alignmetns
(`src/gen_m8_ali.py` & `src/gen_bs_ali`). Then trees were re-estimated
using phyml (`src/retree_all_bs.sh` & `src/retree_all_m8.sh`).

Parameter estimation was performed on the LSF cluster system, but
scripts can be easily converted to something else. For parameter estimations you need:

- `src/run_bs_godon.sh`
- `src/run_m8_godon.sh`
- `src/run_bs_busted.sh`
- `src/run_m8_busted.sh`
- `src/run_bs_godon_misc.sh` (comparison between different ways of
  tree estimation)
- `src/run_m8_godon_misc.sh` (comparison between different ways of
  tree estimation)
- `src/run_bs_godon_on_m8.sh` (perfrormance of M8 on branch-site model simulations)
- `src/run_m8_godon_on_bs.sh` (perfrormance of branch-site model on M8 simulations)

The results then are improted into python `shelve` using
`src/import_shelve.py`. The results are exported to the text format
using `src/export_lnL.py` and `src/exp_busted_pvalue.py`.

Use R scripts in `src/` for further data processing:

- `src/bs_m8.R` - performance of M8 on branch-site model simulations
- `src/bs_roc.R` - performance of the branch-site model
- `src/busted_bs.R` - performance of busted on branch-site simulations
- `src/busted_bs.R` - performance of busted on branch-site simulations
- `src/dist_plot.R` - plot simulation parameters distribution
- `src/m8_bs.R` - performance of branch-site model on M8 simulations
- `src/m8_roc.R` - performance of M8
