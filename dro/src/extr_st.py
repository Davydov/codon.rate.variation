#!/usr/bin/env python
## extract starts from json files
import shelve
import os.path
import json

db = shelve.open('dro.db')
odir = 'starts'

created = set()

for k in db['keys']:
    if k.endswith('.BSG'):
        b = k.split('.', 1)[0]
        h0 = db[k]['H0']['maxLParameters']
        h1 = db[k]['H1']['maxLParameters']
        path = os.path.join(odir, b)
        if not path in created:
            try:
                os.mkdir(path)
            except OSError:
                pass
            created.add(path)
        h0fn = os.path.join(path, k + '0')
        h1fn = os.path.join(path, k + '1')
        f0 = open(h0fn, 'w')
        json.dump(h0, f0)
        f0.close()
        f1 = open(h1fn, 'w')
        json.dump(h1, f1)
        f1.close()

