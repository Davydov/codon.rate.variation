#!/bin/bash
# run branch-site and M8 on 1k subset of drosophila data
#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J bs-dro[1-160]
#BSUB –R "select[tmp>1024],rusage[mem=6144]"
#BSUB -M 6291456
##BSUB -n 4
##BSUB -R "span[ptile=4]"

NAME=bsgp-dro-bs
SUB=4-r1k-all
SOURCE=dro-fst-r1k.tgz

source $HOME/mysub/mysub.bash

export OMP_NUM_THREADS=1
PAR="--procs 1 --seed 1 --log-level info"
GODON=$CLUSTER/godon-30fb0f4

cmd () {
	if [[ $1 == *.fst ]]
	then
		fst=$1
		nwk=${1%.*}.nwk
		log=res/${1%.*}.nog.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		#$GODON test $PAR BS --m0-tree --all-branches --codon-omega --json $json --trajectory $tr --out $log $fst $nwk 
		#python -c "import json; print(json.load(open('$json')))['globalOptimizations'][0]['finalTree']" > res/$nwk
		log=res/${1%.*}.sg.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BSG --no-branch-length --all-branches --ncat-site-rate 4 --site-rates --codon-omega --json $json --trajectory $tr --out $log $fst $nwk
		log=res/${1%.*}.cg.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		#$GODON test $PAR BSG --no-branch-length --all-branches --ncat-codon-rate 4 --codon-rates --codon-omega --json $json --trajectory $tr --out $log $fst $nwk
		log=res/${1%.*}.cp.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BSG --no-branch-length --all-branches --ncat-codon-rate 3 --codon-rates --codon-omega --proportional --json $json --trajectory $tr --out $log $fst $nwk
		log=res/${1%.*}.nog.M8.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		#$GODON test $PAR M8 --no-branch-length  --codon-omega --json $json --trajectory $tr --out $log $fst $nwk 
		log=res/${1%.*}.sg.M8.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR M8 --no-branch-length --ncat-site-rate 4 --site-rates --codon-omega --json $json --trajectory $tr --out $log $fst $nwk
		log=res/${1%.*}.cg.M8.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		#$GODON test $PAR M8 --no-branch-length --ncat-codon-rate 4 --codon-rates --codon-omega --json $json --trajectory $tr --out $log $fst $nwk
		log=res/${1%.*}.cp.M8.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR M8 --no-branch-length --ncat-codon-rate 3 --codon-rates --codon-omega --proportional --json $json --trajectory $tr --out $log $fst $nwk
	fi
}

run
