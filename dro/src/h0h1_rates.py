#!/usr/bin/env python
## print rates computed for H0 and H1
import shelve
import random

def print_rates(record, dataset, hyp):
    for i, rate in enumerate(record[hyp]['codonGammaRates']):
        print dataset, branch, hyp, i, rate

def ktods(s):
    return s.rsplit('.', 3)[0]

if __name__ == '__main__':
    random.seed(1)
    db = shelve.open('dro.db')
    ## find 100 random bsg trees
    kk = db['keys']
    allds = [ktods(k) for k in kk]
    allds = set(random.sample(allds, 100))
    mykk = [k for k in kk if 'BSG' in k and ktods(k) in allds]
    print 'dataset branch hyp pos rate'
    for  k in mykk:
        branch = int(k.split('.')[4])
        print_rates(db[k], ktods(k), 'H0')
        print_rates(db[k], ktods(k), 'H1')
        

    db.close()
