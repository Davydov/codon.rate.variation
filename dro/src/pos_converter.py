#!/usr/bin/env python
## converting positions in the alignment
## into positions in the protein
import glob
import os.path
from Bio import AlignIO
import numpy as np
from collections import Counter

def present_absent(seq):
    return np.array(
            [False if '-' in seq[i:i+3] else True  for i in xrange(0, len(seq), 3)]
            )

def defined(seq):
    return np.array(
            [False if '-' in seq[i:i+3] or 'n' in seq[i:i+3] else True  for i in xrange(0, len(seq), 3)]
            )

def get_coords(ali):
    pa = [present_absent(r.seq) for r in ali]
    df = [defined(r.seq) for r in ali]
    index = np.empty((ali.get_alignment_length(),), dtype=int)
    index[:] = -1
    pos = np.sum(pa, axis=0) > len(ali) / 2
    first = np.where(pos==True)[0][0]
    first_codons = Counter((str(r.seq[first*3:first*3+3]) for r in ali))
    first_common = first_codons.most_common(1)[0][0]
    ## if the first is start codon
    ## indexes are probably correct
    if first_common == 'ATG':
        index[pos] = xrange(sum(pos))
    # mask undefined regions
    pos = np.sum(df, axis=0) <= 0.7 * len(ali)
    index[pos] = -1
    return index


if __name__ == '__main__':
    print "dataset alipos protpos"
    for fn in glob.iglob('starts/*.phy'):
        dataset = os.path.basename(fn).rsplit('.', 1)[0]
        ali = AlignIO.read(open(fn), 'phylip')
        assert ali.get_alignment_length() % 3 == 0
        index = get_coords(ali)
        for i, v in enumerate(index):
            if v >= 0:
                print dataset, i, v
