#!/bin/bash
## make blast database with Drosophila melanogaster sequences
makeblastdb -in data/dmel-all-CDS-r5.36.fasta -dbtype nucl -out data/dmel_cds -logfile data/dmel_cds.log
