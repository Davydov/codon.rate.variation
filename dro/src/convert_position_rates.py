#!/usr/bin/env python3

# This script converts alignment coordinates into protein coordinates
# by default it usess D. melanogaster (taxid=7227) sequence as a
# reference, if D. melanogaster is not found, it uses the last
# sequence of the alignment. It uses alignment rates coordinates file
# as an input, and outputs result to stdout. Only rates with
# minimum of three sequences are exported
import sys
import glob
import os.path
import gzip

from collections import Counter

from Bio.SeqIO.FastaIO import SimpleFastaParser

def get_features(line):
    d = {}
    for e in line.split()[1:]:
        k, v = e.split('=')
        d[k] = v
    return d

def get_pos(seq, counts, min_count=3):
    positions = {}
    real_pos = 0
    for i in range(0, len(seq), 3):
        codon = seq[i:i+3]
        if codon != '---':
            if counts[i//3] >= min_count:
                positions[i//3] = real_pos
            real_pos += 1
    return positions

def get_counts(fn):
    cnt = Counter()
    with open(fn) as f:
        for name, seq in SimpleFastaParser(f):
            for i in range(0, len(seq), 3):
                codon = seq[i:i+3]
                if codon != '---' and codon.lower() != 'nnn':
                    cnt[i//3] += 1
    return cnt

starts = {}
for fn in glob.iglob('Selectome_v06_Drosophila-nt_masked/*.fas'):
    ds = os.path.basename(fn).rsplit('.', 2)[0]
    pos_counts = get_counts(fn)
    with open(fn) as f:
        for name, seq in SimpleFastaParser(f):
            f = get_features(name)
            if f['TAXID'] == '7227':
                starts[ds] = get_pos(seq, pos_counts)
                break
        else:
            starts[ds] = get_pos(seq, pos_counts)

fn = sys.argv[1]
if fn.endswith('.gz'):
    f = gzip.open(fn, 'rt', encoding='utf-8')
else:
    f = open(f)
for line in f:
    ds, pos, rate = line.split()
    pos = int(pos)
    if pos in starts[ds]:
        print(ds, starts[ds][pos], rate)
