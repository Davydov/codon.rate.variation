#!/bin/bash
## search blast drosophila melanogaster
blastn -query data/dm_seq.fst -db data/dmel_cds -outfmt '10 qseqid sseqid evalue pident stitle' -out data/cds_search.csv -evalue 0.1 -max_target_seqs 1 -max_hsps 1
