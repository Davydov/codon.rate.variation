#!/bin/bash
## run recombination rate calculator on gene regions
sed 's/.* //' data/regions > data/regions.pos
cd contrib/RRC/
./RRC-open-v2.3.pl -M ../../data/regions.pos
