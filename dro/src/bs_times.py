#!/usr/bin/env python
# compute slowdownd of branch-site with no/codon rate variations
import shelve
import sys
from collections import Counter

db = shelve.open(sys.argv[1])

print 'dataset,variation,model,time'
for k in db:
    if k == 'keys':
        continue
    ds, var, mod = k.rsplit('.', 2)
    time = db[k]['time']
    print '%s,%s,%s,%f' % (ds, var, mod, time)
