#!/usr/bin/env python
## count how many likelihood underoptimizations we have
import shelve
import os.path
import json
from pprint import pprint

def getl(r):
    return r['optimizers'][-1]['maxLnL']

def getls(r):
    h0 = []
    h1 = []
    for run in r['runs']:
        h = h0 if '-fixw' in run['commandLine'] else h1
        h.append(getl(run))
    assert len(h0) > 0 and len(h1) > 0
    return h0, h1

def underopt(h, thr):
    if h[-1] - h[0] > thr:
        return True
    return False

def isgood(r):
    h0, h1 = getls(r)
    if underopt(h0, thr) or underopt(h1, thr):
        return False
    return True
    
thr = 0.5
ngood = 0
counter = 0
db = shelve.open('dro.db')
for b in db['keys']:
    record = db[b]
    counter += 1
    if isgood(record):
        ngood += 1

print '%d/%d' % (ngood, counter)

