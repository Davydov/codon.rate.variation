#!/bin/bash
## run branch-site on drosophila
# this is an outdated example; use godon test --m0-tree --codon-rates
#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J dro-bsg[1-80]
#BSUB –R "select[tmp>1024],rusage[mem=4096]"
#BSUB -M 4194304
##BSUB -n 4
##BSUB -R "span[ptile=4]"

NAME=dro
SUB=4-godon-bstest
SOURCE=dro.tgz

source $HOME/mysub/mysub.bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/software/Utility/nlopt/2.3/lib
export OMP_NUM_THREADS=1
PAR="-nt 1 -method lbfgsb -loglevel info -seed 1 -nobrlen"
GODON=$CLUSTER/godon
module add Phylogeny/paml/4.9a

cmd () {
	if [[ $1 == *.phy ]]
	then
		phy=$1
		nwk=${1%.*}.nwk
		out=res/${1%.*}.M1.mlc
		$HOME/dndstools/cdmw.py --preset M1a --fix-blength 0 $phy $nwk $out
		tree=res/${1%.*}.M1.nwk
		cat $out | grep '^(' | tail -n 1 > $tree
		$HOME/dndstools/pbranch.py $tree
		$HOME/dndstools/phy2fst.py $phy
		fst=${phy%.*}.fst
		for t in ${tree%.*}.*.nwk
		do
		    log=${t%.*}.BS.log
		    json=${log%.*}.json
		    $CLUSTER/godon-bstest -binary $GODON -json $json $fst $t $PAR -log $log -model BS
		    log=${t%.*}.BSG.log
		    json=${log%.*}.json
		    $CLUSTER/godon-bstest -binary $GODON -json $json $fst $t $PAR -log $log -model BSG -ncatcg 5
		done
	fi
}

run
