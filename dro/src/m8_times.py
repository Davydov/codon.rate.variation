#!/usr/bin/env python
## compute computation times for M8
import glob
import os.path
import json
import sys

def import_dir(d):
    res = {}
    
    for fn in glob.iglob(os.path.join(d, '*.json')):
        b = os.path.basename(fn).rsplit('.', 3)[0]
        with open(fn) as f:
            j = json.load(f)
            res[b] = j['time']
    return res

m8gtime = import_dir('res6-seprates')
m8time = import_dir('res7-m8')

bm8 = set(m8time.keys())
bm8g = set(m8gtime.keys())
print 'only m8+g:', (bm8g - bm8)
print 'only m8:', (bm8 - bm8g)

nt = 0.
gt = 0.
for b in set(bm8) & set(bm8g):
    nt += m8time[b]
    gt += m8gtime[b]

print gt, nt
print gt/nt
