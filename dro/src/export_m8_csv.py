#!/usr/bin/env python
## export M8 results to csv
import sys
import os
import glob
import json
import shelve

def readp(fn):
    return json.load(open(fn))['optimizers'][0]

def strpar(r):
    d = r['maxLParameters']
    d['omega'] = d.get('omega', 'NA')
    d['alpha'] = d.get('alphac', 'NA')
    pars = ['p0', 'p', 'q', 'kappa', 'omega', 'alpha']
    template = '\t'.join(('{{{}}}'.format(p) for p in pars))
    return template.format(
            **d)

def add_suffix(l, suffix):
    return [v + suffix for v in l]

def print_header():
    names = ['dataset', 'model']
    pars = ['lnL', 'p0', 'p', 'q', 'kappa', 'omega', 'alpha']
    names.extend(add_suffix(pars, '.0'))
    names.extend(add_suffix(pars, '.1'))

    print '\t'.join(names)

if __name__ == '__main__':
    print_header()
    i = 0
    db = shelve.open(sys.argv[1])
    for fn in db.keys():
        if fn == 'keys':
            continue
        dataset, model, family = fn.rsplit('.', 2)
        if family != 'M8':
            continue
        r = db[fn]

        r0 = r['H0']
        r1 = r['H1']
        print '{0}\t{1}\t{2[maxLnL]}\t{3}\t{4[maxLnL]}\t{5}'.format(
                dataset, model, r0, strpar(r0), r1, strpar(r1))
        if i % 1000 == 0:
            print >> sys.stderr, dataset, i
        i += 1

