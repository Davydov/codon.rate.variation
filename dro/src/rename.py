#!/usr/bin/env python
## rename selectome files
import glob
import os.path
import re

import dendropy

from Bio import SeqIO

idn = re.compile('[A-Z0-9_]*')

outdir = 'data/conv'

if __name__ == '__main__':
    for tfn in glob.glob('data/Selectome_v06_Drosophila-Trees_NHX/*.nhx'):
        rename = {}
        bn = os.path.basename(tfn).rsplit('.', 1)[0]
        ofb = os.path.join(outdir, bn)
        afn = os.path.join('data/Selectome_v06_Drosophila-nt_masked', bn + '.nt_masked.fas')
        seqs = [record for record in SeqIO.parse(afn, 'fasta')]
        tax = set()
        for i, record in enumerate(seqs):
            desc = dict(v.split('=') for v in record.description.split() if '=' in v)
            protid = desc['PROTID']
            tax.add(desc['TAXID'])
            rename[protid] = 'a%03d' % i
            record.id = rename[protid]
        if len(tax) < len(seqs):
            # not one-to-one
            continue
        SeqIO.write(seqs, ofb + '.phy', 'phylip-sequential')
        
        tree = dendropy.Tree.get(path=tfn, schema='newick', suppress_leaf_node_taxa=True)

        for leaf in tree.leaf_node_iter():
            leaf.taxon = dendropy.Taxon(rename[leaf.annotations['PR']])
        tree.deroot()
        tree.write(path=ofb + '.nwk',
                   schema='newick', suppress_annotations=True,
                   suppress_rooting=True,
                   suppress_internal_node_labels=True)

        mf = open(ofb + '.map', 'w')
        for k, v in rename.iteritems():
            print >> mf, '%s\t%s' % (k, v)
