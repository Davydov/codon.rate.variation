#!/usr/bin/env python3
## extract tax name from selectome trees
import os.path
from glob import iglob

import dendropy


if __name__ == '__main__':
    for tree_file in iglob('conv/*.nwk'):
        dataset = os.path.basename(tree_file).rsplit('.', 1)[0]
        nhx_fn = os.path.join('Selectome_v06_Drosophila-Trees_NHX', dataset + '.nhx')
        selectome_tree = dendropy.Tree.get(path=nhx_fn, schema='newick',
                                           suppress_internal_node_taxa=True,
                                           suppress_leaf_node_taxa=True)
        for node in selectome_tree.leaf_nodes():
            print(node.annotations['PR'].value,
                  node.annotations['S'].value)
