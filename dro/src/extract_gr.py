#!/usr/bin/env python
## exract genomic coordinates for RRC
import csv
import re

reg = re.compile('(\d+)\.\.(\d+)')

def mmax(a, b):
    if a is None or a < b:
        return b
    return a

def mmin(a, b):
    if a is None or a > b:
        return b
    return a

for line in open('data/cds_search.csv'):
    row = line.strip().split(',', 4)
    dataset = row[0]
    desc = row[-1]
    loc = [e for e in desc.split() if e.startswith('loc=')][0]
    loc = loc.split('=', 1)[1]
    ch, loc = loc.split(':', 1)
    if ch not in ('2L', '2R', '3L', '3R', 'X', '4'):
        continue
    start = None
    end = None
    for m in reg.finditer(loc):
        a = int(m.group(1))
        b = int(m.group(2))
        start = mmin(start, a)
        start = mmin(start, b)
        end = mmax(end, a)
        end = mmax(end, b)
    assert start < end
    print '%s %s:%d..%d' % (dataset, ch, start, end)
