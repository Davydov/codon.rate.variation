#!/usr/bin/env python3
## map selectome branches to reference tree
import os.path
from glob import iglob

import numpy as np
import dendropy


def neg(v):
    assert v in b'01?'
    if v == b'0':
        return b'1'
    elif v == b'1':
        return b'0'
    return b'?'

def negvec(v):
    return np.fromiter((neg(v) for v in v), dtype='S1')

def get_bipart(n, species):
    sub_nodes = set(n.taxon.label for n in n.leaf_iter())
    b1 = np.fromiter((b'1' if l in sub_nodes else b'0'
                      for l in species), dtype='S1')
    b2 = negvec(b1)
    return b1, b2

def compat(bi, b1, b2):
    v1 = np.logical_or(
        bi==b'?', bi==b1
    )
    v2 = np.logical_or(
        bi==b'?', bi==b2
    )
    return np.all(v1) or np.all(v2)

def rename_tips_short(t):
    for node in t.leaf_nodes():
        node.taxon.label = 'DRO' + node.taxon.label.split(' ', 1)[1][:2].upper()

def rename_tips(t, m):
    for node in t.leaf_nodes():
        node.taxon.label = m[node.taxon.label]

def label_nodes(t):
    for i, node in enumerate(t.internal_nodes()):
        node.label = str(i)


if __name__ == '__main__':
    ref_tree = dendropy.Tree.get(path='tree/Drosophila_species.nwk',
                                 schema='newick')
    label_nodes(ref_tree)
    ref_tree.write(path='reference_tree.nwk',
                   schema='newick')
    rename_tips_short(ref_tree)
    species = [t.label for t in ref_tree.taxon_namespace]
    nodes = []
    for node in ref_tree.internal_nodes():
        b1, b2 = get_bipart(node, species)
        nodes.append([int(node.label), b1, b2])

    ## load flybase protein to species mapping
    with open('fbpp2tax.txt') as f:
        f2t = dict(l.split() for l in f)

    for tree_file in iglob('conv/*.nwk'):
        tree = dendropy.Tree.get(path=tree_file, schema='newick')
        dataset = os.path.basename(tree_file).rsplit('.', 1)[0]
        map_name = tree_file.rsplit('.', 1)[0] + '.map'
        with open(map_name) as mapf:
            names_map = dict(reversed(l.split()) for l in mapf)
        rename_tips(tree, names_map)
        rename_tips(tree, f2t)

        ## this replicates pbranch.py with no parameters
        for i, node in enumerate(tree.internal_nodes(exclude_seed_node=True),
                                 start=1):
            t_b1, t_b2 = get_bipart(node, species)
            node.label = '#1'
            br_schema = tree.as_string(schema='newick', suppress_leaf_taxon_labels=True,
                                 suppress_edge_lengths=True)[:-2]
            node.label = ''
            for ref_node, b1, b2 in nodes:
                if compat(t_b1, b1, b2):
                    assert compat(t_b2, b1, b2)
                    print(dataset, br_schema, ref_node)
                    break
            else:
                print(dataset, br_schema, 'NA')

    
    #for fn in iglob(
