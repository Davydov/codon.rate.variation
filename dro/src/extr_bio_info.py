#!/usr/bin/env python
## extracts info about alignments from drosophila
## it relies on the gtf file (corresponding to the
## apropriate ensemble release 18). output bioinfo.txt
import glob
import os.path
import dendropy
import gzip
import sys

import numpy as np
from collections import defaultdict

from Bio import AlignIO
from Bio.SeqIO.FastaIO import SimpleFastaParser

trid2ds = {}
ds2trid = {}

def get_n_branches_tlen(fn):
    t = dendropy.Tree.get(path=fn, schema='newick')
    t.deroot()
    return len(t.internal_nodes()), sum((n.edge_length for n in t.nodes()))

def gc(seq):
    at = len([l for l in seq if l in 'AT'])
    gc = len([l for l in seq if l in 'GC'])
    if at > 0 and gc > 0:
        return float(gc)/(gc+at)

def ln(seq):
    return len([l for l in seq if not l == '-'])

def get_gc(fn):
    with open(fn) as f:
        ali = AlignIO.read(f, 'fasta')
    dm = get_dm(ali, 7227)
    if dm is not None:
        transid = get_val(dm.description, 'TRANSID')
        dataset = os.path.basename(fn).rsplit('.', 2)[0]
        trid2ds[transid] = dataset
        ds2trid[dataset] = transid
    gc_values = []
    for record in ali:
        gc_values.append(gc(record.seq))
    return np.mean(gc_values), np.std(gc_values)

def get_len(fn):
    with open(fn) as f:
        ali = SimpleFastaParser(f)
        align_array = np.array([list(seq.upper()) for _, seq in ali], np.character)
    #encountered_symbols.update(set(align_array.flatten()))
    nchar = np.sum(np.logical_and(align_array != '-',
                                 align_array != 'N',
                                 align_array != 'X'), axis=0)
    return np.sum(nchar > 0)

def get_val(desc, var):
    return [item.split('=')[1] for item in desc.split() if item.startswith(var + '=')][0]

def get_dm(ali, taxid):
    for record in ali:
        tax = int(get_val(record.description, 'TAXID'))
        # d. melanogaster
        if tax == taxid:
            return record

def gtf_extra_to_dict(e):
    d = {}
    for entry in e.split(';'):
        if not entry.strip():
            continue
        k, v = entry.split()
        d[k] = v.replace('"', '')
    return d

datasets = set()
for fn in glob.iglob('data/conv/*.phy'):
    b = os.path.basename(fn).rsplit('.', 1)[0]
    datasets.add(b)

info = defaultdict(dict)
    
cnt = 0
#encountered_symbols = set()
for ds in datasets:
    info[ds]['nbranches'], info[ds]['tlen'] = get_n_branches_tlen(os.path.join('data/conv', ds + '.nwk'))
    info[ds]['alen'] = get_len(os.path.join('data/Selectome_v06_Drosophila-nt_masked', ds + '.nt_masked.fas'))
    info[ds]['gc.mean'], info[ds]['gc.sd'] = get_gc(os.path.join('data/Selectome_v06_Drosophila-nt_unmasked', ds + '.nt.fas'))
    #print info
    cnt += 1
    #if cnt == 100:
    #    break

#print >> sys.stderr, 'Encountered letters', encountered_symbols

with gzip.open('data/gtf/7227/Drosophila_melanogaster.BDGP5.18.gtf.gz') as gtf:
    for line in gtf:
        ch, ans, fety, start, end, score, strand, phase, extra = line.rstrip('\n').split('\t')
        de = gtf_extra_to_dict(extra)
        trid = de['transcript_id']
        # we convert indexes to python
        start = int(start) - 1
        end = int(end)
        if trid in trid2ds:
            ds = trid2ds[trid]
            if ds not in info:
                continue
            exnu = int(de['exon_number'])

            # don't know how to handle seq edits properly
            assert de.get('seqedit', 'false') == 'false'

            if fety == 'CDS':
                info[ds]['nexon'] = max(info[ds].get('nexon', -1), exnu, exnu)
                info[ds]['start'] = min(info[ds].get('start', 10000000000), start)
                info[ds]['end'] = max(info[ds].get('end', -1), end)
                info[ds]['len'] = info[ds].get('len', 0) + end - start + 1
            #print info[ds]

for ds in info:
    if 'end' in info[ds] and 'len' in info[ds]:
        info[ds]['lenintron'] = info[ds]['end'] - info[ds]['start'] + 1 - info[ds]['len']
        if info[ds]['lenintron'] < 0:
            print >> sys.stderr, 'start: %d, end: %d, len: %d' % (info[ds]['start'], info[ds]['end'], info[ds]['len'])
            print >> sys.stderr, 'negative intron length in tr, ds: %s %s' % (ds2trid[ds], ds)

print 'dataset\talen\tcdslen\tnexon\tlenintron\tnbranches\ttlen\tgc.mean\tgc.sd'
for ds in info:
    rec = info[ds]
    print '\t'.join(map(str, (ds,
                              rec.get('alen', 'NA'),
                              rec.get('len', 'NA'),
                              rec.get('nexon', 'NA'),
                              rec.get('lenintron', 'NA'),
                              rec['nbranches'],
                              rec['tlen'],
                              rec['gc.mean'],
                              rec['gc.sd'])))
