#!/usr/bin/env python
## extracts positions relative to splicing site
import gzip
import sys
import glob
import numpy as np
import os.path
from collections import Counter, defaultdict

from Bio import AlignIO


def get_val(desc, var):
    return [item.split('=')[1] for item in desc.split() if item.startswith(var + '=')][0]

def get_dm(ali, taxid):
    for record in ali:
        tax = int(get_val(record.description, 'TAXID'))
        # d. melanogaster
        if tax == taxid:
            return record

def gtf_extra_to_dict(e):
    d = {}
    for entry in e.split(';'):
        if not entry.strip():
            continue
        k, v = entry.split()
        d[k] = v.replace('"', '')
    return d

def cdspos(cds):
    d = {}
    pos = 0
    for exn in sorted(cds.keys()):
        d[exn] = pos
        assert cds[exn][0] <= cds[exn][1]
        if cds[exn][0] == cds[exn][1]:
            print >> sys.stderr, "warning zero size cds in", trid
        pos += cds[exn][1] - cds[exn][0]
    return d

def reverse_d(d, maxend):
    res = {}
    for i in d:
        start = maxend - d[i][1]
        end = maxend - d[i][0]
        res[i] = (start, end)
    return res

def reverse_ce(cds, exon):
    maxend = -1
    for exnu in exon:
        maxend = max(maxend, exon[exnu][1])

    newexon = reverse_d(exon, maxend)
    newcds = reverse_d(cds, maxend)
    return newcds, newexon


def get_exon_positions(cds, exon, strand, N, cnt=[0, 0]):
    if strand == '-':
        cds, exon = reverse_ce(cds, exon)
    starts = cdspos(cds)
    before = {}
    after = {}
    lastexnu = max(cds.keys())
    codonlen = (starts[lastexnu] + cds[lastexnu][1] - cds[lastexnu][0])
    #print >> sys.stderr, codonlen
    #print >> sys.stderr, cds
    assert codonlen % 3 == 0
    codonlen /= 3

    for exnu in sorted(cds.keys()):
        ## ignore first codon
        if exnu == 1:
            continue
        c = cds[exnu]
        e = exon[exnu]
        clen = c[1] - c[0]
        maxlen = clen / 2
        assert c[0] >= e[0]
        assert c[1] <= e[1]
        #print >> sys.stderr, c, e
        if c[0] == e[0]:
            cnt[0] += 1
            for i in xrange(min(maxlen, N)):
                pos = starts[exnu] + i
                codonpos = pos / 3
                if i == 0 or pos % 3 == 0:
                    after[codonpos] = codonpos * 3 - starts[exnu]
        if c[1] == e[1]:
        #if c[1] == e[1] or (exnu == lastexnu and c[1] + 1 == e[1]):
            cnt[1] += 1
            for i in xrange(min(maxlen, N)):
                pos = starts[exnu] + clen - 1 - i
                codonpos = pos / 3
                if i == 0 or pos % 3 == 2:
                    if codonpos < codonlen:
                        before[codonpos] = starts[exnu] + clen - 1 - (codonpos * 3 + 2)
                    else:
                        assert exnu == lastexnu

    #print >> sys.stderr, cnt
    return before, after

def defined(seq):
    return np.array(
            [False if '-' in seq[i:i+3] or 'n' in seq[i:i+3] else True  for i in xrange(0, len(seq), 3)]
            )

def coverage(ali):
    de = [defined(r.seq) for r in ali]
    pos = np.sum(de, axis=0) > 0.7 * len(ali)
    return pos

def convpos(pos, seq):
    j = -1
    res = {}
    pos = set(pos)
    for i in xrange(0, len(seq), 3):
        if '-' not in seq[i:i+3]:
            j += 1
            if j in pos:
                res[j] = i/3
    assert len(pos) == len(res)
    return res

def read_rates(fn):
    res = defaultdict(list)
    for line in gzip.open(fn):
        ds, pos, rate = line.strip().split()
        pos=int(pos)
        rate=float(rate)
        res[ds].append(rate)
        # this only works if site positions are in order, no gaps
        assert len(res[ds]) == pos + 1
    for ds in res:
        res[ds] = np.array(res[ds], dtype=float)
    return res


if __name__ == '__main__':
    # drosophila melanogaster
    taxid = 7227
    acs = set()
    # get all 1-1 sets
    for fn in glob.iglob('conv/*.phy'):
        acs.add(os.path.basename(fn).rsplit('.', 1)[0])

    trlen = {}
    # extr d.m seq
    for fn in glob.iglob('Selectome_v06_Drosophila-nt_unmasked/*.nt.fas'):
        ac = os.path.basename(fn).rsplit('.', 2)[0]
        if ac in acs:
            ali = AlignIO.read(fn, 'fasta')
            assert len(ali) <= 12
            assert ali.get_alignment_length() % 3 == 0
            dm = get_dm(ali, taxid)
            if dm is not None:
                trid = get_val(dm.description, 'TRANSID')

                trlen[trid] = len(str(dm.seq).replace('-', ''))

    cdslength = Counter()
    exons = defaultdict(dict)
    cds = defaultdict(dict)
    startcodon = {}
    startexon = {}
    strands = {}
    chrs = {}
    with gzip.open('gtf/7227/Drosophila_melanogaster.BDGP5.18.gtf.gz') as gtf:
        for line in gtf:
            ch, ans, fety, start, end, score, strand, phase, extra = line.rstrip('\n').split('\t')
            de = gtf_extra_to_dict(extra)
            trid = de['transcript_id']
            # we convert indexes to python
            start = int(start) - 1
            end = int(end)
            if trid in trlen:
                exnu = int(de['exon_number'])
                # assert strand is equal on all entries for specifict trid
                if trid in strands:
                    assert strands[trid] == strand
                else:
                    strands[trid] = strand

                # assert chr is equal on all entries for specifict trid
                if trid in chrs:
                    assert chrs[trid] == ch
                else:
                    chrs[trid] = ch

                # don't know how to handle seq edits properly
                assert de.get('seqedit', 'false') == 'false'

                if fety == 'CDS':
                    cdslength[trid] += end - start
                    assert exnu not in cds[trid]
                    cds[trid][exnu] = (start, end)
                elif fety == 'exon':
                    assert exnu not in exons[trid]
                    exons[trid][exnu] = (start, end)
                elif fety == 'start_codon':
                    if trid in startcodon:
                        print >> sys.stderr, 'double start codon for', trid
                    startcodon[trid] = (start, end)
                    startexon[trid] = exnu


    assert set(trlen.keys()) == set(cdslength.keys())
    bad = set()
    for trid in trlen:
        ## check if length matches
        if trlen[trid] != cdslength[trid]:
            bad.add(trid)
            print >> sys.stderr, 'length mismatch', trid, trlen[trid], cdslength[trid]
            continue
        else:
            assert trlen[trid] % 3 == 0

        ## check that we have all the exons and cds
        assert len(exons[trid]) == max(exons[trid].keys())
        assert max(cds[trid].keys()) <= max(exons[trid].keys())

    ## let's extract all exon positions
    N=100
    befores = {}
    afters = {}
    for trid in trlen:
        if trid in bad:
            continue


        #print >> sys.stderr, trid
        befores[trid], afters[trid] = get_exon_positions(cds[trid], exons[trid], strands[trid], N)

    # read rates
    rates = read_rates('rates/m8_cg.txt.gz')
    rates_omega = read_rates('rates/m8_co.txt.gz')

    # extr d.m seq
    cnt = 0
    print 'dataset pos junction distance rate omega'
    for fn in glob.iglob('Selectome_v06_Drosophila-nt_unmasked/*.nt.fas'):
        ac = os.path.basename(fn).rsplit('.', 2)[0]
        if ac in acs:
            ali = AlignIO.read(fn, 'fasta')
            dm = get_dm(ali, taxid)
            if dm is not None:
                print >> sys.stderr, cnt, ac
                cnt += 1
                trid = get_val(dm.description, 'TRANSID')
                if trid in befores:
                    cov = coverage(ali)
                    c2a = convpos(befores[trid].keys() + afters[trid].keys(), dm)
                    if not ac in rates or rates[ac] is None:
                        continue
                    for pos in befores[trid]:
                        cpos = c2a[pos]
                        if cov[cpos]:
                            print ac, pos, 'before', befores[trid][pos], rates[ac][cpos], rates_omega[ac][cpos]
                    for pos in afters[trid]:
                        cpos = c2a[pos]
                        if cov[cpos]:
                            print ac, pos, 'after', afters[trid][pos], rates[ac][cpos], rates_omega[ac][cpos]
