#!/usr/bin/env python
## takes rates from different subtrees and averages them
import shelve
import sys
import cPickle as pickle
from collections import defaultdict
import numpy as np

def print_rates(record, dataset, hyp):
    for i, rate in enumerate(record[hyp]['codonGammaRates']):
        print dataset, branch, hyp, i, rate

def ktods(s):
    return s.rsplit('.', 3)[0]

def readtpp(fn):
    f = open(fn)
    f.next()
    d = {}
    for line in f:
        dataset, ap, pp = line.split()
        d[(dataset, int(ap))] = int(pp)
    return d

def read_rates(ds):
    mykk = [k for k in kk if k.startswith(ds) and k.endswith('BSG')]
    rates=defaultdict(list)
    for k in mykk:
        try:
            cgr = db[k]['H0']['codonGammaRates']
        except KeyError:
            print >> sys.stderr, 'no cGR in %s' % k
            continue
        for i, rate in enumerate(cgr):
            rates[i].append(rate)
            continue
            t = (ds, i)
            if t in pp:
                rates[pp[t]].append(rate)
    for pos in rates:
        print ds, pos, np.mean(rates[pos])

if __name__ == '__main__':
    try:
        pass
        #pp =  pickle.load(open('tpp.data'))
    except IOError:
        pp = readtpp('toproteinpositions.txt')
        pickle.dump(pp, open('tpp.data', 'w'))
    db = shelve.open('dro.db')
    ## find 100 random bsg trees
    kk = db['keys']
    allds = [ktods(k) for k in kk]
    print 'dataset pos rate'
    for ds in allds:
        read_rates(ds)
        

    db.close()
