#!/usr/bin/env python
## extract dm sequences for blast
import sys
import os
import glob
from Bio import SeqIO
from Bio.Seq import Seq

def remgap(rec):
    rec.seq = Seq(str(rec.seq).replace('-', ''))

for fn in glob.iglob('data/Selectome_v06_Drosophila-nt_unmasked/*.fas'):
    dataset = os.path.basename(fn).rsplit('.', 2)[0]
    if not os.path.exists(os.path.join('data', 'conv', dataset + '.phy')):
        continue
    for rec in SeqIO.parse(fn, 'fasta'):
        if rec.description.endswith('TAXID=7227'):
            remgap(rec)
            rec.id = dataset
            rec.description = ''
            SeqIO.write(rec, sys.stdout, 'fasta')
            break
