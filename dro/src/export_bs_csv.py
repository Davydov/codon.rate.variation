#!/usr/bin/env python
## export branch-site results to csv
import sys
import shelve
import json
import re

def readp(fn):
    return json.load(open(fn))['optimizers'][0]

def strpar(r):
    d = r['maxLParameters']
    d['omega2'] = d.get('omega2', 'NA')
    d['alpha'] = d.get('alphac', d.get('alphas', 'NA'))
    pars = ['omega0', 'omega2', 'p01sum', 'p0prop', 'kappa', 'alpha']
    template = '\t'.join(('{{{}}}'.format(p) for p in pars))
    return template.format(
            **d)

def add_suffix(l, suffix):
    return [v + suffix for v in l]

def print_header():
    names = ['dataset', 'branch', 'model']
    pars = ['lnL', 'w0', 'w2', 'p01sum', 'p0prop', 'k', 'alpha']
    names.extend(add_suffix(pars, '.0'))
    names.extend(add_suffix(pars, '.1'))

    print '\t'.join(names)

if __name__ == '__main__':
    print_header()
    i = 0
    db = shelve.open(sys.argv[1])
    for fn in db:
        if fn == 'keys':
            continue
        dataset, model, family = fn.rsplit('.', 2)
        if family != 'BS':
            continue
        r = db[fn]

        for tst in r['tests']:
            branch = re.sub(':[0-9.e-]+', '', tst['tree']).rstrip(';')
            branch = re.sub('a[0-9]+', '', branch)
            r0 = tst['H0']
            r1 = tst['H1']
            print '{0}\t{1}\t{2}\t{3[maxLnL]}\t{4}\t{5[maxLnL]}\t{6}'.format(
                dataset, branch, model, r0, strpar(r0), r1, strpar(r1))
        if i % 1000 == 0:
            print >> sys.stderr, dataset, i
        i += 1

