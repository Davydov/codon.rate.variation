#!/usr/bin/env python3
## convert dataset from selectome to geneid (in melanogasted)
from glob import iglob
from os.path import basename

def get_prot_id(fn, taxid='7227'):
    with open(fn) as f:
        for line in f:
            if line.startswith('>'):
                record = {}
                for e in line.split():
                    if '=' in e:
                        k, v = e.split('=', 1)
                        record[k] = v
                if record['TAXID'] == taxid:
                    return record


if __name__ == '__main__':
    for fn in iglob('Selectome_v06_Drosophila-nt_masked/*.fas'):
        b = basename(fn).rsplit('.', 2)[0]
        record = get_prot_id(fn)
        if record and record.get('GENEID', None):
            print(b, record['GENEID'])
