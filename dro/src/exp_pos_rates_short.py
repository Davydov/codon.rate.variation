#!/usr/bin/env python
## takes rates from different subtrees and compares them
import shelve
import sys
import cPickle as pickle
from collections import defaultdict
import numpy as np

def print_rates(record, dataset, hyp):
    for i, rate in enumerate(record[hyp]['codonGammaRates']):
        print dataset, branch, hyp, i, rate

def ktods(s):
    return s.rsplit('.', 3)[0]

def readtpp(fn):
    f = open(fn)
    f.next()
    d = {}
    for line in f:
        dataset, ap, pp = line.split()
        d[(dataset, int(ap))] = int(pp)
    return d

def get_rates(ds):
    print ds
    mykk = [k for k in kk if k.startswith(ds) and k.endswith('BSG')]
    rates = []
    for k in mykk:
        try:
            cgr = db[k]['H0']['codonGammaRates']
            rates.append(np.array(cgr))
        except KeyError:
            print >> sys.stderr, 'no cGR in %s' % k
            continue
    if not rates:
        return
    return np.mean(rates, axis=0)

if __name__ == '__main__':
    db = shelve.open('dro.db')
    ## find 100 random bsg trees
    kk = db['keys']
    allds = {ktods(k) for k in kk}
    res = {ds: get_rates(ds) for ds in allds}
    
    np.save('rates', res)


    db.close()
