#!/usr/bin/env python
## takes rates from m8 and exports them
import sys
import os
import glob
import json
from collections import defaultdict
import numpy as np

def print_rates(ds, rates):
    for pos, r in enumerate(rates):
        print ds, pos, r

if __name__ == '__main__':
    for fn in glob.iglob(sys.argv[1] + '/*.json'):
        with open(fn) as f:
            record = json.load(f)
        dataset = os.path.basename(fn).rsplit('.', 3)[0]
        print_rates(dataset, record['H1']['final'][sys.argv[2]])
        

