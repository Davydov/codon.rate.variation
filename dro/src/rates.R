## import sites rates and plot them
library(ggplot2)
theme_set(theme_bw())
library(gridExtra)
library(plyr)
library(gdata)
library(pbapply)
library(magrittr)


## computes quantile of mean (or median) estimate
mq <- function(x, q, N=100, p=0.5, FUN=mean) {
	quantile(sapply(1:N, function(y) FUN(sample(x, length(x) * p))), q)
}

## read estimated site rates
erates  <- read.table('erates2.txt', header=T, fill=T)
## convert distrances before junction to negative
erates[erates$junction=='before', 'distance']  <- -erates[erates$junction=='before','distance']
##erates[erates$junction=='before', 'distance']  <- -3 -erates[erates$junction=='before','distance']
erates$junction <- factor(erates$junction, levels=c('before', 'after'))
erates$junction.name <- revalue(erates$junction, c('before'="5' exon", 'after'="3' exon"))


png('splicing.png')

ggplot(
    subset(na.omit(erates),
           (junction=='before' & distance <= 0) | (junction=='after' & distance >= 0) ), aes(distance, rate)) +
	stat_summary(geom="ribbon",
		     fun.ymin = function(x) mq(x, 0.01),
		     fun.ymax = function(x) mq(x, 0.99),
		     alpha=0.5, fill='blue') +
	stat_summary(fun.y = "mean", geom = "line") +
	facet_wrap(~junction, scales='free_x')

dev.off()

## paper; plot rates before and after splicing junction
pdf('dro-splicing.pdf', width=4, height=4)
ggplot(
    subset(na.omit(erates),
           (junction=='before' & distance <= 0) | (junction=='after' & distance >= 0) ), aes(distance, rate)) +
	stat_summary(geom="ribbon",
		     fun.ymin = function(x) mq(x, 0.01),
		     fun.ymax = function(x) mq(x, 0.99),
		     alpha=0.5, fill='blue') +
	stat_summary(fun.y = "mean", geom = "line") +
    facet_wrap(~junction.name, scales='free_x') +
    labs(y='Substitution rate', x='Distance from the junction to a codon')
ggplot(
    subset(na.omit(erates),
    ((junction=='before' & distance <= 0) | (junction=='after' & distance >= 0))
    ##& ((distance >= 0 & distance%%3==0)|(distance < 0 & (distance+1)%%3==0))
    ), aes(distance, omega)) +
	stat_summary(geom="ribbon",
		     fun.ymin = function(x) mq(x, 0.01),
		     fun.ymax = function(x) mq(x, 0.99),
		     alpha=0.5, fill='blue') +
	stat_summary(fun.y = "mean", geom = "line") +
    facet_wrap(~junction.name, scales='free_x') +
    labs(y=expression(omega), x='Distance from the junction to a codon')
dev.off()


## minimum gene length (we remove short genes to avoid biases)
min.len <- 100

## read rates (rho) from M8
pos.rates.alpha <- read.table('m8_cg.conv.txt.gz', col.names=c('dataset', 'pos', 'rate'))
long.genes <- subset(pos.rates.alpha, pos==min.len)$dataset
## keep only long genes and first 100 codons
pos.rates.alpha <- subset(pos.rates.alpha, dataset %in% long.genes & pos<min.len)

## read site estimates of omega  from M8
pos.rates.omega <- read.table('m8_co.conv.txt.gz', col.names=c('dataset', 'pos', 'rate'))
## keep only long genes and first 100 codons
pos.rates.omega <- subset(pos.rates.omega, dataset %in% long.genes & pos<min.len)


png('gene_start_%d.png')

## plot median estimates of codon rho and site omega
grid.arrange(
    ggplot(subset(pos.rates.omega, pos>0), aes(pos*3, rate)) +
    stat_summary(geom='ribbon',
                 fun.ymin = function(x) mq(x, 0.01, FUN=median),
                 fun.ymax = function(x) mq(x, 0.99, FUN=median),
                 alpha=0.5, fill='blue') +
    stat_summary(fun.y = "median", geom = "line") +
    labs(x='Position', y=expression(omega)),
    ggplot(subset(pos.rates.alpha, pos>0), aes(pos*3, rate)) +
    stat_summary(geom='ribbon',
                 fun.ymin = function(x) mq(x, 0.01, FUN=median),
                 fun.ymax = function(x) mq(x, 0.99, FUN=median),
                 alpha=0.5, fill='blue') +
    stat_summary(fun.y = "median", geom = "line") +
    labs(x='Position', y="Substitution rate")
)

## plot mean estimates of codon rho and site omega
grid.arrange(
    ggplot(subset(pos.rates.omega, pos>0), aes(pos*3, rate)) +
    stat_summary(geom='ribbon',
                 fun.ymin = function(x) mq(x, 0.01, FUN=mean),
                 fun.ymax = function(x) mq(x, 0.99, FUN=mean),
                 alpha=0.5, fill='blue') +
    stat_summary(fun.y = "mean", geom = "line") +
    labs(x='Position', y=expression(omega)),
    ggplot(subset(pos.rates.alpha, pos>0), aes(pos*3, rate)) +
    stat_summary(geom='ribbon',
                 fun.ymin = function(x) mq(x, 0.01, FUN=mean),
                 fun.ymax = function(x) mq(x, 0.99, FUN=mean),
                 alpha=0.5, fill='blue') +
    stat_summary(fun.y = "mean", geom = "line") +
    labs(x='Position', y="Substitution rate")
)

dev.off()


## paper; plot omega and rho at the start of gene
pdf('dro-start.pdf', width=4, height=4)
grid.arrange(
    ggplot(subset(pos.rates.omega, pos>0), aes(pos*3, rate)) +
    stat_summary(geom='ribbon',
                 fun.ymin = function(x) mq(x, 0.01, FUN=median),
                 fun.ymax = function(x) mq(x, 0.99, FUN=median),
                 alpha=0.5, fill='blue') +
    stat_summary(fun.y = "median", geom = "line") +
    labs(x='', y=expression(omega)),
    ggplot(subset(pos.rates.alpha, pos>0), aes(pos*3, rate)) +
    stat_summary(geom='ribbon',
                 fun.ymin = function(x) mq(x, 0.01, FUN=median),
                 fun.ymax = function(x) mq(x, 0.99, FUN=median),
                 alpha=0.5, fill='blue') +
    stat_summary(fun.y = "median", geom = "line") +
    labs(x='Position', y="Substitution rate")
)
dev.off()


## relative omega rates
## paper; plot relative omega and rho at the start of gene
pdf('dro-start-relative.pdf', width=8, height=8)
pos.rates.omega %>%
    dplyr::group_by(dataset) %>%
    dplyr::filter(pos>0) %>%
    dplyr::mutate(medrate=median(rate), rate=medrate/rate, variable='omega') %>%
    dplyr::select(-medrate) %>%
    dplyr::bind_rows(pos.rates.alpha %>% dplyr::filter(pos>0) %>% mutate(variable='rate')) %>%
    ggplot(aes(pos*3, rate, color=variable, fill=variable)) +
    stat_summary(geom='ribbon',
                 fun.ymin = function(x) mq(x, 0.01, FUN=median),
                 fun.ymax = function(x) mq(x, 0.99, FUN=median),
                 alpha=0.5) +
    stat_summary(fun.y = "median", geom = "line") +
    labs(x='Position', y='value') +
    scale_color_manual(values=c('red', 'blue'), labels=expression(median(omega)/omega, substituion~rate)) +
    scale_fill_manual(values=c('pink', 'lightblue'), labels=expression(median(omega)/omega, substituion~rate))
dev.off()

## check for omega & rho correlation
pos.rates.omega %>%
    dplyr::group_by(dataset) %>%
    dplyr::filter(pos>0) %>%
    dplyr::mutate(medrate=median(rate), rate=rate/medrate, variable='omega') %>%
    dplyr::select(-medrate) %>%
    dplyr::bind_rows(pos.rates.alpha %>% dplyr::filter(pos>0) %>% mutate(variable='rho')) %>%
    dplyr::filter(rate>0.1 & rate < 10) %>%
    tidyr::spread(variable, rate) %>%
    ggplot(aes(omega, rho)) +
    geom_point(alpha=0.2) +
    geom_density2d()


## expression levels

expr <- read.table('../expr_processed.txt')
expr <- subset(expr, select=c(dataset, mean.expr, max.expr))

## merge codon rates with expression values
pos.rates.alpha.e <- pos.rates.alpha
pos.rates.alpha.e <- merge(pos.rates.alpha.e, expr)

pos.rates.omega.e <- pos.rates.omega
pos.rates.omega.e <- merge(pos.rates.omega.e, expr)

## split mean expression into 4 quantiles
a.q <- quantile(pos.rates.alpha.e$mean.expr, seq(0, 1, 0.25))

str(cut(pos.rates.omega.e$mean.expr, a.q))


## paper; plot omega posterior estimates at gene start split by
## expression quantiles
pdf('dro-omega-q.pdf', width=5, height=5)
tmp <- subset(pos.rates.omega.e, pos>0 & !is.na(mean.expr))
tmp$cut <- cut(tmp$mean.expr, a.q, include.lowest=T)
levels(tmp$cut) <- c('0-25%', '25-50%', '50-75%', '75-100%')
ggplot(tmp, aes(pos*3, rate)) +
    stat_summary(geom='ribbon',
                 fun.ymin = function(x) mq(x, 0.01, FUN=median),
                 fun.ymax = function(x) mq(x, 0.99, FUN=median),
                 alpha=0.5, fill='blue') +
    stat_summary(fun.y = "median", geom = "line") +
    labs(x='Position', y=expression(omega)) +
    facet_wrap(~ cut) +
    scale_y_log10(breaks=seq(0.01, 0.06, 0.005))
rm(tmp)
dev.off()


## paper; plot rho posterior estimates at gene start split by
## expression quantiles
pdf('dro-rate-q.pdf', width=5, height=5)
tmp <- subset(pos.rates.alpha.e, pos>0 & !is.na(mean.expr))
tmp$cut <- cut(tmp$mean.expr, a.q, include.lowest=T)
levels(tmp$cut) <- c('0-25%', '25-50%', '50-75%', '75-100%')
ggplot(tmp, aes(pos*3, rate)) +
    stat_summary(geom='ribbon',
                 fun.ymin = function(x) mq(x, 0.01, FUN=median),
                 fun.ymax = function(x) mq(x, 0.99, FUN=median),
                 alpha=0.5, fill='blue') +
    stat_summary(fun.y = "median", geom = "line") +
    labs(x='Position', y='Substitution rate') +
    facet_wrap(~ cut)
rm(tmp)
dev.off()


## autocorrelation statistic
a.stat <- function(rates) {
    rates.minus <- rates
    rates.minus$pos <- rates.minus$pos + 1
    rates.merged <- merge(rates, rates.minus, by=c('dataset', 'pos'))
    rates.merged$diff <- rates.merged$rate.x - rates.merged$rate.y
    mean(abs(rates.merged$diff))
}

## shuffle posterior rates within genes
r.shuf <- function(rates) {
    ddply(rates, "dataset", transform, rate=sample(rate))
}

## compute mean rates for genes
r.mean <- function(rates) {
    ddply(rates, "dataset", summarise, av.rate=mean(rate))
}

## use small subset for permutations, otherwise it is very slow
pos.rates.alpha.small <- subset(pos.rates.alpha, dataset %in% sample(unique(pos.rates.alpha$dataset), 100))

## testing that mean is not changed while shuffling
head(r.mean(pos.rates.alpha.small))
head(r.mean(r.shuf(pos.rates.alpha.small)))
head(subset(pos.rates.alpha.small, dataset=='EMGT00050000000152.Drosophila.001'))
head(subset(r.shuf(pos.rates.alpha.small), dataset=='EMGT00050000000152.Drosophila.001'))


## compute real statistic value
a.stat(pos.rates.alpha.small)

## compute null distribution of statistic
null.dist <- sort(pbsapply(1:10000,
                           function(x) a.stat(r.shuf(pos.rates.alpha.small))))
# effect size
mean(null.dist)-a.stat(pos.rates.alpha.small)

#p-value
sum(a.stat(pos.rates.alpha.small)>=null.dist)/length(null.dist)
