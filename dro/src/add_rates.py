#!/usr/bin/env python
## addds site rates to the dro.db
## these results were computed independently
## so integration is needed
import shelve
import glob
import json
import os.path

if __name__ == '__main__':
    db = shelve.open('dro.db')
    for i, fn in enumerate(glob.iglob('ratesres/*/*.BSG0.json')):
        bn=os.path.basename(fn)
        ac=bn.rsplit('.', 1)[0][:-1]
        cr0 = json.load(open(fn))['model']['codonGammaRates']
        cr1 = json.load(open(fn.replace('BSG0','BSG1')))['model']['codonGammaRates']
        record = db[ac]
        record['H0']['codonGammaRates'] = cr0
        record['H1']['codonGammaRates'] = cr1
        db[ac]=record
        if i%1000 == 0:
            print i, ac
            db.sync()
    db.close()



