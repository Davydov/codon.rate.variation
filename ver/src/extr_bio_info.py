#!/usr/bin/env python
## extracts info about alignments from drosophila
## it relies on the gtf file (corresponding to the
## apropriate ensemble release 44). output bioinfo.txt
import glob
import os.path
import dendropy
import gzip
import numpy as np
import sys
from Bio import AlignIO
from collections import defaultdict

trid2ds = {}
ds2trid = {}

def get_n_branches_tlen(fn):
    t = dendropy.Tree.get(path=fn, schema='newick')
    t.deroot()
    return len(t.internal_nodes()), sum((n.edge_length for n in t.nodes()))

def gc(seq):
    at = len([l for l in seq if l in 'AT'])
    gc = len([l for l in seq if l in 'GC'])
    if at > 0 and gc > 0:
        return float(gc)/(gc+at)

def ln(seq):
    return len([l for l in seq if not l == '-'])

def get_species(m, s):
    for k in m:
        if k.startswith(s):
            return m[k]

def get_gcl(fn, mapfn):
    with open(fn) as f:
        ali = AlignIO.read(f, 'phylip-sequential')
    gc_values = []
    for record in ali:
        gc_values.append(gc(record.seq))
    return ali.get_alignment_length(), np.mean(gc_values), np.std(gc_values)
        
def get_val(desc, var):
    return [item.split('=')[1] for item in desc.split() if item.startswith(var + '=')][0]

def get_map(fn):
    d = {}
    with open(fn) as f:
        for line in f:
            k, v = line.strip().split()
            d[k] = v
    return d

def gtf_extra_to_dict(e):
    d = {}
    for entry in e.split(';'):
        if not entry.strip():
            continue
        k, v = entry.split()
        d[k] = v.replace('"', '')
    return d

def read_gtf(fn, d, g2t):
    with gzip.open(fn) as gtf:
        for line in gtf:
            ch, ans, fety, start, end, score, strand, phase, extra = line.rstrip('\n').split('\t')
            de = gtf_extra_to_dict(extra)
            gid = de['gene_id']
            trid = de['transcript_id']
            # we convert indexes to python
            start = int(start) - 1
            end = int(end)
            if gid in e2h:
                ds = e2h[gid]
                if ds not in info:
                    continue
                exnu = int(de['exon_number'])

                # don't know how to handle seq edits properly
                assert de.get('seqedit', 'false') == 'false'

                if fety == 'CDS':
                    g2t[gid].add(trid)
                    d[trid]['nexon'] = max(d[trid].get('nexon', -1), exnu, exnu)
                    d[trid]['start'] = min(d[trid].get('start', 10000000000), start)
                    d[trid]['end'] = max(d[trid].get('end', -1), end)
                    d[trid]['len'] = d[trid].get('len', 0) + end - start + 1



h2e = {}
e2h = {}
first = True
for line in open('data/h2e'):
    if first == True:
        first = False
        continue
    
    ac, ens = line.split()
    h2e[ac] = ens
    e2h[ens] = ac

datasets = set()
for fn in glob.iglob('data/Singleton/*.phy'):
    b = os.path.basename(fn).rsplit('.', 1)[0]
    datasets.add(b)

info = defaultdict(dict)
    
cnt = 0
pth = 'data/Singleton'
for ds in datasets:
    
    info[ds]['nbranches'], info[ds]['tlen'] = get_n_branches_tlen(os.path.join(pth, ds + '.M1.nwk'))
    n = os.path.join(pth, ds)
    info[ds]['alen'], info[ds]['gc.mean'], info[ds]['gc.sd'] = get_gcl(n + '.phy', n + '.rename.map')
    #print info
    cnt += 1
    #if cnt == 100:
    #    break


g2t = defaultdict(set)
tres = defaultdict(dict)
read_gtf('data/Homo_sapiens.NCBI36.44.gtf.gz', tres, g2t)
read_gtf('data/Mus_musculus.NCBIM36.44.gtf.gz', tres, g2t)

for gene in g2t:
    chosen = None
    mlen = -1
    for tran in g2t[gene]:
        if tres[tran]['len'] > mlen:
            chosen = tran
            mlen = tres[tran]['len']
    info[e2h[gene]].update(tres[tran])


for ds in info:
    if 'end' in info[ds] and 'len' in info[ds]:
        info[ds]['lenintron'] = info[ds]['end'] - info[ds]['start'] + 1 - info[ds]['len']
        if info[ds]['lenintron'] < 0:
            print >> sys.stderr, 'start: %d, end: %d, len: %d' % (info[ds]['start'], info[ds]['end'], info[ds]['len'])
            print >> sys.stderr, 'negative intron length in tr, ds: %s %s' % (h2e[ds], ds)

print 'dataset\talen\tcdslen\tnexon\tlenintron\tnbranches\ttlen\tgc.mean\tgc.sd'
for ds in info:
    rec = info[ds]
    print '\t'.join(map(str, (ds,
                              rec.get('alen', 'NA'),
                              rec.get('len', 'NA'),
                              rec.get('nexon', 'NA'),
                              rec.get('lenintron', 'NA'),
                              rec['nbranches'],
                              rec['tlen'],
                              rec['gc.mean'],
                              rec['gc.sd'])))
