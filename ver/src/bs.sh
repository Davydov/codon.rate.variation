#!/bin/bash
## example file on running godon with vertabrate dataset
#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J bsg2-ver[1-80]
#BSUB –R "select[tmp>1024],rusage[mem=4096]"
#BSUB -M 4194304
##BSUB -n 4
##BSUB -R "span[ptile=4]"

NAME=ver-bsg3
SUB=1-godon
SOURCE=dl3.tbz2

source $HOME/mysub/mysub.bash

export OMP_NUM_THREADS=1
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/software/Utility/nlopt/2.3/lib
PAR="-cpu 1 -method lbfgsb -model BSG -nobrlen -loglevel info"

cmd () {
	if [[ $1 == Singleton/*.phy ]]
	then
		phy=$1
		fst=${1%.*}.fst
		$HOME/dndstools/phy2fst.py $phy
		nwk=${1%.*}.M1.nwk
		$HOME/dndstools/pbranch.py --deroot $nwk
		d=$(dirname $1)
		for nnwk in ${nwk%.*}.*.nwk
		do
			bnwk=$(basename $nnwk)
			b=res/$(dirname $1)/${bnwk%.*}
			res=$b.BS0.norm.tr
			log=${res%.*}.out
			mkdir -p $(dirname $res)
			$CLUSTER/godon $PAR -fixw -log $log -out $res  $fst $nnwk
			res=$b.BS1.norm.tr
			log=${res%.*}.out
			$CLUSTER/godon $PAR -log $log -out $res  $fst $nnwk
			res=$b.BSCG0.norm.tr
			log=${res%.*}.out
			$CLUSTER/godon $PAR -ncatcg 4 -fixw -log $log -out $res  $fst $nnwk
			res=$b.BSCG1.norm.tr
			log=${res%.*}.out
			$CLUSTER/godon $PAR -ncatcg 4 -log $log -out $res  $fst $nnwk
			res=$b.BSSG0.norm.tr
			log=${res%.*}.out
			$CLUSTER/godon $PAR -ncatsg 4 -fixw -log $log -out $res  $fst $nnwk
			res=$b.BSSG1.norm.tr
			log=${res%.*}.out
			$CLUSTER/godon $PAR -ncatsg 4 -log $log -out $res  $fst $nnwk
		done
	fi
}

run
