#!/bin/bash
# run branch-site and M8 on all vertebrate data
#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J bs-ver[1-160]
#BSUB –R "select[tmp>1024],rusage[mem=6144]"
#BSUB -M 6291456
##BSUB -n 4
##BSUB -R "span[ptile=4]"

NAME=bsgp-ver-all
SUB=1-all
SOURCE=dl3.tbz2

source $HOME/mysub/mysub.bash

export OMP_NUM_THREADS=1
PAR="--procs 1 --seed 1 --log-level info"
GODON=$CLUSTER/godon-30fb0f4

cmd () {
	if [[ $1 == Singleton/*.phy ]]
	then
		phy=$1
		fst=${1%.*}.fst
		$HOME/dndstools/phy2fst.py $phy
		nwk=${1%.*}.M1.nwk
		bn=$(basename $phy)
		bn=${bn%.*}
		onwk=res/$bn.nwk

		log=res/$bn.nog.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BS --m0-tree --all-branches --no-leaves --codon-omega --json $json --trajectory $tr --out $log $fst $nwk 
		python -c "import json; print(json.load(open('$json')))['globalOptimizations'][0]['finalTree']" > $onwk

		log=res/$bn.sg.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BSG --no-branch-length --all-branches --no-leaves --ncat-site-rate 4 --site-rates --codon-omega --json $json --trajectory $tr --out $log $fst $onwk

		log=res/$bn.cg.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BSG --no-branch-length --all-branches --no-leaves --ncat-codon-rate 4 --codon-rates --codon-omega --json $json --trajectory $tr --out $log $fst $onwk

		log=res/$bn.cp.BS.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR BSG --no-branch-length --all-branches --no-leaves --ncat-codon-rate 3 --codon-rates --codon-omega --proportional --json $json --trajectory $tr --out $log $fst $onwk

		log=res/$bn.nog.M8.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR M8 --no-branch-length  --codon-omega --json $json --trajectory $tr --out $log $fst $onwk 

		log=res/$bn.sg.M8.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR M8 --no-branch-length --ncat-site-rate 4 --site-rates --codon-omega --json $json --trajectory $tr --out $log $fst $onwk

		log=res/$bn.cg.M8.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR M8 --no-branch-length --ncat-codon-rate 4 --codon-rates --codon-omega --json $json --trajectory $tr --out $log $fst $onwk

		log=res/$bn.cp.M8.log
		tr=${log%.*}.tr
		json=${log%.*}.json
		$GODON test $PAR M8 --no-branch-length --ncat-codon-rate 3 --codon-rates --codon-omega --proportional --json $json --trajectory $tr --out $log $fst $onwk
	fi
}

run
