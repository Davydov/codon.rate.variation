#!/usr/bin/env python
## convert homolens id into ensembl
import sys
import re

from bs4 import BeautifulSoup, Comment

ens = re.compile('[^A-Z0-9](ENS[A-Z0-9]+)')

if __name__ == '__main__':
    print 'ac Ensembl.Gene.ID'
    with open('data/Singleton.html') as f:
        soup = BeautifulSoup(f, 'lxml')
    table = soup('table')[0]
    for row in table('tr'):
        cells = row('td')
        ac = cells[0].text.strip()
        desc = cells[-1].text.strip()
        if not ac.startswith('HBG'):
            continue
        match = ens.search(desc)
        if match:
            print ac, match.group(1)
        else:
            print >> sys.stderr, "couldn't find for %s, desc: " % ac, desc
