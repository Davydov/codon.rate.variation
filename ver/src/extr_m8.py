#!/usr/bin/env python3
## extract maximum likelihood fro M8 results
import sys
import glob
import os.path

def time_in_seconds(t):
    sec = 0.0
    if 'h' in t:
        h, t = t.split('h')
        sec += float(h) * 3600
    if 'm' in t:
        m, t = t.split('m')
        sec += float(m) * 60
    assert t.endswith('s')
    sec += float(t[:-1])
    return sec


def pr(fn):
    record = {}
    for line in open(fn):
        if line.startswith('Maximum likelihood:'):
            record['L'] = float(line.rsplit(' ', 1)[1])
        if '=' in line:
            n, v = line.split('=')
            if n in ('p0', 'p', 'q', 'kappa', 'omega', 'alphas', 'alphac'):
                record[n] = float(v)
        if line.startswith('Running time:'):
            record['time'] = time_in_seconds(line.strip().rsplit(' ', 1)[1])
    return(record)

print('ac hyp method var val')
for fn in glob.glob('%s/*.out' % sys.argv[1]):
    d, model, method, _ = os.path.basename(fn).split('.')
    assert model in ('M8', 'M8a')
    if model == 'M8':
        hyp = 1
    elif model == 'M8a':
        hyp = 0
    record = pr(fn)
    for k in ('L', 'time', 'p0', 'p', 'q', 'kappa'):
        assert k in record
    for k, v in record.items():
        print(d, hyp, method, k, v)
