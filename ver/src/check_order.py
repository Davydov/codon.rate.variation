#!/usr/bin/env python
## check that order of seqs is the same in phy and fasta
import glob
import os.path
from Bio import AlignIO

for fn in glob.iglob('data/Singleton/*.phy'):
    ali = AlignIO.read(fn, 'phylip-sequential')
    ac = os.path.basename(fn).split('.')[0]
    ali_old = AlignIO.read('data/Singleton-or/%s_dna.fasta' % ac, 'fasta')
    print ac
    assert len(ali) == len(ali_old)
    assert ali.get_alignment_length() == ali_old.get_alignment_length()
