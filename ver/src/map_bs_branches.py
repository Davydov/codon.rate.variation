#!/usr/bin/env python3
## map branches to reference tree
import os.path
import re
from glob import iglob

import numpy as np
import dendropy


def neg(v):
    assert v in b'01?'
    if v == b'0':
        return b'1'
    elif v == b'1':
        return b'0'
    return b'?'

def negvec(v):
    return np.fromiter((neg(v) for v in v), dtype='S1')

def get_bipart(n, species):
    sub_nodes = set(n.taxon.label for n in n.leaf_iter())
    b1 = np.fromiter((b'1' if l in sub_nodes else b'0'
                      for l in species), dtype='S1')
    b2 = negvec(b1)
    return b1, b2

def compat(bi, b1, b2):
    v1 = np.logical_or(
        bi==b'?', bi==b1
    )
    v2 = np.logical_or(
        bi==b'?', bi==b2
    )
    return np.all(v1) or np.all(v2)

def rename_tips(t, m):
    for node in t.leaf_nodes():
        node.taxon.label = m[node.taxon.label]

def label_nodes(t):
    for i, node in enumerate(t.internal_nodes()):
        node.label = str(i)

def read_spmap(fn):
    d = {}
    with open(fn) as f:
        for line in f:
            line = line.strip()
            abbr, sp = line.split('\t', 1)
            d[abbr] = sp
    return d

def ac2sp(n, d):
    m = re.search('^[A-Z]{2,5}', n).group(0)
    if m == 'PTX':
        m = 'PT'
    return d[m]

if __name__ == '__main__':
    abbr2sp = read_spmap('species.txt')

    ref_tree = dendropy.Tree.get(path='reference.nwk',
                                 schema='newick')
    label_nodes(ref_tree)
    ref_tree.write(path='reference_tree.nwk',
                   schema='newick')

    species = [t.label for t in ref_tree.taxon_namespace]
    nodes = []
    for node in ref_tree.internal_nodes():
        b1, b2 = get_bipart(node, species)
        nodes.append([int(node.label), b1, b2])

    for tree_file in iglob('Singleton/*.M0.nwk'):
        tree = dendropy.Tree.get(path=tree_file, schema='newick')
        dataset = os.path.basename(tree_file).rsplit('.', 2)[0]
        map_name = tree_file.rsplit('.', 2)[0] + '.rename.map'
        with open(map_name) as mapf:
            names_map = dict(reversed(l.split()) for l in mapf)
        rename_tips(tree, names_map)
        names_map2 = {t.label: ac2sp(t.label, abbr2sp) for t in tree.taxon_namespace}
        rename_tips(tree, names_map2)
        tree.deroot()

        ## this replicates pbranch.py with no parameters
        for i, node in enumerate(tree.internal_nodes(exclude_seed_node=True),
                                 start=1):
            t_b1, t_b2 = get_bipart(node, species)
            node.label = '#1'
            br_schema = tree.as_string(schema='newick', suppress_leaf_taxon_labels=True,
                                       suppress_edge_lengths=True, suppress_rooting=True)[:-2]
            node.label = ''
            for ref_node, b1, b2 in nodes:
                if compat(t_b1, b1, b2):
                    assert compat(t_b2, b1, b2)
                    print(dataset, br_schema, ref_node)
                    break
            else:
                print(dataset, br_schema, 'NA')

