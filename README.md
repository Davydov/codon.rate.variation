This is supporting information for the manuscript: [Large-Scale
Comparative Analysis of Codon Models Accounting for Protein and
Nucleotide Selection](https://doi.org/10.1101/174839).

The repository is split into three parts: simulations (`sim`),
vertebrate dataset (`ver`) and Drosophila dataset (`dro`).

You might need the following programs to run everything:

- [Godon](https://bitbucket.org/Davydov/godon) for model parameter
  estimation.
- [HYPHY](http://www.hyphy.org/) for BUSTED.
- [cosim](https://bitbucket.org/Davydov/cosim) for simulations.
- [mysub](https://bitbucket.org/Davydov/mysub) for the job submission
  (this will be probably useful only on LSF).
- [phyml](https://github.com/stephaneguindon/phyml) for branch-length
  estimation.
- [dndstools](https://bitbucket.org/Davydov/dndstools) for various
  tree and sequence manipulations.


Model abbreviations:
- `nog` model without rate variation
- `sg` model with site gamma rate variation
- `cp` model with codon 3-rate variation
- `cg` model with codon gamma rate variation
